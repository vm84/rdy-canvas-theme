<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

global $websiteTitle, $has_footer, $has_cf7_form,$gallery;
$has_cf7_form = true;


?><footer class="footer">
    <?php

    if($has_footer) {
        ?>
        <div class="footer__wrapper">
        <div class="footer__contact"><?php


            /**
             * Contact.
             */

            $phone = get_field('contact_phone', 'option');
            $email = get_field('contact_email', 'option');
            $address = get_field('contact_address', 'option');
            $contact_footer_text = get_field('contact_footer_text', 'option');
            $tr_contact_us = get_field('tr_contact_us', 'option');

            ?>
            <div class="footer__contact__info"><?php

                if (!empty($tr_contact_us)) {
                    ?><h3 class="footer__contact__info__title" data-animation="fade"><?php echo $tr_contact_us; ?></h3><?php
                }

                if (!empty($phone)) {
                    ?><a href="tel:<?php echo $phone; ?>"
                         class="contact__wrapper__phone display-inline"><?php echo $phone; ?></a><?php
                }
                if (!empty($email)) {
                    ?><a href="mailto:<?php echo $email; ?>"
                         class=" contact__wrapper__email display-inline"><?php echo $email; ?></a><?php
                }
                if (!empty($email)) {
                    ?>
                    <div class="contact__wrapper__address"><?php echo $address; ?></div><?php
                }

                /**
                 * Socials.
                 */

                $socials = get_field('socials_repeater', 'option');

                if (!empty($socials)) {
                    ?>
                    <div class="socials"><?php
                    foreach ($socials as &$social) {
                        $title = $social['social_title'];
                        $link = $social['social_link'];

                        if (!empty($link)) {
                            ?><a href="<?php echo $link; ?>" target='_blank'
                                 class="social"><?php echo do_uppercase($title); ?></a><?php
                        }

                    }
                    ?></div><?php
                }

                ?></div>
            <div class="footer__contact__form">
                <div class="contact__form__text"><?php echo $contact_footer_text; ?></div>
                <?php get_template_part('elements/inc__contact__form'); ?>
            </div>
        </div>
        </div><?php
    }


    /**
    * Bottom.
    */

    ?><div class="bottom flex flex-center">
        <div class="bottom__copy">© 2021 All rights Reserved Konstantikos Gkolfinopoulos</div>
        <div class="divider">|</div>
        <nav class="bottom__menu">
            <?php

            wp_nav_menu( array(
                'theme_location' => 'footer',
                'menu' => 'FooterMenu_'.ICL_LANGUAGE_CODE,
                'container' => false,
                'menu_class' => 'footer-ul',
                'walker' => new MainMenu_myWalker,
                'menu_to_uppercase'=>false,
            ));

            ?>
        </nav>
    </div>
</footer>
<noscript>In order to properly view this page you need JavaScript enabled!</noscript>
</div> <?php /*app__wrapper from header */?>
</div> <?php /*app__Holder from header */?>
<?php

if(isset($gallery)){
    if(count($gallery)>1){
        $main_gallery = $gallery;
        include(locate_template('elements/slider_gallery.php', false, false));
    }
}

?>
</div><?php /*app from header*/?>
<?php




wp_footer();

if ($_SERVER['HTTP_HOST']!='localhost'){

    ?>

<?php
}



?>
</body>
</html>
