<?php
/**
 * Theme functions and definitions
 */

$templatedir = get_template_directory();
$template_url = get_template_directory_uri();

define('DS', DIRECTORY_SEPARATOR);
$websiteFolder = dirname(__FILE__) . DS;
$cacheDir = $websiteFolder . 'cache' . DS;

require $templatedir . '/functions/setup.php';
require $templatedir . '/functions/walkers.php';
require $templatedir . '/functions/editor.php';
require $templatedir . '/functions/functions_wp.php';
require $templatedir . '/functions/functions_php.php';
require $templatedir . '/functions/navigation.php';
//require $templatedir . '/functions/comments.php';
require $templatedir . '/functions/custom_post_types.php';
//require $templatedir . '/functions/project_functions.php';
require $templatedir . '/functions/admin_functionalities.php';
require $templatedir . '/functions/pre_get_posts.php';



show_admin_bar(false);

if ( ! isset( $content_width ) ) {
    $content_width = 1078;  //eg for images in post
}


/* =========  global VARS for pages ========= */

if (!defined('ICL_LANGUAGE_CODE')) {
    define('ICL_LANGUAGE_CODE', 'en');
}
$default_lang = 'en';

//$active_languages = array('en'=>'English', 'el'=>'Greek');
$developmentMode = true;


$sessionName= 'kga';
$websiteTitle = 'KGA';


//printd in header

$socials = array(
    'linkedin' => '#',
    'facebook' => '#',
);

$schemaTags = array(
    'name' => $websiteTitle,
    'legalName' => $websiteTitle,
    'description' => '',
    'image' => $template_url.'/images/logo.png',
    'logo' => $template_url.'/images/logo.png',
    'url' => WP_HOME,
    'email' => '',
    'telephone' => '',
    'faxNumber' => '',
    'founder' => '',
    'address_array' => array(
        'country_code' => 'GR',    //https://en.wikipedia.org/wiki/ISO_3166-1
        'region' => 'Athens',
        'locality' => 'Kifisia',
        'zipcode' => '',
        'address' => '',
        'latitude' => '',
        'longitude' => '',
    ),
    'sameAs' => $socials
);



$css_version = '.v.1.0.1';      //change css versioning here and in config.rb OR gulpfile.js

$page_has_shares = false;
$has_cf7_form = false;
$has_footer = true;
$extra_body_class = '';


//the sizes for the featured image from cms->settings->media AND setup.php (used in cache_get_all_fields)
$featured_image_sizes = array('thumbnail', 'medium', 'large');

$default_thumbs_array = array(
    'thumbnail' => $template_url.'/images/default-thumb.jpg',
    'medium' => $template_url.'/images/default-med.jpg',
    'large' => $template_url.'/images/default.jpg',
);

$default_thumb = $default_thumbs_array['thumbnail'];


$months = array('', 'Ιαν', 'Φεβ', 'Μάρ', 'Απρ', 'Μάι', 'Ιούν', 'Ιούλ', 'Αύγ', 'Σεπτ', 'Οκτ', 'Νοέ', 'Δεκ');
$days = array('Δευτέρα', 'Τρίτη', 'Τετάρτη', 'Πέμπτη', 'Παρασκευή', 'Σάββατο', 'Κυριακή');


$static_translations = array();
$my_settings = array();


if ($developmentMode){
    error_reporting(E_ALL);
    ini_set('display_errors', 1);
}




/*
 *********************************************
============== JavaScript files ====================
 *********************************************
*/

$isHttp2 = stristr($_SERVER["SERVER_PROTOCOL"], "HTTP/2") ? true : false;

//place all javascripts in footer
remove_action('wp_head', 'wp_print_scripts');
remove_action('wp_head', 'wp_print_head_scripts', 9);
remove_action('wp_head', 'wp_enqueue_scripts', 1);
add_action('wp_footer', 'wp_print_scripts', 5);
add_action('wp_footer', 'wp_enqueue_scripts', 5);
add_action('wp_footer', 'wp_print_head_scripts', 5);

function my_scripts(){
    global $template_url, $isHttp2, $developmentMode, $page_has_shares, $has_cf7_form, $css_version, $hasmap;



    /* MINIFIED JS */
    // if(basename(get_page_template()) === 'home.php'){
    //     wp_enqueue_script( 'home', $template_url . '/public/home.js', ['jquery'], '1.0', true );
    // }elseif(  basename(get_page_template()) == 'about.php'){
    //     wp_enqueue_script( 'about', $template_url . '/public/about.js', ['jquery'], '1.0', true );
    // }elseif(  basename(get_page_template()) == 'projects.php'){
    //     wp_enqueue_script( 'projects', $template_url . '/public/projects.js', ['jquery'], '1.0', true );
    // }elseif(is_single()){
    //     wp_enqueue_script( 'sproject', $template_url . '/public/sproject.js', ['jquery'], '1.0', true );
    // }
    wp_enqueue_script('main', $template_url . '/public/main.js', ['jquery'], '1.0', true);

    if ($hasmap){
        wp_enqueue_script('mapslib', 'https://maps.googleapis.com/maps/api/js?language=en&v=3&key=AIzaSyCnc4nVPrmM0pL71ghAmcoDK_W-TCRi5Iw',array( 'jquery' ), null, true);	//new key

    }



    if ($has_cf7_form && function_exists('wpcf7_enqueue_scripts')){
		//script loading is prevented from wp_config.php
        add_filter( 'wpcf7_load_js', '__return_true', 10, 2);
        wpcf7_enqueue_scripts();
		//for acceptance: add "acceptance_as_validation: on" on the additional settings of CF7
    }
	else{
      wp_dequeue_script('google-recaptcha');
      remove_action('wp_footer', 'wpcf7_recaptcha_onload_script');
      wp_dequeue_script( 'google-invisible-recaptcha' );

      wp_dequeue_style( 'contact-form-7' );
      wp_dequeue_script( 'wpcf7-recaptcha' );
      wp_dequeue_style( 'wpcf7-recaptcha' );
      wp_dequeue_script( 'google-recaptcha' );
    }
}
if (!is_admin()){
    add_action("wp_enqueue_scripts", "my_scripts", 999);


	//optimization for js: add DEFER
	function add_defer_attribute($tag, $handle) {
		//pr($handle);
		/*$script_handles_to_avoid = array('google-recaptcha');
		if (!in_array($handle, $script_handles_to_avoid)){*/
		if ($handle != 'google-recaptcha'){
			return str_replace(' src', ' defer src', $tag);
		}
		return $tag;
		//return str_replace(' src', ' defer src', $tag);
	}
	add_filter('script_loader_tag', 'add_defer_attribute', 10, 2);


	//remove CSS for gutenberg
	add_action( 'wp_enqueue_scripts', function() {
        wp_dequeue_style( 'wp-block-library' );
    });

}


add_action('in_admin_header', function () {
    if (!is_admin()) {
        return;
    }
    remove_all_actions('admin_notices');
    remove_all_actions('all_admin_notices');

}, 1000);



/*REMOVE STYLE AND JAVASCRIPT TAGS*/
add_action(
    'after_setup_theme',
    function() {
        add_theme_support( 'html5', [ 'script', 'style' ] );
    }
);





///**** SECURITY *****////
/**
 * Enables the HTTP Strict Transport Security (HSTS) header in WordPress.
 */
// function tg_enable_strict_transport_security_hsts_header_wordpress() {
//     header( 'Strict-Transport-Security: max-age=10886400' );
// }
// add_action( 'send_headers', 'tg_enable_strict_transport_security_hsts_header_wordpress' );


// header('Content-Security-Policy: default-src \'self\' \'unsafe-inline\' \'unsafe-eval\' https: data:');


/* TODO

============================

GA + schema + $schemaTags
run gulp

social media links

check forms

-----

fix manifest.json + humans.txt
theme preview image

========

Google maps Api Key instructions: https://developers.google.com/maps/documentation/javascript/get-api-key



####################

Image sizes:

- large: 1080 x 650
- medium: 880 x 530
- thumbnail: 465 x 280  // for inner pages list: resize to 250 height

*/
//echo sprintf("%02d", $counter);

/*
Lazyload examples:

<div class="cover-img bLazy-bg" data-src="<?php echo $img; ?>"></div>
<img src=data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw== class="b-lazy" alt="xxx" width="149" height="169" data-src="<?php echo $img; ?>">
*/
