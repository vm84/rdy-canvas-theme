import Page from 'classes/Page'
import gsap from "gsap/gsap-core";
import Masonry from 'masonry-layout'

export default class Projects extends Page {
  constructor () {
    super({
      id: "projects",
      element: '[data-template="projects"]',
      elements: {
        introWrapper: ".intro__wrapper",
      },
    });
  }


  create (loco) {
    super.create()

    // this.elements.scrollTo.click(function () {
    //   const target = jQuery(this).data('target')
    //   const offset = jQuery(this).data('offset')
    //   locoScroll.scrollTo(target, -offset)
    // })

    if (document.getElementById('section-2')) {
      gsap.timeline({
        scrollTrigger: {
          trigger: '.section-2',
          start: 'top 5%',
          scroller: '.smooth-scroll',
          scrub: true,
          invalidateOnRefresh: true,
          endTrigger: '.footer',
          end: 'bottom top',
          toggleClass: { targets: 'body', className: 'change-logo' },
          toggleActions: 'restart none none none' // order:      onEnter onLeave onEnterBack onLeaveBack
        }
      })
    }


    if (document.getElementById('js-grid')) {
      // vanilla JS
      // init with element

      const grid = document.querySelector('.js-grid')
      // eslint-disable-next-line no-unused-vars
      const msry = new Masonry(grid, {
        itemSelector: '.js-grid-item',
      })
    }

    function postAjax(url, data, success) {
      var params = typeof data == 'string' ? data : Object.keys(data).map(
              function(k){ return encodeURIComponent(k) + '=' + encodeURIComponent(data[k]) }
          ).join('&');
      // eslint-disable-next-line no-undef
      var xhr = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
      xhr.open('POST', url);
      xhr.onreadystatechange = function() {
          if (xhr.readyState>3 && xhr.status==200) { success(xhr.responseText); }
      };
      xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
      xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
      xhr.send(params);
      return xhr;
    }

    const dd = document.getElementsByClassName('js-projects__filter');

    Array.from(dd).forEach(element => {
      element.addEventListener('click', () => {

        Array.from(dd).forEach(allelem => {
          allelem.classList.remove('active');
        })
        element.classList.add('active');
        gsap.to( document.getElementById('js-grid'), {duration: .85,autoAlpha:0, ease: 'power3.inOut',
          onComplete:function(){
                    // eslint-disable-next-line no-undef
            postAjax(templateURL + 'ajax_category.php', { id: element.dataset.id }, function(data){
              const js_grid = document.getElementById('js-grid')
              js_grid.innerHTML  = data
              const grid = document.querySelector('.js-grid')
              // eslint-disable-next-line no-unused-vars
              const msry = new Masonry(grid, {
                itemSelector: '.js-grid-item',
                percentPosition: true
              })

              gsap.to(document.getElementById('js-grid'), { duration: .85, autoAlpha: 1, ease: 'power3.inOut' });

              loco.update();


            });
          }
        });
      });
    });


  }

  onResize () {
    // const style = getComputedStyle(this.elements.introWrapper)
    // console.log(style.margin)
    // gsap.to('.intro__wrapper', { width: x, height: y, margin: style.margin })

  }
}
