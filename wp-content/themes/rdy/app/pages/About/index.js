
import Page from 'classes/Page'

export default class About extends Page {
  constructor () {
    super({
      id: "about",
      element: '[data-template="about"]',
      elements: {},
    });
  }

  create () {
    super.create()
  }
}
