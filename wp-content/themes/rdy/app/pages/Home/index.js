
import Page from 'classes/Page'
import gsap from "gsap/gsap-core";

export default class Home extends Page {
  constructor () {
    super({
      id: "home",
      element: '[data-template="home"]',
      elements: {
        introWrapper: ".intro__wrapper",
      },
    });
  }

  create (scroll) {
    super.create(scroll)
    if (document.getElementById('file-video')) {
      const vid = document.getElementById('file-video')
      vid.play()
    }

    // this.elements.scrollTo.click(function () {
    //   const target = jQuery(this).data('target')
    //   const offset = jQuery(this).data('offset')
    //   locoScroll.scrollTo(target, -offset)
    // })

    if (document.getElementById('hp-intro')) {
      gsap.timeline({
        scrollTrigger: {
          trigger: ".intro",
          start: "top top",
          scroller: ".smooth-scroll",
          scrub: true,
          pin: true,
          invalidateOnRefresh: true,
          end: "bottom top",

          //pinSpacing: true
          // markers: true
        },
      });

      if (document.getElementById('header__logo-letters')) {
        gsap.timeline({
          scrollTrigger: {
            trigger: '.hp-about',
            start: 'top 5%',
            scroller: '.smooth-scroll',
            scrub: true,
            invalidateOnRefresh: true,
            endTrigger: '.footer',
            end: 'bottom top',
             toggleClass: { targets: 'body', className: 'change-logo' },
            toggleActions: 'restart none none none' // order:      onEnter onLeave onEnterBack onLeaveBack
          }

        })
      }

      /* const introWrap = gsap.timeline({
        scrollTrigger: {
          trigger: '.intro__wrapper',
          start: 'top top',
          scroller: '.smooth-scroll',
          scrub: true,
          invalidateOnRefresh: true,
          onRefresh: self => {
            if (self.progress > 0) {
              location.reload()
            } else {
              gsap.to('.intro__wrapper', { duration: 0.5, ease: 'linear', width: window.innerWidth, height: window.innerHeight, margin: 0 })
            }
          },
          end: 'bottom top',
          // toggleClass: { targets: '.intro', className: 'active' },
          // toggleActions: 'restart none none none', // order:      onEnter onLeave onEnterBack onLeaveBack
          onEnter: function () {
            // console.log('enter')
            gsap.to('.intro__wrapper', { duration: 0.5, ease: 'linear', width: window.innerWidth - 100, height: window.innerHeight - 100, margin: 50 })
          },
          onEnterBack: function () {
            // console.log('enter back')
            gsap.to('.intro__wrapper', { duration: 0.5, ease: 'linear', width: window.innerWidth - 100, height: window.innerHeight - 100, margin: 50 })
          },
          onLeaveBack: function () {
            // console.log('leave back')
            gsap.to('.intro__wrapper', { duration: 0.5, ease: 'linear', width: window.innerWidth, height: window.innerHeight, margin: 0 })
          }
          // markers: true
        }

      }) */
    }
  }
}
