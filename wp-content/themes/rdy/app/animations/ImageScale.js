import gsap from "gsap/gsap-core";
import Animation from "classes/Animation";
import easeCustom from "../utils/ease";

export default class ImageScale extends Animation {
  constructor({ element, elements }) {
    super({
      element,
      elements,
    });
  }

  animateIn() {
    const homePageIntro = document.querySelector(".intro");

    if (document.getElementById("hp-intro")) {
      const pinAmount = homePageIntro.clientHeight;

      this.timelineIn = gsap.timeline({
        scrollTrigger: {
          trigger: this.element,
          //start: "top bottom",
          start: `${pinAmount}vh bottom`,
          end: "bottom top",
          scroller: ".smooth-scroll",
          toggleActions: "play play none none",
          //markers: true,
        },
      });
    } else {
      this.timelineIn = gsap.timeline({
        scrollTrigger: {
          trigger: this.element,
          start: "top bottom",
          end: "bottom top",
          scroller: ".smooth-scroll",
          toggleActions: "play play none none",
          //markers: true,
        },
      });
    }

    let delay = this.element.getAttribute("data-delay");
    let duration = this.element.getAttribute("data-duration");

    if (delay == undefined) {
      delay = 0;
    }
    if (duration == undefined) {
      duration = 1.8;
    }

    this.timelineIn.to(
      this.element,
      {
        duration: duration,
        alpha: 1,
        scale: 1,
        transformOrigin: "center center",
        ease: easeCustom.ease1,
        overwrite: "auto",
      },
      delay
    );
  }
  animateOut() {
    //   gsap.set(this.element, {
    //     autoAlpha: 0,
    //   });
  }
}
