import Navigation from "components/Navigation";
import { LocoScroll, gsap, ScrollTrigger } from "./vendors/scroll";
import { Lazy } from "./vendors/lazy";
import each from "lodash/each";



import Home from "pages/Home";
import About from "pages/About";
import Projects from "pages/Projects";
import Detail from "pages/Detail";

import Preloader from 'components/Preloader'

/**
 * Canvas.
 */
import Canvas from 'components/Canvas'


class App {
  constructor() {
    // the ordering matters
    this.locoscroll = new LocoScroll();
    this.createContent();
    this.createCanvas();
    this.createPreloader();

    //this.locoscroll = new LocoScroll();
    this.lazyLoad = this.vanillaLazy();
    this.createNavigation();
    this.createPages(this.locoscroll);

    this.addEventListeners();
    this.addLinkListeners();

    this.onResize();
    this.update();

    this.device = device;
    document.querySelector("body").classList.add("loaded");
  }

  createContent() {
    this.screenSize = document.querySelector(".test");
    this.content = document.querySelector(".main-content");
    this.template = this.content.getAttribute("data-template");
  }
  createPreloader() {
    this.preloader = new Preloader({
      canvas: this.canvas, //- 10 Perfomance images
    });
    this.preloader.once("completed", this.onPreloaded.bind(this));
  }
  createCanvas() {
    this.canvas = new Canvas({
      template: this.template,
      scroll: this.locoscroll,
    });
  }

  // createLocoScroll() {
  //   return new LocoScroll();
  // }

  updateLocoScroll(locoScroll) {
    gsap.registerPlugin(ScrollTrigger);
    locoScroll.on("scroll", ScrollTrigger.update);

    locoScroll.on("scroll", (position) => {
      if (position.scroll.y > 50) {
        document.querySelector("body").classList.add("scrolled");
      } else {
        document.querySelector("body").classList.remove("scrolled");
      }
    });

    ScrollTrigger.scrollerProxy(".smooth-scroll", {
      scrollTop(value) {
        return arguments.length
          ? locoScroll.scrollTo(value, 0, 0)
          : locoScroll.scroll.instance.scroll.y;
      },
      getBoundingClientRect() {
        return {
          top: 0,
          left: 0,
          width: window.innerWidth,
          height: window.innerHeight,
        };
      },
      pinType: document.querySelector(".smooth-scroll").style.transform
        ? "transform"
        : "fixed",
    });

    ScrollTrigger.addEventListener("refresh", () => locoScroll.update());

    ScrollTrigger.refresh();
  }

  vanillaLazy() {
    return new Lazy();
  }

  createNavigation() {
    this.navigation = new Navigation({
      template: this.template,
    });
  }

  createPages() {
    this.pages = {
      home: new Home(),
      about: new About(),
      projects: new Projects(),
      detail: new Detail(),
    };
    this.page = this.pages[this.template];
    this.page.create(this.locoscroll);

    //this.page.show()
    this.onResize();
  }

  onResize() {
    this.height = window.innerHeight;
    this.width = window.innerWidth;
    this.screenSize.innerHTML = `${this.width} x ${this.height}`;
    window.requestAnimationFrame((_) => {
      if (this.page && this.page.onResize) {
        this.page.onResize(this.locoscroll);
      }
    });

    window.requestAnimationFrame((_) => {
      if (this.canvas && this.canvas.onResize) {
        this.canvas.onResize(this.locoscroll);
      }
    });
  }

  onPreloaded() {
    this.onResize(); //Ss 13 before this.canvas.onPreloaded Important!
    this.canvas.onPreloaded(); //- 17 Perfomance images,
    //this.preloader.destroy()

    this.page.show(); // show the page when the preloader finished
    console.log("Preloaded!");
  }

  onPopState() {
    this.onChange({
      url: window.location.pathname,
      push: false,
    });
  }

  async onChange({ url, dataIndex, push = true }) {
    //for animations
 
      //2 WEBGL Transitions, pass the url
      this.canvas.onChangeStart(this.template, url, dataIndex);
    


    /**
     * Remove old Page script
     */
    // const oldPageScript = document.getElementById(this.template + "-js"); //.src
    // oldPageScript.remove();
    // const oldPageStyle = document.getElementById(this.template + "-css"); //.src
    // oldPageStyle.remove();

    //5 animations hide the current page
    await this.page.hide();
    this.locoscroll.destroy();
    this.navigation.onChange();

    const request = await window.fetch(url);
    //console.log(request)

    if (request.status === 200) {
      const html = await request.text();
      const div = document.createElement("div");

      if (push)
      {
        window.history.pushState({}, "", url);
      }

      div.innerHTML = html;
      const divContent = div.querySelector(".main-content");

      /**
       * Create Next Page Body Class
       */
      const parser = new DOMParser();
      const doc = parser.parseFromString(html, "text/html");
      const docBody = doc.querySelector("body");
      document.body.className = docBody.className;

      this.template = divContent.getAttribute("data-template");
      this.content.setAttribute("data-template", this.template);
      this.content.innerHTML = divContent.innerHTML;

     
        //asyn load the canvas.must be after the content on change
      this.canvas.onChangeEnd(this.template, url, dataIndex);
      
      console.log('app on change End' +dataIndex);
      console.log("URL on change End" + url);
    

      /**
       * update scripts
       */

      this.page = this.pages[this.template];
      this.page.create(this.locoscroll);

      this.locoscroll.init();

      //Move Scrollbar to 0
      //this.locoscroll.scroll.instance.scroll.y = this.locoscroll.scroll.instance.delta.y = 0;

      this.updateLocoScroll(this.locoscroll);

      this.onResize(this.locoscroll);

      this.page.show();
      document.querySelector("body").classList.add("loaded");

      this.lazyLoad.update();

      // Refresh the links in order to avoid reloading pages
      this.addLinkListeners();
    } else {
      console.log("Error", request);
    }
  }

  onWheel(event) {
    if (this.canvas && this.canvas.onWheel) {
      this.canvas.onWheel(event);
    }

    if (this.page && this.page.onWheel) {
      this.page.onWheel(event);
    }
  }

  /**
   * Loop.
   */

  update() {
    if (this.page && this.page.update) {
      this.page.update(this.locoscroll);
    }
    if (this.page && this.page.onResize) {
      this.page.onResize(this.locoscroll);
    }

    //The canvas must be under the this.page update, in  order to have the right Y position of the page every time
    if (this.canvas && this.canvas.update) {
      this.canvas.update(this.locoscroll); //this.page.scroll
    }

    this.frame = window.requestAnimationFrame(this.update.bind(this));
  }

  /**
   * Events.
   */

  addEventListeners() {
    //window.addEventListener('mousewheel', this.onWheel.bind(this))

    this.locoscroll.on("scroll", (instance) => {
      this.onWheel(instance);
    });

    // window.addEventListener('mousedown', this.onTouchDown.bind(this))
    // window.addEventListener('mousemove', this.onTouchMove.bind(this))
    // window.addEventListener('mouseup', this.onTouchUp.bind(this))

    // window.addEventListener('touchstart', this.onTouchDown.bind(this))
    // window.addEventListener('touchmove', this.onTouchMove.bind(this))
    // window.addEventListener('touchend', this.onTouchUp.bind(this))

    window.addEventListener("popstate", this.onPopState.bind(this));
    window.addEventListener("resize", this.onResize.bind(this));
  }

  addLinkListeners() {
    const links = document.querySelectorAll("a");
    const canvasLink = document.querySelectorAll(".js-canvas-link");



    each(links, (link) => {
      link.onclick = (event) => {
        event.preventDefault();

        const { href } = link;
        const dataIndex = link.getAttribute("data-index");

        this.onChange({
          url: href,
          dataIndex: parseInt(dataIndex),
        });
      };
    });

    if (document.getElementById("scroll-to-target")) {
      const button = document.querySelector(".scroll-to");

      button.addEventListener("click", () => {
        const target = button.getAttribute("data-target");
        const offset = button.getAttribute("data-offset");
        this.locoscroll.scrollTo(target, {
          offset: -offset,
          duration: 2000,
          easing: [0.87, 0, 0.13, 1],
        });
      });
    }
  }
}

new App();
