#define PI 3.141592653589793238
attribute vec2 uv;
attribute vec3 position;

uniform float uSpeed; //shader3  19:58 explain uStrength, based on the mouse scroll/ drag user speed
uniform vec2 uViewportSizes; //shader1

uniform mat4 modelViewMatrix;
uniform mat4 projectionMatrix;

varying float speed;

varying vec2 vUv;

void main() {
    vUv = uv;
    
    vec4 newPosition = modelViewMatrix * vec4(position, 1.0);
    //newPosition.z -= sin((newPosition.y / uViewportSizes.y ) * (newPosition.x / uViewportSizes.x )* PI * PI / 2.0);
    //newPosition.z *= sin((newPosition.y / uViewportSizes.y ) * (newPosition.x / uViewportSizes.x )* PI + PI / 2.0) * 2.0;
    //newPosition.z -= sin((newPosition.y / uViewportSizes.y ) * (newPosition.x / uViewportSizes.x )* PI + PI / 2.0) * abs(uSpeed);
    
    
    newPosition.z -= (sin(newPosition.y / uViewportSizes.y * PI + PI / 2.0) + sin(newPosition.x / uViewportSizes.x * PI + PI / 2.0) ) * abs(uSpeed);


    gl_Position = projectionMatrix * newPosition;
}