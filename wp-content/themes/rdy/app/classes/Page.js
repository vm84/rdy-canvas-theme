import gsap from "gsap/gsap-core";
//import LocoScroll from '../vendors/scroll'

import each from "lodash/each";

import { mapEach } from "utils/dom";

/**
 * Animations.
 */
import Title from "../animations/Title";
import Image from "../animations/Image";
import ImageScale from "../animations/ImageScale";

export default class Page {
  constructor({ element, elements, id }) {
    this.selector = element;
    this.selectorChildren = {
      ...elements,
      animationsImage: '[data-animation="image"]',
      animationsTitle: '[data-animation="title"]',
      animationsImageScale: '[data-animation="image-scale"]',

      // preloaders: '[data-src]'
    };
    this.id = id;

  }

  create(scroll) {
    
    this.scroll = scroll
    this.element = document.querySelector(this.selector);

    this.elements = {};

    //   this.scroll = {
    //   current: 0,
    //   target: 0,
    //   last: 0,
    //   limit: 0
    // }

    each(this.selectorChildren, (entry, key) => {
      if (
        entry instanceof window.HTMLElement ||
        entry instanceof window.NodeList ||
        Array.isArray(entry)
      ) {
        this.elements[key] = entry;
      } else {
        this.elements[key] = document.querySelectorAll(entry);

        if (this.elements[key].length === 0) {
          this.elements[key] = null;
        } else if (this.elements[key].length === 1) {
          this.elements[key] = document.querySelector(entry);
        }
      }
      // console.log(this.elements[key], entry)
    });

    /**
     * Animations.
     */
    this.createAnimations();

  }

  createAnimations() {
    this.animations = [];

    /**
     * Animation Titles.
     */
    this.animationsTitle = mapEach(this.elements.animationsTitle, (element) => {
      return new Title({
        element,
      });
    });
    this.animations.push(...this.animationsTitle);

    /**
     * Animation Images.
     */

    this.animationsImage = mapEach(this.elements.animationsImage, (element) => {
      return new Image({
        element,
      });
    });
    this.animations.push(...this.animationsImage);

    this.animationsImageScale = mapEach(
      this.elements.animationsImageScale,
      (element) => {
        return new ImageScale({
          element,
        });
      }
    );
    this.animations.push(...this.animationsImageScale);
  }

  /**
   *
   * Transitions.
   */

  show(animation) {
    //6 opposite webgl animate, add animation, because we have different animation on detail page on show funtion
    return new Promise((resolve) => {
      if (animation) {
        this.animationIn = animation;
      } else {
        this.animationIn = gsap.timeline();
        this.animationIn.fromTo(
          this.element,
          {
            autoAlpha: 0,
          },
          {
            autoAlpha: 1,
            //onComplete: resolve
          }
        );
      }

      this.animationIn.call(() => {
        this.addEventListeners();

        resolve();
      });
    });
  }

  hide() {
      console.log('hide page js');
    return new Promise((resolve) => {
      this.destroy();
      this.animationOut = gsap.timeline();

      this.animationOut.to(this.element, {
        autoAlpha: 0,
        onComplete: resolve,
      });
    });
  
  }

  /**
   *
   * Events.
   */

  onResize(scroll) {
    //each(this.animations, (animation) => animation.onResize());

    window.requestAnimationFrame(_ => {

      //this.scroll.limit = scroll.scroll.instance.limit.y


      each(this.animations, animation => {
        animation.onResize && animation.onResize()
      })


    })
  }

  onWheel(event)
  {
    //this.scroll.target += event.delta.y
  }

  /**
   * Loop.
   */

  update(scroll)
  {

    //this.scroll.current = gsap.utils.interpolate(this.scroll.scroll.instance.scroll.y, this.scroll.target, 0.1)
    //this.scroll.target = gsap.utils.clamp(0, scroll.scroll.instance.limit, scroll.scroll.instance.scroll.y)
    //this.scroll.current = Math.floor(scroll.scroll.instance.scroll.y)

  }

  /**
   * Listeners.
   */

  addEventListeners() {
    // window.addEventListener('mousewheel', this.onMouseWheelEvent);
  }

  removeEventListeners() {
    // window.removeEventListener('mousewheel', this.onMouseWheelEvent)
  }

  destroy() {

    // this.removeEventListeners(); // remove the scroll when the page is hidden
  }
}
