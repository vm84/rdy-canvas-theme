import LazyLoad from 'vanilla-lazyload'

export function Lazy () {
  const lazyLoadInstance = new LazyLoad({
  // Your custom settings go here
  })

  return lazyLoadInstance
}
