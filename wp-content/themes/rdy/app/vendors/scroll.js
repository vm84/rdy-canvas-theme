import LocomotiveScroll from "locomotive-scroll";
import gsap from "gsap/gsap-core";
import ScrollTrigger from "gsap/ScrollTrigger";

export function LocoScroll() {
  const locoScroll = new LocomotiveScroll({
    el: document.querySelector(".smooth-scroll"),
    smooth: true,
    smoothMobile: false,
    reloadOnContextChange: true,
    // lerp: 0.06
  });

  gsap.registerPlugin(ScrollTrigger);
  locoScroll.on("scroll", ScrollTrigger.update);

  locoScroll.on("scroll", (position) => {
    if (position.scroll.y > 50) {
      document.querySelector("body").classList.add("scrolled");
    } else {
      document.querySelector("body").classList.remove("scrolled");
    }
  });

  ScrollTrigger.scrollerProxy(".smooth-scroll", {
    scrollTop(value) {
      return arguments.length
        ? locoScroll.scrollTo(value, 0, 0)
        : locoScroll.scroll.instance.scroll.y;
    },
    getBoundingClientRect() {
      return {
        top: 0,
        left: 0,
        width: window.innerWidth,
        height: window.innerHeight,
      };
    },
    pinType: document.querySelector(".smooth-scroll").style.transform
      ? "transform"
      : "fixed",
  });

  ScrollTrigger.addEventListener("refresh", () => locoScroll.update());

  ScrollTrigger.refresh();

  return locoScroll;
}

export { gsap, ScrollTrigger };
