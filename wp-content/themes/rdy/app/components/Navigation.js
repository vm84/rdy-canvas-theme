import gsap from "gsap/gsap-core";
import Component from "classes/Component";
import easeCustom from "../utils/ease";

export default class Navigation extends Component {
  constructor(template) {
    super({
      element: "#navigation",
      elements: {
        trigger: "#nav-trigger",
        close: "#close-nav",
        closeTop: ".navigation__close__top",
        closeBottom: ".navigation__close__bottom",
        overlay: ".navigation__overlay",
        lineTop: ".nav__line__top",
        lineMed: ".nav__line__med",
        lineBottom: ".nav__line__bottom",
        navigationWrapper: ".navigation__wrapper",
      },
    });

    this.menu_tl = gsap.timeline({ paused: true, reversed: false });

    //this.template = template

    this.addLinkListeners();
    this.navigationOpen();
  }
  onChange() {
    this.element.classList.remove("loc");
    this.menu_tl.timeScale(1);
    this.menu_tl.reverse();
  }

  addLinkListeners() {
    // this.elements.trigger.onclick = event => {
    //   this.element.classList.toggle('loc')
    //   if (this.element.classList.contains('loc')) {
    //     this.menu_tl.play()
    //     this.menu_tl.timeScale(1)
    //   } else {
    //     this.element.classList.remove('loc')
    //     this.menu_tl.timeScale(1)
    //     this.menu_tl.reverse()
    //   }
    // }

    this.elements.trigger.onclick = () => {
      this.element.classList.toggle("loc");
      this.menu_tl.play();
      this.menu_tl.timeScale(1);
    };

    this.elements.close.onclick = () => {
      this.element.classList.remove("loc");
      this.menu_tl.timeScale(1);
      this.menu_tl.reverse();
    };
  }

  navigationOpen() {
    this.menu_tl.set(this.element, { visibility: "visible", alpha: 1 }, "0");
    this.menu_tl.set(this.elements.close, { autoAlpha: 1 }, "0.3");
    this.menu_tl.to(
      this.elements.overlay,
      {
        duration: 0.5,
        alpha: 0.5,
        visibility: "visible",
        ease: easeCustom.ease2,
      },
      "0"
    );

    this.menu_tl.to(
      this.elements.lineTop,
      {
        duration: 0.5,
        scaleX: 0,
        transformOrigin: "left center",
        ease: easeCustom.ease1,
      },
      "0"
    );
    this.menu_tl.to(
      this.elements.lineMed,
      {
        duration: 0.5,
        scaleX: 0,
        transformOrigin: "right center",
        ease: easeCustom.ease1,
      },
      "0"
    );
    this.menu_tl.to(
      this.elements.lineBottom,
      {
        duration: 0.5,
        scaleX: 0,
        transformOrigin: "left center",
        ease: easeCustom.ease1,
      },
      "0"
    );

    this.menu_tl.to(
      this.elements.navigationWrapper,
      { duration: 0.5, y: 0, ease: easeCustom.ease2 },
      "0.2"
    );
    this.menu_tl.to(
      this.elements.closeTop,
      {
        duration: 0.5,
        rotation: -45,
        transformOrigin: "bottom center",
        ease: easeCustom.ease1,
      },
      "0.25"
    );
    this.menu_tl.to(
      this.elements.closeBottom,
      {
        duration: 0.5,
        rotation: 45,
        transformOrigin: "top center",
        ease: easeCustom.ease1,
      },
      "0.25"
    );
  }
}
