import GSAP from 'gsap'
//- 6 Perfomance images, import textures from OGL
import {Texture} from 'ogl'

import Component from "classes/Component";

//import { split } from '../utils/text';

export default class Preloader extends Component {
  constructor({ canvas }) {  //- 7 Perfomance images,The preloader need to receive the canvas, in order to load the textures
      super({
          element: '.preloader',
          elements: {
            title: '.preloader__text',
            number: '.preloader__number',
            numberText: '.preloader__number__text',
            images: document.querySelectorAll('img')
          }
      })
      this.canvas = canvas//- 7 Perfomance images
    //- 5 Perfomance images, in order to load the texture from ogl to the preloader
    //store the texture url that we are preloading
    window.TEXTURES = {}



    // //Animation split text
    // split({
    //   element: this.elements.title,
    //   expression: "<br>"
    // })

    // //Wrap the title span in two differernt elements = <span><span></span></span>
    // split({
    //   element: this.elements.title,
    //   expression: "<br>"
    // })

    // this.elements.titleSpans = this.elements.title.querySelectorAll('span span')




    this.length = 0

    //console.log(this.element, this.elements)
    this.createLoader()


  }

  createLoader()
  {


    //- 4 Perfomance images
    //window.ASSETS.forEach(image => {
    this.elements.images.forEach(image => {

      //- 11 Perfomance images, import textures from OGL
        const texture = new Texture(this.canvas.gl, {
        generateMipmaps: false //in order to do not break in safari
      })

      //- 12 Perfomance images,
      const media = new window.Image()



      media.crossOrigin = 'anonymous'

      if (image.src == '')
      {
        media.src = image.dataset.src

      } else
      {
          media.src = image.src
      }
      media.onload = _ => {
        texture.image = media
        this.onAssetLoaded()

      }



      window.TEXTURES[image] = texture


    });

  }
  onAssetLoaded(image) {

    this.length += 1
    //const percent = this.length / window.ASSETS.length //- 13 Perfomance images,
    const percent = this.length / this.elements.images.length


    this.elements.numberText.innerHTML = `${Math.round(percent * 100)}%`

    if (percent === 1) {
      this.onLoaded()
    }
  }

  onLoaded() {
    //  Return a new Promise when the animation is completed
     return new Promise(resolve => {

      this.emit('completed')


       this.animateOut = GSAP.timeline({
         delay: 2
       })



      //100% percentage animation
       this.animateOut.to(this.elements.numberText, {
         autoAlpha: 0,
         y: "100%",
         duration: 1.5,
         ease: 'expo.out',
         stagger: 0.1
       }, "-=1.4")




      //layer
      this.animateOut.to(this.element, {
        autoAlpha: 0,
        duration: 1,

      })

      this.animateOut.call(_=>{
        this.destroy()
      })

    })
  }

  destroy () {

   this.element.parentNode.removeChild(this.element)
  }

}
