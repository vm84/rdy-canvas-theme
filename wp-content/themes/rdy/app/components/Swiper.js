import Swiper, { Navigation, Lazy } from "swiper";

export default class Slider {
  constructor() {
    Swiper.use([Navigation, Lazy]);
    new Swiper(".swiper", {
      effect: "slide",
      loop: true,
      speed: 1200,
      lazy: {
        loadPrevNext: true,
        loadPrevNextAmount: 2,
      },
      // eslint-disable-next-line no-dupe-keys
      navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev",
      },
    });
  }
}
