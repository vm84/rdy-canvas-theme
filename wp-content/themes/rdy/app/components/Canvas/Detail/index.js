//17 WEBGL Transitions From media collections, and clean - change some elements
import GSAP from "gsap";
import { Mesh, Plane, Program, Texture } from "ogl";
import fragment from "shaders/plane-fragment.glsl";
import vertex from "shaders/plane-vertex.glsl";
//import Detection from "classes/Detection";

import { getOffset } from "utils/dom";

export default class {
  constructor({ gl, scene, sizes, transition, scroll }) {
    //5 Webgl Image return on close,
    this.id = "detail"; // 9 Webgl Image return on close,
    this.element = document.querySelector(".detail__media__image");
    this.transition = transition; //5 Webgl Image return on close,
    this.gl = gl;
    this.scene = scene;
    this.sizes = sizes;
    this.geometry = new Plane(this.gl);
    this.scroll = scroll.scroll.instance.scroll;


    this.createTexture();
    this.createProgram();

    this.createMesh();
    this.createBounds({ sizes: this.sizes }, this.scroll);

    //7 Webgl Image return on close,

    //this.show()
  }

  createTexture() {
    // const image = this.element.getAttribute("data-src");
    // this.texture = window.TEXTURES[image];

    this.texture = new Texture(this.gl);

    this.image = new window.Image();
    //const image = this.element.querySelector("img");
    this.image.crossOrigin = "anonymous";
    //this.image.src = image.getAttribute("data-src");
    this.image.src = this.element.getAttribute("data-src");
    this.image.onload = (_) => (this.texture.image = this.image);
  }

  createProgram() {
    this.program = new Program(this.gl, {
      fragment,
      vertex,
      uniforms: {
        uAlpha: { value: 0 }, //6 Webgl Image return on close,
        tMap: { value: this.texture },
      },
    });
  }

  createMesh() {
    this.mesh = new Mesh(this.gl, {
      geometry: this.geometry,
      program: this.program,
    });

    //this.mesh.rotation.z = Math.PI * 0.01;
    this.mesh.setParent(this.scene);
  }

  createBounds({ sizes }) {
    this.sizes = sizes;
    this.bounds = this.element.getBoundingClientRect();
    this.boundsCase = getOffset(this.element, this.scroll.y);
    this.updateScale(sizes);
    // this.updateX(this.scroll.x);
    //this.updateY(this.scroll.y);
    console.log(this.boundsCase);
  }

  /**
   *
   *Animations.
   */

  show() {
    //5 Webgl Image return on close,
    //we need to await in order to display the image in the show method
    

    if (this.transition)
    {
     
      this.transition.animate(this.mesh, (_) => {
        //7 Webgl Image return on close, Only when the animation is ended, we create a callbcak
        this.program.uniforms.uAlpha.value = 1; // the callback
      });
      

    } else {
      //2 opposite webgl animate

      console.log("show opposite");

      GSAP.to(this.program.uniforms.uAlpha, {
        value: 1,

      });

      //vagos
      GSAP.to(this.mesh.program.uniforms.uAlpha, {
        value: 1,
      });
    }

  }

  hide()
  {

    console.log('hide')
    GSAP.to(this.program.uniforms.uAlpha, {
      value: 0,
    });


  }

  /**
   * Events.
   */
  onResize(sizes) {

    this.createBounds(sizes, this.scroll);
    this.updateX(this.scroll.x);
    this.updateY(this.scroll.y);
 
    this.show()
  }

  onTouchDown() {}

  onTouchMove() {}

  onTouchUp() {}

  /**
   * Loop.
   */

  updateScale() {
    this.height = this.bounds.height / window.innerHeight;
    this.width = this.bounds.width / window.innerWidth;

    //image ratio
    this.mesh.scale.x = this.sizes.width * this.width;
    this.mesh.scale.y = this.sizes.height * this.height;

  }

  updateX() {
    this.x = this.bounds.left / window.innerWidth;
    this.mesh.position.x = -this.sizes.width / 2 + this.mesh.scale.x / 2 + this.x * this.sizes.width;
  }

  updateY(y = 0){

    this.y = (this.boundsCase.top - y) / window.innerHeight; //percentage scale
    this.mesh.position.y = (this.sizes.height / 2) - (this.mesh.scale.y / 2) - (this.y * this.sizes.height)
  }

 

  update(scroll) {
    
    this.updateY(scroll.y);
    this.updateX(scroll.x);

    const y = (this.bounds.top - scroll.y) / window.innerHeight;
    this.mesh.position.y = (this.sizes.height / 2) - (this.mesh.scale.y / 2) - (y * this.sizes.height);

  }



  /**
   * Destroy.
   */
  //1 opposite webgl animate
  destroy()
  {

    this.scene.removeChild(this.mesh);
  }
}
