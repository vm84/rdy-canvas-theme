import GSAP from "gsap";
import { Mesh, Program, Texture } from "ogl";
import fragment from "shaders/collections-fragment.glsl";
import vertex from "shaders/collections-vertex.glsl";

import { getOffset } from "utils/dom";

export default class {
  constructor({ element, geometry, gl, index, scene, sizes }) {
    this.element = element;
    this.geometry = geometry;
    this.gl = gl;
    this.scene = scene;
    this.index = index;
    this.sizes = sizes; //set sizes 7

    //7 loop drag
    this.extra = {
      x: 0,
      y: 0,
    };

    //13 WEBGL Transitions
    this.opacity = {
      current: 0,
      target: 0,
      lerp: 0.1,
      multiplier: 0,
    };

    this.createTexture();
    this.createProgram();
    this.createMesh();

    //7 opposite webgl animate
    //this.createBounds({ sizes: this.sizes });

    //this.collectionList = document.querySelector(".collections__wrapper");
  }

  createTexture() {
    //- 14 Perfomance images
    // const image = this.element.querySelector(
    //   ".collections__gallery__media__image"
    // );
    // this.texture = window.TEXTURES[image.getAttribute("data-src")];

    // //https://github.com/oframe/ogl/blob/master/examples/textures.html
    this.texture = new Texture(this.gl);

    this.image = new window.Image();
    const image = this.element.querySelector("img");
    this.image.crossOrigin = "anonymous";
    this.image.src = image.getAttribute("data-src");
    this.image.onload = (_) => (this.texture.image = this.image);
  }

  createProgram() {
    this.program = new Program(this.gl, {
      fragment,
      vertex,
      uniforms: {
        uAlpha: { value: 1 },
        tMap: { value: this.texture },
      },
    });
  }

  createMesh() {
    this.mesh = new Mesh(this.gl, {
      geometry: this.geometry,
      program: this.program,
    });

    this.mesh.setParent(this.scene);

    this.mesh.scale.x = 2;
    //Images side by side in canvas
    //this.mesh.position.x += this.index * this.mesh.scale.x

    //Random transform mesh images
    //this.mesh.rotation.z = GSAP.utils.random(-Math.PI*0.03, Math.PI*0.03)

    //this.mesh.rotation.z = GSAP.utils.random(-Math.PI*0.1, Math.PI*0.1)
    //this.mesh.rotation.x = GSAP.utils.random(-Math.PI*0.1, Math.PI*0.1)
  }
  //set sizes 8
  createBounds({ sizes }, scroll) {
    //deconstruct the sizes. Because if we dont, can not pass numbers
    this.sizes = sizes;
    this.bounds = this.element.getBoundingClientRect();

    this.updateScale(sizes);
    // this.updateX();
    // this.updateY(scroll);
    // console.log(this.bounds.top);
    // console.log(scroll);


    this.boundsCase = getOffset(this.element, scroll.y);



  }

  /**
   *
   *Animations.
   */

  show() {
    //show canvas elements with opacity

    //14 WEBGL Transitions, in order to avoid to update the uAlpha in multiple places
    GSAP.fromTo(
      this.opacity,
      {
        multiplier: 0,
      },
      {
        multiplier: 1,
      }
    );
  }

  hide() {
    //Remove canvas elements with opacity
    GSAP.to(this.opacity, {
      multiplier: 0,
    });
  }

  /**
   * Events.
   */
  onResize(sizes, scroll) {
    // this.extra = {
    //   x: 0,
    //   y: 0,
    // };
     this.createBounds(sizes, scroll);
    this.updateX(scroll && scroll.x);
    this.updateY(scroll && scroll.y);



  }

  /**
   * Loop.
   */

  //set sizes 9
  updateScale() {
    this.height = this.bounds.height / window.innerHeight;
    this.width = this.bounds.width / window.innerWidth;

    //image ratio
    this.mesh.scale.x = this.sizes.width * this.width;
    this.mesh.scale.y = this.sizes.height * this.height;
  }

  updateX(x = 0) {
    this.x = (this.bounds.left + x) / window.innerWidth;
    this.mesh.position.x =
      -this.sizes.width / 2 +
      this.mesh.scale.x / 2 +
      this.x * this.sizes.width +
      this.extra.x; //8 loop drag, add extra
    //console.log(this.mesh.position.x )
    //console.log('----------------' )
  }

  updateY(y = 0)
  {

    //this.y = (this.bounds.top - y) / window.innerHeight; //percentage scale
    this.y = (this.boundsCase.top - y) / window.innerHeight; //percentage scale

    console.log(this.y)
    //this.y = this.bounds.top + (this.homeItem.position % this.boundsList.height), this.boundsCase.top - y

    //this.y = this.bounds.top + (this.element.position % this.boundsList.height) - y

    this.mesh.position.y = this.sizes.height / 2 - this.mesh.scale.y / 2 - this.y * this.sizes.height + this.extra.y


  }

  update(scroll, index) {
    //10 WEBGL Transitions, pass the index

    //console.log(scroll)

    //this.scroll = scroll //vag

    this.updateX(0); // vag scroll
    this.updateY(scroll);
    // const amplitude = 0.1;
    // const frequency = 1;

    //simple sin wave
    //this.mesh.rotation.z = -0.02 * Math.PI * Math.sin(this.index / frequency)
    //this.mesh.position.y = amplitude * Math.sin(this.index / frequency)

    //11 WEBGL Transitions, Highlight the current media element on center on the view port
    this.opacity.target = this.index === index ? 1 : 0.4;
    this.opacity.current = GSAP.utils.interpolate(
      this.opacity.current,
      this.opacity.target,
      this.opacity.lerp
    );

    //pass the opacity to the program shader
    // this.program.uniforms.uAlpha.value =
      this.opacity.current * this.opacity.multiplier; // multiplier is the animation in and out

    this.program.uniforms.uAlpha.value = this.opacity.multiplier; // multiplier is the animation in and out
  }
}
