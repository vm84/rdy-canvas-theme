import GSAP from "gsap";
import { Plane, Transform } from "ogl";
import Prefix from "prefix";
import map from "lodash/map";

import Media from "./Media";
import { transform } from "lodash";

export default class {
  constructor({ gl, scene, sizes, transition, scroll }) {
    this.id = "home"; // 8 Webgl Image return on close,
    this.gl = gl;
    this.scene = scene;
    this.sizes = sizes; //set sizes 5
    this.scroll = scroll //Canvas .js function createHome

    this.group = new Transform()
    this.transition = transition; //3 opposite webgl animate

    //2 titles animation
    this.transformPrefix = Prefix("transform");

    this.galleryWrapperElement = document.querySelector(".hp-projects"); //collections__gallery__wrapper
    //this.galleryElement = document.querySelector(".collections__gallery");

    this.mediasElement = document.querySelectorAll(".js-canvas-link"); //collections__gallery__media
    //this.mediasElementActive = ".collections__gallery__media";

    //2 Change collection on scroll
    // this.collectionsElements = document.querySelectorAll(
    //   ".collection__article"
    // );

    //1 titles animation
    //this.titlesElement = document.querySelector(".collections__titles");

    // this.scroll = {
    //   current: 0,
    //   target: 0,
    //   last: 0,
    //   lerp: 0.1,
    //   velocity: 1,
    // };

    this.createGeometry();
    this.createGallery();

    this.onResize({ sizes: this.sizes }); //6 opposite webgl animate

    this.group.setParent(this.scene);

    this.show();
  }
  createGeometry() {
    this.geometry = new Plane(this.gl);
  }

  createGallery() {
    this.medias = map(this.mediasElement, (element, index) => {


      return new Media({
        element,

        geometry: this.geometry,
        index,
        gl: this.gl,
        scene: this.group,
        sizes: this.sizes,
      });
    });
  }

  /**
   * Animations.
   */
  async show() {
    //8 opposite webgl animate, async and more
    //8 opposite webgl animate, async and more

    if (this.transition) {
      const { src } = this.transition.mesh.program.uniforms.tMap.value.image;
      const texture = window.TEXTURES[src];
      const media = this.medias.find((media) => media.texture === texture);
      // console.log(src)
      // console.log(texture)
      // console.log(media)

      //9 opposite webgl animate, back element to position
      const scroll =
        -media.bounds.top - media.bounds.height / 2 + window.innerHeight / 2;

      //11 opposite webgl animate,
      //add the update here in order to have the rotation, scale, position values in transition animate, below
      this.update();

      this.transition.animate(
        {
          position: { x: 0, y: media.mesh.position.y, z: 0 },
          rotation: media.mesh.rotation, //-12:26 last course  //11 opposite webgl animate, back element to position
          scale: media.mesh.scale,
        },
        (_) => {
          media.opacity.multiplier = 1;

          map(this.medias, (item) => {
            if (media !== item) {
              item.show();
            }
          });

          //10 opposite webgl animate, back element to position
          // this.scroll.current =
          //   this.scroll.target =
          //   this.scroll.start =
          //   this.scroll.last =
          //     scroll;
        }
      );
    } else {
      map(this.medias, (media) => media.show());
    }

    //3opposite webgl animate
    // if(this.transition){
    //   this.transition.animate(this.medias[0].mesh, _=> {

    //   })
    // }
  }
  hide() {
    map(this.medias, (media) => media.hide());
  }

  /**
   * Events.
   */

  onResize(event) {
     this.sizes = event.sizes;
    // console.log(this.scroll.scroll.instance.scroll);

     this.bounds = this.galleryWrapperElement.getBoundingClientRect();

    // //this.scroll.last = this.scroll.target = 0;
    // //console.log(this.scroll.scroll.instance.scroll);
    map(this.medias, (media) =>
      media.onResize(event, this.scroll.scroll.instance.scroll)
    );


    //2 scroll limit, the gallery width - the width of the first gallery element
    //this.scroll.scroll.instance.limit.y = this.bounds.height - this.medias[0].element.clientHeight;


  }

  // onTouchDown({ x, y }) {
  //   this.scroll.last = this.scroll.current;
  // }
  // onTouchMove({ x, y }) {
  //   const distance = y.start - y.end;
  //   this.scroll.target = this.scroll.last - distance;
  // }

  // onTouchUp({ x, y }) {}

  onWheel({ pixelY }) {
    //this.scroll.target -= pixelY;


  }

  /**
   *
   * onChange
   */

  onChange(index) {
    //2 Change collection on scroll

    // this.index = index;

    // const selectedCollection = parseInt(
    //   this.mediasElement[this.index].getAttribute("data-index")
    // );

    //3 Change collection on scroll, change the content on scroll
    // map(this.collectionsElements, (element, elementIndex) =>{
    //     // Because the operator is check the type if its strin or number we add the parseInt above
    //     if(elementIndex === selectedCollection) {
    //         element.classList.add(this.collectionsElementsActive)
    //     }else {
    //         element.classList.remove(this.collectionsElementsActive)
    //     }
    // })

    // 3 titles animation
    // this.titlesElement.style[this.transformPrefix] = `translateY(${25 * selectedCollection}%) translate(-50%, -50%) rotate(-90deg)`

    //8 opposite webgl animate
    //this.media = this.medias[this.index], we remnove it because, cause flickering problems on backward animation
  }

  // onWheel({pixelX, pixelY}){

  // }

  /**
   * Update.
   */
  update(scroll) {
    //console.log(this.scroll)

    //LIMIT THE SCROLL OF THE SLIDER BECAUSE IS NOT INFINITY

    //1 scroll limit //this.width is the width of the gallery wrapper
    // this.scroll.target = GSAP.utils.clamp(
    //   -this.scroll.limit,
    //   0,
    //   this.scroll.target
    // );

    // this.scroll.current = GSAP.utils.interpolate(
    //   this.scroll.current,
    //   this.scroll.target,
    //   this.scroll.lerp
    // );

    //Matching the DOM element with webGL elements on scroll, translateX the collections__gallery
    //this.galleryElement.style[this.transformPrefix] = `translateY(${this.scroll.current}px)`

    // if(this.scroll.last < this.scroll.current) {
    //   this.scroll.direction = 'right'
    // }else if(this.scroll.last > this.scroll.current) {
    //   this.scroll.direction = 'left'
    // }

    //this.scroll.last = this.scroll.current

    //9 WEBGL Transitions move it before medias
    //Get the index number of the element based on scroll
    //1 Change collection on scroll
    //  const index = Math.floor(Math.abs((this.scroll.current - (this.medias[0].bounds.height )) / this.scroll.limit) * (this.medias.length - 1))

    // if(this.index !== index){ //to avoid the looping on console.log
    //     //console.log(index)
    //     this.onChange(index)
    //     this.index = index
    // }


    map(this.medias, (media, index) => {
      
      media.update(scroll.y, this.index); //10 WEBGL Transitions, pass the index on each media elements

      // media.mesh.rotation.z = Math.abs(GSAP.utils.mapRange(0, 1, -0.2, 0.2, index / (this.medias.length -1))) -0.1
      //media.mesh.position.y += Math.cos((media.mesh.position.x / this.sizes.width) * Math.PI * 0.1) * 40 - 40
    });
  }

  /**
   * Destroy.
   */
  destroy() {
    this.scene.removeChild(this.group);
  }
}
