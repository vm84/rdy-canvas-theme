//Copy from collections media.js
import GSAP from "gsap";
import { Mesh, Plane, Program, Texture } from "ogl";
import fragment from "shaders/plane-fragment.glsl";
import vertex from "shaders/plane-vertex.glsl";

export default class {
  //7 WEBGL Transitions, set up constructor
  constructor({ gl, scene, sizes, url, dataIndex }) {
    //this.collections = collections;
    this.gl = gl;

    this.scene = scene
    this.sizes = sizes
    this.url = url
    this.dataIndex = dataIndex
    this.geometry = new Plane(this.gl);

    // this.scroll = {
    //   current: 0,
    //   target: 0,
    //   last: 0,
    //   lerp: 0.1,
    //   velocity: 1,
    // };
  }

  createProgram(texture) {
    this.program = new Program(this.gl, {
      fragment,
      vertex,
      uniforms: {
        uAlpha: { value: 1 },
        tMap: { value: texture }, //17 WEBGL Transitions, display the texture
      },
    });
  }

  createMesh(mesh) {
    this.mesh = new Mesh(this.gl, {
      geometry: this.geometry,
      program: this.program,
      dataIndex: this.dataIndex
    });

    // //18 WEBGL Transitions, apply the right width, height of the texture
    this.mesh.scale.x = mesh.scale.x;
    this.mesh.scale.y = mesh.scale.y;
    this.mesh.scale.z = mesh.scale.z;



    this.mesh.position.x = mesh.position.x;
    this.mesh.position.y = mesh.position.y;
    // //webgl element order, like z-index - the clicked image have to be in the same position z with the inner image
    this.mesh.position.z = mesh.position.z + 0.01;

    this.mesh.rotation.x = mesh.rotation.x;
    this.mesh.rotation.y = mesh.rotation.y;
    this.mesh.rotation.z = mesh.rotation.z;

    this.mesh.setParent(this.scene);
  }

  /**
   *
   * Element.
   */
  //7 Webgl Image return on close,
  setElement(element, dataIndex){


    if (element.id === "home")
    {
      //console.log(element.galleries[element.index]);
      //16 WEBGL Transitions, Check the collections index from the collections, and display the right texture

      //MediaIndex is data-index from element click
      // index is the number of galleries
      const { index, medias } = element.galleries[element.index];

      console.log(element);
      
      if (isNaN(this.dataIndex))
      {
        this.media = element.galleries[element.index].medias[element.transition.dataIndex];
        console.log('1')
      }else if ((this.dataIndex != undefined || typeof (this.dataIndex)) != "undefined") {
        console.log("2");
        this.media = element.galleries[element.index].medias[this.dataIndex];

      } else {
        console.log("3");
         this.media =
           element.galleries[element.index].medias[
             element.transition.dataIndex
           ];
      }
      
  
      
      this.createProgram(this.media.texture);
      this.createMesh(this.media.mesh);


      this.transition = "detail";

      //console.log("transition " + this.transition);
    } else
    {
      this.createProgram(element.galleries[element.index].texture);
      this.createMesh(element.galleries[element.index].mesh);

      this.transition = "home";
      //console.log("transition else" + this.transition);
    }
  }

  /**
   *
   *Animations.
   */

  animate(element, onComplete) {
    //19 WEBGL Transitions, add the element, //3 Webgl Image return on close,

    console.log('animate')

    const timeline = GSAP.timeline();
    timeline.to(
      this.mesh.scale,
      {
        duration: 1.5,
        ease: "expo.inOut",
        x: element.scale.x,
        y: element.scale.y,
        z: element.scale.z,
      },
      0
    );

    timeline.to(
      this.mesh.position,
      {
        duration: 1.5,
        ease: "expo.inOut",
        x: element.position.x,
        y: element.position.y,
        z: element.position.z,
      },
      0
    );

    timeline.to(
      this.mesh.rotation,
      {
        duration: 1.5,
        ease: "expo.inOut",

        x: element.rotation.x,
        y: element.rotation.y,
        z: element.rotation.z,
      },
      0
    );

    timeline.call((_) => {
      //14 opposite webgl animate, flickering problem, with slider
      onComplete();
    });

    timeline.call((_) => {
        //6 opposite webgl animate, remove destroy on close
      this.scene.removeChild(this.mesh);

      },
      null,
      "+=0.2"
    ); //13 opposite webgl animate, add delay, flickering problem, with slider
  }
}
