import GSAP from "gsap";
import { Renderer, Camera, Transform } from "ogl";

//import About from "./About";
import Home from "./Home";
//import Collections from "./Collections";

//19 WEBGL Transitions,
import Detail from "./Detail";

//6 WEBGL Transitions,
import Transition from "./Transition";

export default class Canvas {
  constructor({ template, scroll }) {
    this.template = template;
    this.scroll = scroll; //createCanvas scroll, app index js

    //1 Caclulate distance
    this.x = {
      start: 0,
      distance: 0,
      end: 0,
    };
    this.y = {
      start: 0,
      distance: 0,
      end: 0,
    };

    this.createRenderer();
    this.createCamera();
    this.createScene();
    this.onResize(); //set sizes 4
  }

  createRenderer() {
    this.renderer = new Renderer({
      alpha: true, //transparent canvas
      antialias: true, // fix the image sharp borders
    });
    this.gl = this.renderer.gl;
    document.body.appendChild(this.gl.canvas);
  }
  createCamera() {
    this.camera = new Camera(this.gl);
    this.camera.position.z = 5;
  }

  createScene() {
    this.scene = new Transform();
  }

  /**
   * Home.
   */
  createHome() {
    //Canvas for Homapege
    this.home = new Home({
      gl: this.gl,
      scene: this.scene,
      sizes: this.sizes,
      scroll: this.scroll,
      transition: this.transition,
    });
  }

  destroyHome() {
    if (!this.home) return;
    this.home.destroy();
    this.home = null;
  }

  /**
   * About.
   */
  // createAbout() {
  //   this.about = new About({
  //     gl: this.gl,
  //     scene: this.scene,
  //     sizes: this.sizes, //set sizes 4
  //   });
  // }
  // destroyAbout() {
  //   if (!this.about) return;
  //   this.about.destroy();
  //   this.about = null;
  // }

  /**
   * Collections.
   */
  // createCollections() {
  //   this.collections = new Collections({
  //     gl: this.gl,
  //     scene: this.scene,
  //     sizes: this.sizes,
  //     transition: this.transition, //4 opposite webgl animate
  //   });
  // }
  // destroyCollections() {
  //   if (!this.collections) return;
  //   this.collections.destroy();
  //   this.collections = null;
  // }

  //19 WEBGL Transitions
  /**
   * Detail.
   *
   */
  createDetail() {
    this.detail = new Detail({
      gl: this.gl,
      scene: this.scene,
      sizes: this.sizes,
      transition: this.transition, //4 Webgl Image return on close, 45:27
      scroll: this.scroll,
    });
  }
  destroyDetail() {
    if (!this.detail) return;
    this.detail.destroy();
    this.detail = null;
  }

  /**
   * Events.
   */

  onPreloaded() {
    //- 16 Perfomance images,
    this.onChangeEnd(this.template);
  }

  //for animations
  onChangeStart(template, url, dataIndex) {
    //3 WEBGL Transitions, pass template, url

    // if (this.about) {
    //   this.about.hide();
    // }

    // if (this.collections) {
    //   this.collections.hide();
    // }

    if (this.detail) {
      this.detail.hide();
    }

    if (this.home) {
      this.home.hide();
    }

    //1 WEBGL Transitions
    this.isFromHomeToDetail =
      this.template === "home" && url.indexOf("projects") > -1; //4 WEBGL Transitions, check if the detail string is part of the url, if yes, we get a number hight than -1
    this.isFromDetailToHome =
      this.template === "detail" && url.indexOf("projects") > -1;

    //5 WEBGL Transitions, if any of the above is true, we are going to create the new element
    if (this.isFromHomeToDetail || this.isFromDetailToHome) {
      //Create a new transition class, which is going to animate from this state to other one
      this.transition = new Transition({
        gl: this.gl,
        scene: this.scene,
        sizes: this.sizes,
        url,
        dataIndex: parseInt(dataIndex),
      });

      this.transition.setElement(this.home || this.detail, dataIndex); //9 Webgl Image return on close,
    }
  }

  onChangeEnd(template, url, dataIndex)
  {
    console.log('data-index '+dataIndex);
    // if (template === "about") {
    //   this.createAbout();
    // } else if (this.about) {
    //   this.destroyAbout();
    // }

    // if (template === "collections") {
    //   this.createCollections();
    // } else if (this.collections) {
    //   this.destroyCollections();
    // }

    //18 WEBGL Transitions
    if (template === "detail") {
      this.createDetail();
    } else if (this.detail) {
      this.destroyDetail();
    }

    if (template === "home") {
      this.createHome();
    } else {
      this.destroyHome();
    }

    this.template = template; //10 WEBGL Transitions
  }

  onResize() {
    this.renderer.setSize(window.innerWidth, window.innerHeight);

    this.camera.perspective({
      aspect: window.innerWidth / window.innerHeight,
    });

    //set sizes 1
    const fov = this.camera.fov * (Math.PI / 180);
    const height = 2 * Math.tan(fov / 2) * this.camera.position.z;
    const width = height * this.camera.aspect;

    //set sizes 2
    this.sizes = {
      height,
      width,
    };

    const values = {
      sizes: this.sizes,
    };

    // if (this.about) {
    //   this.about.onResize(values);
    // }

    // if (this.collections) {
    //   this.collections.onResize(values);
    // }

    //20 WEBGL Transitions

    if (this.detail) {
      this.detail.onResize(values);
    }

    //set sizes 3
    if (this.home) {
      this.home.onResize(values);
    }
  }

  onTouchDown(event) {
    this.isDown = true; //we want the onTouchMove to fired only when the Touch Down start
    //check the events
    this.x.start = event.touches ? event.touches[0].clientX : event.clientX;
    this.y.start = event.touches ? event.touches[0].clientY : event.clientY;

    const values = {
      x: this.x,
      y: this.y,
    };

    if (this.home) {
      this.home.onTouchDown(values);
    }
    // if (this.about) {
    //   this.about.onTouchDown(values);
    // }
    // if (this.collections) {
    //   this.collections.onTouchDown(values);
    // }
    //21 WEBGL Transitions
    if (this.detail) {
      this.detail.onTouchDown(values);
    }
  }
  onTouchMove(event) {
    if (!this.isDown) return;

    const x = event.touches ? event.touches[0].clientX : event.clientX;
    const y = event.touches ? event.touches[0].clientY : event.clientY;

    //2 Caclulate distance
    this.x.end = x;
    this.y.end = y;

    const values = {
      x: this.x,
      y: this.y,
    };

    if (this.home) {
      this.home.onTouchMove(values);
    }
    // if (this.about) {
    //   this.about.onTouchMove(values);
    // }
    // if (this.collections) {
    //   this.collections.onTouchMove(values);
    // }

    //22 WEBGL Transitions
    if (this.detail) {
      this.detail.onTouchMove(values);
    }
  }
  onTouchUp(event) {
    this.isDown = false;
    const x = event.changedTouches
      ? event.changedTouches[0].clientX
      : event.clientX;
    const y = event.changedTouches
      ? event.changedTouches[0].clientY
      : event.clientY;

    //3 Caclulate distance
    this.x.end = x;
    this.y.end = y;

    const values = {
      x: this.x,
      y: this.y,
    };

    // if (this.about) {
    //   this.about.onTouchUp(values);
    // }

    if (this.home) {
      this.home.onTouchUp(values);
    }
    // if (this.collections) {
    //   this.collections.onTouchUp(values);
    // }

    //23 WEBGL Transitions
    if (this.detail) {
      this.detail.onTouchUp(values);
    }
  }
  //On wheel 1
  onWheel(event) {
    if (this.home) {
      this.home.onWheel(event);
    }

    // if (this.collections) {
    //   this.collections.onWheel(event);
    // }
  }

  /**
   * Loop.
   */

  update(scroll) {
    // from index.js function update - canvas.update
    //Canvas Scroll 2, we add scroll

    // if (this.about) {
    //   this.about.update(scroll); //Canvas Scroll 3. We pass scroll argument to about page
    // }

    // if (this.collections) {
    //   this.collections.update();
    // }

    if (this.home) {
      this.home.update(scroll.scroll.instance.scroll);
    }

    //24 WEBGL Transitions
    if (this.detail) {
      this.detail.update(scroll.scroll.instance.scroll);
    }

    this.renderer.render({
      camera: this.camera,
      scene: this.scene,
    });
  }
}
