
import { Plane, Transform } from 'ogl'

import map from 'lodash/map'

import Gallery from './Gallery'


export default class {
  constructor({ gl, scene, sizes, scroll, transition }) {
    this.id = "home"
    this.gl = gl
    this.scene = scene
    this.sizes = sizes
    this.group = new Transform()
    this.scroll = scroll //Canvas .js function createHome
    this.transition = transition


    this.createGeometry()
    this.createGalleries()


    this.group.setParent(scene)

    //animations
    this.show()


  }
  createGeometry(){
    //https://oframe.github.io/ogl/examples/?src=base-primitives.html
    this.geometry = new Plane(this.gl);
  }

  createGalleries(){

    this.galleriesElements =  document.querySelectorAll('.hp-projects')

    this.galleries = map(this.galleriesElements, (element, index) =>
    {
      return new Gallery({
        element,
        geometry: this.geometry,
        index,
        gl: this.gl,
        scene: this.group,
        sizes: this.sizes,
        scroll: this.scroll,
        transition: parseInt(this.transition),
      });
    })



  }


  /**
   * Animations.
   */
  show () {
    map(this.galleries, gallery => gallery.show())
  }
  hide () {
    map(this.galleries, gallery => gallery.hide())
  }

  /**
   * Events.
   */

  //set sizes 4
  onResize(event)
  {


    map(this.galleries, gallery => gallery.onResize(event))


  }

  // onTouchDown(event) {

  //   map(this.galleries, gallery => gallery.onTouchDown(event))

  // }
  // onTouchMove(event) {

  //   map(this.galleries, gallery => gallery.onTouchMove(event))

  // }
  // onTouchUp(event) {
  //   map(this.galleries, gallery => gallery.onTouchUp(event))
  // }

  //On wheel 2
  onWheel({pixelX, pixelY}){

  }


  /**
   * Update.
   */
  update() {
    //scroll from index.js app => this.canvas.update(this.locoscroll)

    // Check index galleries on scroll
    const index = Math.floor(Math.abs((this.scroll.scroll.instance.scroll.y - (this.galleries[0].bounds.height / 2)) / this.scroll.scroll.instance.limit.y) * (this.galleries.length))


    if (this.index !== index) {
      this.index = index;
    }

    map(this.galleries, (gallery) =>
      gallery.update(this.scroll.scroll.instance.scroll.y, this.index)
    );
  }

  /**
   * Destroy
   */
   destroy(){
    map(this.galleries, gallery => gallery.destroy())
  }

}
