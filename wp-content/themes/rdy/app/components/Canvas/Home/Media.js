import GSAP from 'gsap'
import { Mesh, Program, Texture } from 'ogl'
import fragment from 'shaders/plane-fragment.glsl'
import vertex from 'shaders/plane-vertex.glsl'


import { getOffset } from "utils/dom";

export default class {
  constructor({ element, geometry, gl, index, scene, sizes, scroll, dataIndex}){
    this.element = element
    this.geometry = geometry
    this.gl = gl
    this.scene = scene
    this.index = index
    this.sizes = sizes
    this.scroll = scroll
    this.dataIndex = dataIndex


    //7 loop drag
    this.extra = {
      x: 0,
      y: 0
    }
    this.createTexture()
    this.createProgram()
    this.createMesh()

    //this.createBounds({sizes: this.sizes})



  }

  createTexture() {

    //- 13 Perfomance images
     //const image = this.element.querySelector('img')

    //this.texture = window.TEXTURES[image.getAttribute('data-src')]



    //https://github.com/oframe/ogl/blob/master/examples/textures.html
    this.texture = new Texture(this.gl)


    this.image = new window.Image();
    const image = this.element.querySelector('img')
    this.image.crossOrigin = 'anonymous'
    this.image.src = image.getAttribute('data-src')
    this.image.onload = _ => (this.texture.image = this.image)


  }

  createProgram(){
    this.program = new Program(this.gl, {
      fragment,
      vertex,
      uniforms: {
        uAlpha: { value: 0 },
        tMap: { value: this.texture },

        //Vertex for Home animation
        uSpeed: { value: 0 },
        uViewportSizes: { value: [this.sizes.width, this.sizes.height] },
      },
    });


  }

  createMesh(){
    this.mesh = new Mesh(this.gl, {
      geometry: this.geometry,
      program: this.program,
      dataIndex: parseInt(this.dataIndex),
    });


    this.mesh.setParent(this.scene)
    //this.mesh.scale.x = 2




  }
  //set sizes 8
  createBounds({ sizes }, scroll) {
    this.sizes = sizes
    this.bounds = this.element.getBoundingClientRect()

    this.updateScale(sizes)
    // this.updateX()
    // this.updateY() //this.scroll.scroll.instance.scroll.y

    this.boundsCase = getOffset(this.element, scroll.y);
  }

/**
   *
   *Animations.
   */

   show(){
    //show canvas elements with opacity

    GSAP.fromTo(this.program.uniforms.uAlpha, {
      value: 0
    },{
      value: 1
    })


  }

  hide (){
     //Remove canvas elements with opacity
    GSAP.to(this.program.uniforms.uAlpha, {
      value: 0
    })
  }


  /**
   * Events.
   */
  onResize(sizes, scroll){
    this.extra = {
      x: 0,
      y: 0
    }
    this.scroll = {x: 0, y: 0 }
    this.createBounds(sizes, scroll);

    this.updateX();
    this.updateY(); //scroll from gallery.js onResize




  }

  /**
   * Loop.
   */

  //2 rotation
  updateRotation() {
    this.mesh.rotation.z = GSAP.utils.mapRange(
    -this.sizes.width / 2,
    this.sizes.width / 2 ,
    Math.PI * 0.1,
    -Math.PI * 0.1,
    this.mesh.position.x)
  }


  //set sizes 9
  updateScale () {


    this.height = this.bounds.height / window.innerHeight
    this.width = this.bounds.width / window.innerWidth

    //image ratio
    this.mesh.scale.x = this.sizes.width * this.width
    this.mesh.scale.y = this.sizes.height * this.height


    //12 rotation play with the scale on scroll
    // const scale = GSAP.utils.mapRange(-this.sizes.width / 2, this.sizes.width / 2 , 0, 0.5,  Math.abs(this.mesh.position.x))
    // this.mesh.scale.x += scale
    // this.mesh.scale.y += scale
  }

  updateX( x = 0) {

    this.x = (this.bounds.left + x) / window.innerWidth
    this.mesh.position.x = (-this.sizes.width / 2) + (this.mesh.scale.x / 2) + (this.x * this.sizes.width) + this.extra.x //8 loop drag, add extra

  }

  updateY( y = 0 ) {

     //percentage scale



     //this.y = (this.bounds.top - y) / window.innerHeight
     this.y = (this.boundsCase.top - y) / window.innerHeight;
     this.mesh.position.y = (this.sizes.height / 2) - (this.mesh.scale.y / 2) - (this.y  * this.sizes.height) + this.extra.y

    //console.log(this.mesh.position.y);
    //3 rotation
    //the.sizes.width is the total amount of all images
    //this.mesh.position.y += Math.cos((this.mesh.position.x / this.sizes.width) * Math.PI * 0.1 ) * 40 -40

  }

  //UPDATE ON SCROLL
  update(scroll, index) // Gallery js update line 170
  {
    //console.log(this.mesh.position.y);

    //this.updateRotation()
    //this.updateX(scroll)
    this.updateY(scroll); //add scroll in order to move the canvas elements on scrollY
    //this.updateScale ()

    //console.log('media '+index);
    //Vertex for Home animation
    //this.program.uniforms.uSpeed.value = scroll / 1000;



  }


}
