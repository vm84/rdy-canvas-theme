import GSAP from 'gsap'

import { Transform } from 'ogl'

import map from 'lodash/map'
import Media from './Media'
import { getOffset } from "utils/dom";


export default class Gallery {
  constructor({
    element,
    geometry,
    index,
    gl,
    scene,
    sizes,
    scroll,
    transition,
  }) {
    this.elementWrapper = element.querySelector(".hp-projects__wrapper");
    this.element = element;
    this.geometry = geometry;
    this.index = index;
    this.gl = gl;
    this.scene = scene;
    this.sizes = sizes;
    this.scroll = scroll;

    this.group = new Transform();

    this.transition = transition;

    // this.scroll = {
    //   current: 0,
    //   target: 0,
    //   last: 0,
    //   lerp: 0.1,
    //   velocity: 1 //7 rotation
    // }

    this.createMedias();
    this.onResize({ sizes: this.sizes });

    //the wrapper of all medias
    this.group.setParent(this.scene);
  }
  createMedias() {
    this.mediasElements = this.element.querySelectorAll(".hp-project");
    this.medias = map(this.mediasElements, (element, index) =>
    {
      const dataIndex = element.querySelector(".js-canvas-link").getAttribute("data-index")

      return new Media({
        element,
        geometry: this.geometry,
        index,
        dataIndex: parseInt(dataIndex),
        gl: this.gl,
        scene: this.group,
        sizes: this.sizes,
        scroll: this.scroll,
      });
    });
  }

  /**
   * Animations.
   */
  async show()
  {
   //console.log('async show')
    //8 opposite webgl animate, async and more


    if (this.transition)
    {
      const dataIndex = this.transition.dataIndex; //from Canvas 165 line
  
      console.log("async show transition");


      //const { src } = this.transition.mesh.program.uniforms.tMap.value.image;
      //const texture = window.TEXTURES[src];
      const texture = this.medias[dataIndex].texture;
      //const media = this.medias.find((media) => media.texture === texture);
      const media = this.medias.find((media) => media.texture === texture);

      // console.log(src);
      // console.log(texture);
      // console.log(media);
      //9 opposite webgl animate, back element to position
      //const scroll = -media.bounds.top - media.bounds.height / 2 + window.innerHeight / 2;
      //const scroll = -media.bounds.top - media.bounds.height / 2 + window.innerHeight / 2;

       const y = (media.bounds.top - this.scroll.scroll.instance.scroll.y) / window.innerHeight;
      const  scroll = this.sizes.height / 2 - media.mesh.scale.y / 2 - y * this.sizes.height;
      
      //const scroll = getOffset(this.mediasElements[dataIndex]);

      //11 opposite webgl animate,
      //add the update here in order to have the rotation, scale, position values in transition animate, below
      this.update();
      // console.log(media.bounds.top);
      // console.log(this.transition.mesh);
      // console.log(media);

      this.transition.animate(
        {
          position: {
            x: media.mesh.position.x, // was 0
            y: media.mesh.position.y,
            z: 0,
          },
          rotation: media.mesh.rotation, //-12:26 last course  //11 opposite webgl animate, back element to position
          scale: media.mesh.scale,
        },
        (_) => {
          //console.log(media);
          //media.opacity.multiplier = 1;
            // const target = this.medias[dataIndex].element;

            // this.scroll.scrollTo(target, {
            //   offset: scroll,
            //   duration: 0,
            //   easing: [0.87, 0, 0.13, 1],
            // });



          media.program.uniforms.uAlpha.value = 1;
          map(this.medias, (item) => {
          
            if (media !== item) {
              item.show();
            }
          });

          //10 opposite webgl animate, back element to position
          // this.scroll.current =
          //   this.scroll.target =
          //   this.scroll.start =
          //   this.scroll.last =
          //     scroll;
       
     
          //this.scroll.scroll.instance.scroll.y = scroll;
        }
      );
    } else
    {

      map(this.medias, (media) => media.show());
    }

    //3opposite webgl animate
    // if(this.transition){
    //   this.transition.animate(this.medias[0].mesh, _=> {

    //   })
    // }
  }


  hide() {
    map(this.medias, (media) => media.hide());
  }

  /**
   *
   *Events.
   */
  onResize(event) {
    this.bounds = this.elementWrapper.getBoundingClientRect();

    this.sizes = event.sizes; //the vieport of canvas in webgl

    //this.height = (this.bounds.height / window.innerHeight) * this.sizes.height; //  gallery width

    //this.scroll.current = this.scroll.target = 0
    //this.scroll.scroll.instance.scroll.y =  this.scroll.scroll.instance.delta.y = 0
    //console.log(this.scroll.scroll.instance.scroll);
    map(this.medias, (media) =>

      media.onResize(event, this.scroll.scroll.instance.scroll, this.index)


    );

    //map(this.medias, media => media.onResize(event))
  }

  // onTouchDown({ x, y}) {

  //   this.scroll.start = this.scroll.current

  // }
  // onTouchMove({ x, y}) {

  //   const distance = x.start - x.end
  //   this.scroll.target =  this.scroll.start - distance

  // }
  // onTouchUp({ x, y}) {

  // }

  /**
   * Update.
   */
  update(scroll, index)
  {

    //const y = scroll / window.innerHeight;


    //const index = Math.floor(Math.abs((this.scroll.scroll.instance.scroll.y - (this.medias[0].bounds.width / 2)) / this.scroll.scroll.instance.limit.y) * (this.medias.length - 1))


    //  this.scroll.target = GSAP.utils.clamp(-this.scroll.scroll.instance.limit.y, 0, this.scroll.scroll.instance.delta.y)

    // this.scroll.current = GSAP.utils.interpolate(this.scroll.scroll.instance.scroll.y , this.scroll.target, this.scroll.scroll.lerp)


      // if (this.index !== index) {

      //   this.index = index;
      // }

      map(this.medias, (media, index) => {
       // console.log(index);
        //media.update(this.scroll.current * this.scroll.scroll.lerp, this.index); // this.index for extra galleries
        media.update(scroll, this.index); // this.index for extra galleries

      });


    //MOVE THE GROUP, which is the wrapper of all medias and Change the position of each group based on y position

    // we multiply by the.sizes from onResize events, otherwise we get percentage values
    //this.group.position.y = y * this.sizes.height;
  }

  /**
   * Destroy.
   */
  destroy() {
    //destroy canvas elements in ogl after leving the page
    this.scene.removeChild(this.group);
  }
}
