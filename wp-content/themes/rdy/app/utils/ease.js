import { gsap } from 'gsap'
import { CustomEase } from 'gsap/CustomEase'

gsap.registerPlugin(CustomEase)

const easeCustom = {
  ease1: CustomEase.create('ease1', '.45,.01,.07,1'),
  ease2: CustomEase.create('ease2', 'M0,0 C0.496,0.174 0.396,1 1,1'),
  ease3: CustomEase.create('ease3', 'M0,0 C0.35,0.028 0.338,1 1,1'),
  ease4: CustomEase.create('ease4', '.58,.01,.16,1')
}

export default easeCustom
