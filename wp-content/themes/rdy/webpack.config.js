const path = require('path')
const webpack = require('webpack')
const autoprefixer = require("autoprefixer");

const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const TerserPlugin = require('terser-webpack-plugin')
const BrowserSyncPlugin = require('browser-sync-webpack-plugin')

const IS_DEVELOPMENT = process.env.NODE_ENV === 'dev'

const dirApp = path.join(__dirname, 'app')
const dirStyles = path.join(__dirname, 'styles')
const dirNode = 'node_modules'

module.exports = {
  // entry: {
  //   home: [
  //     path.join(dirApp, 'index.js'),
  //     path.join(dirStyles, 'pages/home/home.scss')
  //   ],
  //   about: [
  //     path.join(dirApp, 'index.js'),
  //     path.join(dirStyles, 'pages/about/about.scss')
  //   ],
  //   projects: [
  //     path.join(dirApp, 'index.js'),
  //     path.join(dirStyles, 'pages/projects/projects.scss')
  //   ],
  //   sproject: [
  //     path.join(dirApp, 'index.js'),
  //     path.join(dirStyles, 'pages/sproject/sproject.scss')
  //   ]
  // },
  entry: [path.join(dirApp, "index.js"), path.join(dirStyles, "index.scss")],

  output: {
    //filename: '[name].[contenthash].js'
    filename: "[name].js",
  },

  resolve: {
    modules: [dirApp, dirStyles, dirNode],
  },

  plugins: [
    new webpack.DefinePlugin({
      IS_DEVELOPMENT,
    }),

    new webpack.LoaderOptionsPlugin({
      options: {
        postcss: [autoprefixer()],
      },
    }),

    new MiniCssExtractPlugin({
      filename: "[name].css",
      chunkFilename: "[id].css",
    }),

    new CleanWebpackPlugin(),

    new BrowserSyncPlugin({
      files: "**/*.php",
      proxy: "http://localhost/rdy-canvas-theme", // your dev server here
    }),
  ],

  module: {
    rules: [
      {
        test: /\.js$/,
        use: {
          loader: "babel-loader",
        },
      },

      {
        test: /\.scss$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              publicPath: "",
            },
          },

          {
            loader: "css-loader",
          },
          {
            loader: "postcss-loader",
          },
          {
            loader: "sass-loader",
          },
        ],
      },
      {
        test: /\.(jpe?g|png|gif|webp|svg|fnt)$/,
        loader: "file-loader",
      },

      {
        test: /\.(woff2?)$/,
        loader: "file-loader",
        options: {
          name(file) {
            return "[name].[ext]";
          },
        },
      },

      /*
            {
                test: /\.(jpe?g|png|gif|svg|webp)$/i,
                use: [
                    {
                        loader: ImageMinimizerPlugin.loader,
                        options: {
                            severityError: "warning", // Ignore errors on corrupted images
                            minimizerOptions: {
                                plugins: ["gifsicle"],
                            },
                        },
                    },
                ],
            }, */
      {
        test: /\.(glsl|frag|vert)$/,
        loader: "raw-loader",
        exclude: /node_modules/,
      },
      {
        test: /\.(glsl|frag|vert)$/,
        loader: "glslify-loader",
        exclude: /node_modules/,
      },
    ],
  },

  optimization: {
   minimizer: [
      new TerserPlugin({
        minify: TerserPlugin.uglifyJsMinify,
        // `terserOptions` options will be passed to `uglify-js`
        // Link to options - https://github.com/mishoo/UglifyJS#minify-options
        terserOptions: {},
      }),
    ],
  },
};
