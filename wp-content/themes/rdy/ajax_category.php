<?php
header('Content-Type: text/html; charset=utf-8');

define('WP_USE_THEMES', false);
//include('../wp-blog-header.php');
include('../../../wp-load.php');

//include('functions_site.php');

if (!my_is_ajax()){
    die();
}


$cat_id = (isset($_POST['id']) && is_numeric($_POST['id']) && $_POST['id']>0) ? quote_smart($_POST['id'], true) : 0;
if ($cat_id == 0){
    echo json_encode($reply);
    die();
}

/*
$lang = 'el';
if (isset($_POST['lang']) && $_POST['lang']!=''){
    $lang = quote_smart($_POST['lang'], true);
}

global $sitepress;
$sitepress->switch_lang($lang, true);*/

if($cat_id==999){
    $projects = new WP_Query( array(
        'post_type' => 'projects',
        'post_status' => 'publish',
        'posts_per_page' => -1,
        'order' => 'ASC',
        'orderby' => 'menu_order', 
        'update_post_term_cache' => false, // don't retrieve post terms
        'update_post_meta_cache' => false, // don't retrieve post meta
        'nopaging' => true,
        'ignore_sticky_posts' => true,
        'no_found_rows' => true,  
      ));
}else {
    $projects = new WP_Query( array(
        'post_type' => 'projects',
        'post_status' => 'publish',
        'posts_per_page' => -1,
        'order' => 'ASC',
        'orderby' => 'menu_order', 
        'update_post_term_cache' => false, // don't retrieve post terms
        'update_post_meta_cache' => false, // don't retrieve post meta
        'nopaging' => true,
        'ignore_sticky_posts' => true,
        'no_found_rows' => true,  
        
        'tax_query' => array(
            array(
                'taxonomy'  => 'project-categories',
                'field'     => 'cat_id',
                'terms'     => $cat_id
            )
        )
      ));
}


  global $ajax_call;
  $ajax_call = true;
  ?><?php

  while ($projects->have_posts()){
    $projects->the_post();
    $project_id = get_the_ID();
    $full_image = wp_get_attachment_image_src( get_post_thumbnail_id( $project_id ), "full" );
    $image_w = $full_image[1];
  
 
    ?>
      <div class="js-grid-item">
        <div class="gutter">
        <?php include(locate_template('project-content.php', false, false)); ?>
      </div>
      </div>
    
   
    <?php
  } 


die();
?>