<?php
global $websiteTitle, $schemaTags ,$ajax_call;

/**
 * ACF Fields.
 */
$title =get_the_title();
$text = get_the_excerpt();
/**
 * Image.
 */
$img_url = $full_image[0];
$image_h = $full_image[2];
$image_w = $full_image[1];
$ratio = ($image_h * 100) / $image_w;




/**
 * Link.
 */

$hp_project_link = get_the_permalink();



?><div class="hp-project" itemscope itemtype="http://schema.org/CreativeWork">
  <?php

    if(!empty($hp_project_link)) {
      ?><a itemprop="url" href="<?php echo $hp_project_link; ?>" class="hp-project__wrapper hp-project__link relative cursor" style="padding-top:<?php echo $ratio ?>%;">
          <?php 
          if($ajax_call){
            ?><img itemprop="image" src="<?php echo $img_url; ?>" class="lazy-image" width="<?php echo $image_w; ?>" height="<?php echo $image_h; ?>" alt="<?php echo $websiteTitle.'-'.$title; ?>"><?php
          }else {
            ?><img itemprop="image" data-src="<?php echo $img_url; ?>" class="lazy lazy-image" width="<?php echo $image_w; ?>" height="<?php echo $image_h; ?>" alt="<?php echo $websiteTitle.'-'.$title; ?>"><?php
          }
            if(!empty($text) || !empty($title)) {
              ?><span class="hp-project__info">
                  <span class="hp-project__info__overlay display-block"></span>
                  <span class="hp-project__info__inner display-block">
                  <?php

                    if(!empty($title)) {
                      ?><h3 class="hp-project__info__title"><?php echo $title; ?></h3><?php
                    }
                     if(!empty($text)) {
                      ?><span class="hp-project__info__text display-block"><?php echo $text; ?></span><?php
                    }

                  ?>
                  </span>

              </span><?php
            }
          ?>
    </a><?php
    }
  ?>


    <?php

    //=========== SCHEMA ==========

    ?>
    <span itemprop="dateModified" content="<?php echo $post->post_modified; ?>"></span>
    <span itemprop="author publisher" itemscope itemtype="http://schema.org/Corporation">
      <span itemprop="name" content="<?php echo $websiteTitle; ?>"></span>
       <span class="hidden" itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
            <img itemprop="url" src="<?php echo $schemaTags['image']; ?>" alt="logo">
            <meta itemprop="width" content="260">
            <meta itemprop="height" content="251">
        </span>
    </span>
</div>
