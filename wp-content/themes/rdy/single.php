<?php

/**
 * The Template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

global $websiteTitle, $device, $template_url, $gallery;

get_header();

while (have_posts()) : the_post();
  $thispage = $post;
  $thispage_id = $post->ID;

  $title = get_the_title();
  $details_repeater = get_field('details_repeater', $thispage_id);
  $info_repeater = get_field('info_repeater', $thispage_id);
  $gallery_projects = get_field('gallery_projects', $thispage_id);
  $extra_content = get_field('extra_content', $thispage_id);
  $gallery = get_field('gallery', $thispage_id);
  if (isset($gallery[0]['url'])) {
    $full_image = $gallery[0]['url'];
  }


?><main id="content" class="main-content" data-template="detail" data-scroll-container itemscope="" itemtype="http://schema.org/CreativeWork">
    <div class="sprojects relative detail__media__image" data-src="<?php echo $full_image; ?>">
      <img itemprop="image" alt="<?php echo $websiteTitle . '-' . $title; ?>" class=" img" src="<?php echo $full_image; ?>" />
    </div>
    <section id="section-2" class="section-2">
      <div class="col col-12">
        <div class="gutter">
          <header>
            <h1 class="sproject__title title-72 font-light" itemprop="name" data-animation="title" data-delay="0.15"><?php echo get_the_title(); ?></h1>
          </header>
          <div class="flex sproject__inner">
            <div class="sproject__content">

              <div class="sproject__content-inner" data-animation="title" data-delay="0.2">
                <?php echo get_the_content(); ?>
              </div>
            </div>
            <div class="sproject_details">
              <?php
              if (!empty($details_repeater)) {
              ?><div class="sproject_detail-inner">
                  <?php
                  foreach ($details_repeater as  $counter => $detail) {

                  ?><div class="sproject_detail" data-animation="title" data-delay="<?php echo '0.' . $counter; ?>"><?php echo '<strong>' . do_uppercase($detail['highlight_label']) . ': </strong>' . $detail['highlight_text']; ?></div><?php
                                                                                                                                                                                                                                        }
                                                                                                                                                                                                                                          ?>
                </div><?php
                    }
                    foreach ($info_repeater as $info) {
                      ?><div class="sproject_info-label" data-animation="title"><strong><?php echo do_uppercase($info['info_label']); ?></strong></div>
                <div class="sproject_info-text" data-animation="title" data-delay=".2"><?php echo $info['info_text']; ?></div><?php
                                                                                                                            }
                                                                                                                              ?>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section>
      <?php
      if (!empty($gallery_projects)) {
      ?><div class="col col-12">
          <div class="grid gallery_project">
            <div id="js-grid" class="js-grid">
              <?php
              foreach ($gallery_projects as $counter => $gallery_project) {
                $delay = ($counter % 2) ? "0.2" : "0.0";
                $image_h = $gallery_project['height'];
                $image_w = $gallery_project['width'];
                $ratio = ($image_h * 100) / $image_w;
              ?>
                <div class="js-grid-item" data-animation="image" data-delay="<?php echo $delay; ?>">
                  <div class="gutter">
                    <div style="padding-top:<?php echo $ratio ?>%;" class="relative">
                      <img data-src="<?php echo $gallery_project['url']; ?>" class="lazy lazy-image" width="<?php echo $image_w; ?>" height="<?php echo $image_h; ?>" alt="<?php echo $websiteTitle . '-' . $title; ?>">
                    </div>
                  </div>
                </div>
              <?php
              } ?>
            </div>
          </div>
        </div><?php
            }
            if (!empty($extra_content)) {
              ?><div class="col col-12">
          <div class="gutter">
            <div class="extra-content" data-animation="title">
              <?php echo $extra_content; ?>
            </div>
          </div>
        </div><?php
            }
              ?>
    </section>

  </main>

<?php

/*
wp_reset_postdata();*/

endwhile;

get_footer();
