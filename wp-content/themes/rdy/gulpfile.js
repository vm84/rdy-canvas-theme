/*READ: https://css-tricks.com/gulp-for-beginners/

to start:

ONLY ONCE:

- install Node.js
- open up CMD terminal
###- install gulp by running:  npm install gulp -g      ??

FOR EVERY PROJECT:

- Go to your project/theme folder. At the root you will need to place :
  package.json
  gulpfile.js
- on the terminal go to your directory. eg
    cd E:\htdocs\fasianos\wp-content\themes\fasianos
    (where the above files are placed.)
- run:  npm install gulp --save-dev
- install plugins as mentioned below:
*/

var gulp = require('gulp');

/*
INSTALL PLUGINS

npm install gulp-compass --save-dev
npm install gulp-rename --save-dev
npm install --save-dev del
npm install gulp-concat --save-dev
npm install gulp-uglify --save-dev
npm install --save-dev pump
npm install --save-dev gulp-image

npm install --save-dev gulp-svg-sprite
npm install --save-dev gulp-svgmin

OR all at once:

npm install --save-dev gulp-compass gulp-rename gulp-concat gulp-uglify pump gulp-image del gulp-real-favicon gulp-svg-sprite gulp-svgmin

-------------

*** BUG fixes ***
npm audit fix
npm update minimatch --depth 10
npm update debug
npm update tunnel-agent

npm audit fix


ERRORS:

npm i -g gulp-cli
npm install -g npm


-------------

src		 where we put the name of the file we want to work with and use as an input,
pipe	 will take output of the previous command as pipe it as an input for the next,
dest 	writes the output of previous commands to the folder we specify.s


https://www.liquidlight.co.uk/blog/article/how-do-i-update-to-gulp-4/
*/

var compass = require('gulp-compass'),
    rename = require("gulp-rename"),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    pump = require('pump'),
    image = require('gulp-image'),
    del = require('del'),
	
	svgSprite = require('gulp-svg-sprite'),
    svgmin = require('gulp-svgmin');

var realFavicon = require ('gulp-real-favicon');
var fs = require('fs');


//============ SVG ======

//gulp svg-omg

gulp.task('svg-omg', function () {
    return gulp.src('./images/svg/**/*.svg')
        .pipe(svgmin({
            //https://github.com/svg/svgo/tree/master/plugins
            plugins: [
                {
                    removeDimensions: false
                }, {
                    removeViewBox: false
                }, {
                    cleanupNumericValues: {
                        floatPrecision: 2
                    }
                }
            ]
        }))
        .pipe(gulp.dest('./images/svg-optimized'));
});


//gulp svg_sprite

svgsprite_config = {
    mode: {
        css: { // Activate the «css» mode
            render: {
                scss: false // Activate SCSS output (with default options)
            }
        },
        //symbol: true // Activate the «symbol» mode
        symbol: {
            dest: './images/svg-sprites',
            example: true
        }

        /*symbol: { // symbol mode to build the SVG
          render: {
            css: false, // CSS output option for icon sizing
            scss: false // SCSS output option for icon sizing
          },
          dest: 'sprite', // destination folder
          prefix: '.svg--%s', // BEM-style prefix if styles rendered
          sprite: 'sprite.svg', //generated sprite name
          example: true // Build a sample page, please!
        }*/
    }
};

gulp.task('svg_sprite', function () {
    return gulp.src('./images/svg-optimized/**/*.svg')
        .pipe(svgSprite(svgsprite_config))
        .pipe(gulp.dest('./images/svg-sprites'));
});


//============ Compass
//gulp compass

/*gulp.task('run_compass', function() {
    return gulp.src('./styles/main/!*.scss')
        .pipe(compass({
            config_file: './styles/config.rb',
            css: 'styles/css',
            sass: 'styles/main'
            //import_path
        }))
        .pipe(rename("main.v.1.0.1.css"))       //### VERSIONING HERE!!!
        .pipe(gulp.dest('styles/css'));
});

gulp.task('compass', ['run_compass'], function () {
    //first run_compass and then delete the unstaged css file
    return del(['styles/css/main.css']);
});*/

//============ concatenate & minify JS files
//gulp js

gulp.task('js', function (cb) {
    pump([
        gulp.src(['js/blankshield.min.js', 'js/hammer.min.js', 'js/TweenMax.min.js', 'js/jquery-gsapSlider.min.js', 'js/functions.js', 'js/main.js']),
        //or gulp.src('js/*.js'),
        uglify(),
        concat('app.min.1.0.1.js'),   //### VERSIONING HERE!!!
        gulp.dest('js')
    ], cb);
    //pump notifies for errors
});

//=========== optimize images
//gulp images

gulp.task('images', function () {
    gulp.src('./images/*.{jpg,png,jpeg,gif}')
        .pipe(image({
            pngquant: true,
            optipng: true,
            zopflipng: false,
            jpegRecompress: true,
            mozjpeg: true,
            guetzli: false,
            gifsicle: true,
            svgo: false,
            concurrent: 10
        }))
        .pipe(gulp.dest('./images'));
});

//gulp uploads

gulp.task('uploads', function () {
    gulp.src('../../uploads/*.{jpg,png,jpeg,gif}')
        .pipe(image({
            pngquant: true,
            optipng: true,
            zopflipng: false,
            jpegRecompress: true,
            mozjpeg: true,
            guetzli: false,
            gifsicle: true,
            svgo: false,
            concurrent: 10
        }))
        .pipe(gulp.dest('../../uploads'));
});

//gulp optim

/*gulp.task('optim', ['images', 'uploads']);*/

//========== Create Favicon
//gulp generate-favicon
//after, you should update the theme color in header.php of your theme: <meta name="theme-color" content="#30b1de">

// File where the favicon markups are stored
var FAVICON_DATA_FILE = 'faviconData.json';

var favicon_settings = {
    project_name: 'Fasianos',
    logo_260x260_path: 'images/favicon.png',
    generated_favicons_destination: '../../../',
    favicon_background_color: '#ffffff',
    theme_color: '#355f9e'
};

// Generate the icons. This task takes a few seconds to complete.
// You should run it at least once to create the icons. Then,
// you should run it whenever RealFaviconGenerator updates its
// package (see the check-for-favicon-update task below).
gulp.task('generate-favicon', function(done) {
    realFavicon.generateFavicon({
        masterPicture: favicon_settings.logo_260x260_path,
        dest: favicon_settings.generated_favicons_destination,
        iconsPath: '/',
        design: {
            ios: {
                pictureAspect: 'backgroundAndMargin',
                backgroundColor: favicon_settings.favicon_background_color,
                margin: '14%',
                assets: {
                    ios6AndPriorIcons: false,
                    ios7AndLaterIcons: true,
                    precomposedIcons: false,
                    declareOnlyDefaultIcon: true
                }
            },
            desktopBrowser: {},
            windows: {
                pictureAspect: 'whiteSilhouette',
                backgroundColor: favicon_settings.theme_color,
                onConflict: 'override',
                assets: {
                    windows80Ie10Tile: false,
                    windows10Ie11EdgeTiles: {
                        small: false,
                        medium: true,
                        big: false,
                        rectangle: false
                    }
                }
            },
            androidChrome: {
                pictureAspect: 'noChange',
                themeColor: favicon_settings.theme_color,
                manifest: {
                    name: favicon_settings.project_name,
                    display: 'browser',
                    orientation: 'notSet',
                    onConflict: 'override',
                    declared: true
                },
                assets: {
                    legacyIcon: false,
                    lowResolutionIcons: false
                }
            },
            safariPinnedTab: {
                pictureAspect: 'silhouette',
                themeColor: favicon_settings.theme_color
            }
        },
        settings: {
            compression: 3,
            scalingAlgorithm: 'Mitchell',
            errorOnImageTooSmall: false
        },
        markupFile: FAVICON_DATA_FILE
    }, function() {
        done();
    });
});

//=========== WATCH any change and run specified task
//gulp watch
/*
gulp.task('watch', function() {
    gulp.watch('js/!*.js', ['js']);
    gulp.watch('images/!**', ['images']);
    gulp.watch(['styles/main/!*.scss', 'styles/partials/!*.scss', 'styles/vendor/!*.scss'], ['compass']);
});*/

//====== WATCH ONLY for css changes
//gulp watch:css

/*gulp.task('watch:css', function() {
    gulp.watch(['styles/main/!*.scss', 'styles/partials/!*.scss', 'styles/vendor/!*.scss'], ['compass']);
});*/

//============ run ALL tasks once
//gulp

/*gulp.task('default', ['compass', 'js', 'images']);*/

gulp.task(
    'default',
    gulp.parallel('js', 'images')
);