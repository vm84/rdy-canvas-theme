<?php
/**
 * The template for displaying 404 pages (Not Found)
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

global $hassocial,$footer_color,$header_color, $disable_footer ;
$header_color =   'color-pink';
$footer_color =  'color-green';
$hassocial = false;
$disable_footer = true;


get_header(); ?>

    

<?php get_footer(); ?>
