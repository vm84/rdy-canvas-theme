<?php

/**
 * Template Name: Home
 * Template Post Type: page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

global $websiteTitle, $device, $template_url;

get_header();

?><div id="content" class="main-content" data-template="home"><?php
while (have_posts()) : the_post();
    $thispage = $post;
    $thispage_id = $post->ID;

    $enable_video_intro = get_field('has_video', $thispage_id);
    $video_file = get_field('video_url', $thispage_id);

    /**
     * Image.
     */
    $full_image = my_get_post_thumb($thispage_id);
    //$med_image = my_get_post_thumb($thispage_id, 'medium');
    //$thumb_image = my_get_post_thumb($thispage_id, 'thumbnail');

    ?><main>
<div id="hp-intro" class="intro">
    <div class="full-height relative intro__wrapper">
        <?php

        if ((empty($enable_video_intro))) {

            ?><div class="cover opc-0 hp-intro__image" data-animation="image-scale">
            <div class="intro__overlay cover"></div>
            <!-- <div class="cover" style="background-image: url(<?php echo $bg_image; ?>)"></div> -->
            <picture class="cover">

                <img alt="<?php echo $websiteTitle . '-' . get_the_title(); ?>" class="fit-image" src="<?php echo $full_image; ?>" data-src="<?php echo $full_image; ?>" />
            </picture>
            </div>

        <?php

        } else {
            if (!empty($video_file)) {
                if ($device == 'desktop') {
                    ?>
                    <div class="embed-container embed-container--auto">
                        <video class="overflow video-inner" muted="muted" loop id="file-video">
                            <source src="<?php echo $video_file; ?>" type="video/mp4">
                            Your browser does not support the <code>video</code> element.
                        </video>
                    </div>
                <?php
                } else {

                    ?><div class="cover opc-0 hp-intro__image" data-animation="image-scale">
                    <div class="intro__overlay cover"></div>
                    <div class="cover" style="background-image: url(<?php echo $bg_image; ?>)"></div>
                    </div><?php
                }
            }
        }
        if ($device === 'mobile') {
            $offset = 100;
        } else {
            $offset = 0;
        }

        ?>
        <div id="scroll-to-target" class="scroll-to" data-target="#section-2" data-offset="<?php echo $offset; ?>">
            <div class="scroll-to__icon">
                <?php include(locate_template('elements/svg/arrow-down.php', false, false)); ?>
            </div>

        </div>
    </div>
</div>
<?php


$home_about_image = get_field('home_about_image', $thispage_id);
$home_about_title = get_field('home_about_title', $thispage_id);
$home_about_link_label = get_field('home_about_link_label', $thispage_id);
$home_about_link = get_field('home_about_link', $thispage_id);

$img_url = $home_about_image['url'];
$image_h = $home_about_image['height'];
$image_w = $home_about_image['width'];
$ratio = ($image_h * 100) / $image_w;

if (!empty($home_about_image)) {


    ?><section id="section-2" class="hp-about">
    <div class="hp-about__wrapper">
        <div class="hp-about__wrapper__image relative" data-animation="title">
            <picture style="padding-top: <?php echo $ratio; ?>%">
                <img class="lazy lazy-image" width="<?php echo $image_w; ?>" height="<?php echo $image_h; ?>" data-src="<?php echo $img_url; ?>" alt="<?php echo $websiteTitle . '-' . $home_about_title; ?>">
            </picture>
        </div>
        <div class="hp-about__content" data-animation="title">
            <header class="hp-about__content__header">
                <h1 class="title-72 font-light hp-about__content__title" data-animation="title"><?php echo $home_about_title; ?></h1>
            </header>
            <div class="hp-about__content__txt " data-animation="title" data-delay=".15">
                <?php echo get_the_content(); ?>
            </div>
            <?php

            if (!empty($home_about_link)) {
                ?><div data-animation="title" data-delay=".2">
                <a href="<?php echo $home_about_link; ?>" class="link-underline link-underline--black link-underline--active color--black button-link "><?php echo do_uppercase($home_about_link_label); ?></a>
                </div><?php
            }

            ?>
        </div>
    </div>
    </section><?php
}

/**
 * Projects
 */

//hp_projects_title, hp_projects_text, hp_projects_link_label, hp_projects_link, hp_projects_repeater

$hp_projects_title = get_field('hp_projects_title', $thispage_id);
$hp_projects_text = get_field('hp_projects_text', $thispage_id);
$hp_projects_link_label = get_field('hp_projects_link_label', $thispage_id);
$hp_projects_link = get_field('hp_projects_link', $thispage_id);
$hp_projects_repeater = get_field('hp_projects_repeater', $thispage_id);

?><section class="hp-projects">
    <div class="col col-12">
        <div class="gutter">
            <div class="hp-projects__wrapper">
                <div class="hp-projects__wrapper__left">
                    <?php


                        ?><header class="hp-projects__header">
                        <?php
                        if (!empty($hp_projects_title)) {
                            ?><h2 class="hp-projects__header__title" data-animation="title"><?php echo $hp_projects_title; ?>
                            </h2><?php
                        }
                        if (!empty($hp_projects_text)) {
                            ?><div class="hp-projects__header__text" data-animation="title" data-delay=".15"><?php echo $hp_projects_text; ?></div><?php
                        }

                        if (!empty($hp_projects_link)) {
                            ?><div data-animation="title" data-delay=".2"><a href="<?php echo $hp_projects_link; ?>" class="link-underline link-underline--black link-underline--active color--black button-link"><?php echo do_uppercase($hp_projects_link_label); ?></a></div><?php
                        }
                        ?>
                        </header><?php
                    ?>
                </div>
                <div class="hp-projects__wrapper__right" data-animation="image">
                    <?php

                    if ($device === 'mobile') {
                        ?><header class="hp-projects__header hp-projects__header-mobile">
                        <?php
                        if (!empty($hp_projects_title)) {
                            ?><h2 class="hp-projects__header__title" data-animation="title">
                            <?php echo $hp_projects_title; ?></h2><?php
                        }
                        if (!empty($hp_projects_text)) {
                            ?><div class="hp-projects__header__text" data-animation="title"><?php echo $hp_projects_text; ?></div><?php
                        }

                        if (!empty($hp_projects_link)) {
                            ?><div data-animation="title" data-delay=".2"><a href="<?php echo $hp_projects_link; ?>" class="link-underline link-underline--black link-underline--active color--black button-link"><?php echo do_uppercase($hp_projects_link_label); ?></a></div><?php
                        }
                        ?>
                        </header><?php
                    }

                    foreach ($hp_projects_repeater as $counter => $project) {

                      include(locate_template('hp-project.php', false, false));

                    }

                    ?>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="section-2" class="hp-about">
    <div class="hp-about__wrapper">
        <div class="hp-about__wrapper__image relative" data-animation="title">
            <picture style="padding-top: <?php echo $ratio; ?>%">
                <img class="lazy lazy-image" width="<?php echo $image_w; ?>" height="<?php echo $image_h; ?>" data-src="<?php echo $img_url; ?>" alt="<?php echo $websiteTitle . '-' . $home_about_title; ?>">
            </picture>
        </div>
        <div class="hp-about__content" data-animation="title">
            <header class="hp-about__content__header">
                <h1 class="title-72 font-light hp-about__content__title" data-animation="title"><?php echo $home_about_title; ?></h1>
            </header>
            <div class="hp-about__content__txt " data-animation="title" data-delay=".15">
                <?php echo get_the_content(); ?>
            </div>
            <?php

            if (!empty($home_about_link)) {
                ?><div data-animation="title" data-delay=".2">
                <a href="<?php echo $home_about_link; ?>" class="link-underline link-underline--black link-underline--active color--black button-link "><?php echo do_uppercase($home_about_link_label); ?></a>
                </div><?php
            }

            ?>
        </div>
    </div>
</section>
<!-- <section class="hp-projects">
    <div class="col col-12">
        <div class="gutter">
            <div class="hp-projects__wrapper">
                <div class="hp-projects__wrapper__left">
                    <?php


                        ?><header class="hp-projects__header">
                        <?php
                        if (!empty($hp_projects_title)) {
                            ?><h2 class="hp-projects__header__title" data-animation="title"><?php echo $hp_projects_title; ?>
                            </h2><?php
                        }
                        if (!empty($hp_projects_text)) {
                            ?><div class="hp-projects__header__text" data-animation="title" data-delay=".15"><?php echo $hp_projects_text; ?></div><?php
                        }

                        if (!empty($hp_projects_link)) {
                            ?><div data-animation="title" data-delay=".2"><a href="<?php echo $hp_projects_link; ?>" class="link-underline link-underline--black link-underline--active color--black button-link"><?php echo do_uppercase($hp_projects_link_label); ?></a></div><?php
                        }
                        ?>
                        </header><?php
                    ?>
                </div>
                <div class="hp-projects__wrapper__right" data-animation="image">
                    <?php

                    if ($device === 'mobile') {
                        ?><header class="hp-projects__header hp-projects__header-mobile">
                        <?php
                        if (!empty($hp_projects_title)) {
                            ?><h2 class="hp-projects__header__title" data-animation="title">
                            <?php echo $hp_projects_title; ?></h2><?php
                        }
                        if (!empty($hp_projects_text)) {
                            ?><div class="hp-projects__header__text" data-animation="title"><?php echo $hp_projects_text; ?></div><?php
                        }

                        if (!empty($hp_projects_link)) {
                            ?><div data-animation="title" data-delay=".2"><a href="<?php echo $hp_projects_link; ?>" class="link-underline link-underline--black link-underline--active color--black button-link"><?php echo do_uppercase($hp_projects_link_label); ?></a></div><?php
                        }
                        ?>
                        </header><?php
                    }
                    $counter = 10;
                    foreach ($hp_projects_repeater as $project) {

                      include(locate_template('hp-project.php', false, false));
                    $counter++;
                    }
                    

                    ?>
                </div>
            </div>
        </div>
    </div>
</section> -->







    </main><?php

    /*
wp_reset_postdata();*/

endwhile;

get_footer();
?></div><?php
