<?php

/**
 * Template Name: Projects
 * Template Post Type: page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

global $websiteTitle, $device, $template_url;

get_header();
?><div id="content" class="main-content" data-template="projects"><?php

			while (have_posts()) : the_post();
				$thispage = $post;
				$thispage_id = $post->ID;

				$title = get_field('top_title', $thispage_id);
				$full_image = my_get_post_thumb($thispage_id);


				//$bg_image = my_get_post_thumb($thispage_id);
				//$top_title = get_field('page_top_title', $thispage_id);


			?><main>
			<div class="projects relative">
				<div class="projects__bg"></div>
				<div class="col col-12 ">
					<div class="gutter">
						<div class="projects__wrapper">
							<picture class="projects__wrapper__image">
								<source media="(min-width: 1200px)" data-srcset="<?php echo $full_image; ?> 1x, <?php echo $full_image; ?>.jpg 2x" />
								<!-- <source media="(min-width: 800px)" data-srcset="<?php echo $med_image; ?> 1x, <?php echo $full_image; ?> 2x" />
                <source media="(min-width: 100px)" data-srcset="<?php echo $thumb_image; ?> 1x, <?php echo $med_image; ?> 2x" /> -->
								<img alt="<?php echo $websiteTitle . '-' . $title; ?>" class="fit-image" src="<?php echo $full_image; ?>" data-animation="image" />
							</picture>

							<div class="projects__details">
								<h1 class="projects__details__title" data-animation="title"><?php echo $title; ?></h1>
								<div class="projects__details__text" data-animation="title" data-delay=".15">
									<?php echo get_the_content() ?>
								</div>
							</div>
							<div id="scroll-to-target" class="scroll-to" data-target="#section-2" data-offset="0" data-offset="0">
								<div class="scroll-to__icon">
									<?php include(locate_template('elements/svg/arrow-down.php', false, false)); ?>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
			<?php

				$project_categories = get_terms(array(
					'taxonomy' => 'project-categories',
					'orderby' => 'name',
					'order'   => 'ASC',
					'hide_empty'  => true,
					'hierarchical'  => true,
					'update_term_meta_cache' => false
				));

				/***** PROJECTS *****/
				$projects = new WP_Query(array(
					'post_type' => 'projects',
					'post_status' => 'publish',
					'posts_per_page' => -1,
					'order' => 'ASC',
					'orderby' => 'menu_order',
					'update_post_term_cache' => false, // don't retrieve post terms
					'update_post_meta_cache' => false, // don't retrieve post meta
					'nopaging' => true,
					'ignore_sticky_posts' => true,
					'no_found_rows' => true,
				));



				if ($projects->have_posts()) { ?>
				<section id="section-2" class="projects-list section-2">
					<div class="col col-12">
						<div class="gutter">
							<div class="projects__filter">
								<div class="projects__filter-holder active js-projects__filter" data-id="999">
									<div class="projects__filter-item">ALL</div>
								</div>
								<?php
																																			foreach ($project_categories as $cat) {
								?><div data-id=<?php echo $cat->term_id; ?> class="projects__filter-holder js-projects__filter">
										<div class="projects__filter-item"><?php echo do_uppercase($cat->name); ?></div>
									</div><?php
																																			}
												?>
							</div>
						</div>
						<div class="gutter-tablet">
							<div id="js-grid" class="js-grid"><?php
																																			while ($projects->have_posts()) {
																																				$projects->the_post();
																																				$project_id = get_the_ID();
																																				$full_image = wp_get_attachment_image_src(get_post_thumbnail_id($project_id), "full");
																																				$image_w = $full_image[1];
																								?>
									<div class="js-grid-item" data-animation="image">
										<div class="gutter gutter-grid">
											<?php include(locate_template('project-content.php', false, false)); ?>
										</div>
									</div>
								<?php } ?>
							</div>
						</div>
					</div>
				</section><?php
																																		}
																																		wp_reset_postdata();

									?>







		</main><?php

																																	/*
wp_reset_postdata();*/

																																	endwhile;

																																	get_footer();
						?></div><?php
