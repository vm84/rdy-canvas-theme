<?php

/**
 * Template Name: About
 * Template Post Type: page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

global $websiteTitle, $has_footer, $extra_body_class;
$has_footer = false;
$extra_body_class = 'header-blue';

get_header();
?><div id="content" class="main-content" data-template="about"><?php

while (have_posts()) : the_post();
  $thispage = $post;
  $thispage_id = $post->ID;

  $title = get_the_title();
  $full_image = my_get_post_thumb($thispage_id);
  //$med_image = my_get_post_thumb($thispage_id, 'medium');
  //$thumb_image = my_get_post_thumb($thispage_id, 'thumbnail');

  $extra_content = get_field('extra_content', $thispage_id);


?><main>
    <div class="about">
      <div class="about__wrapper">
        <picture class="about__wrapper__image overflow">
          <?php /*<source media="(min-width: 1200px)" data-srcset="<?php echo $full_image; ?> 1x,
                <?php echo $full_image; ?>.jpg 2x" />
                <source media="(min-width: 800px)"
                    data-srcset="<?php echo $med_image; ?> 1x, <?php echo $full_image; ?> 2x" />
                <source media="(min-width: 100px)"
                    data-srcset="<?php echo $thumb_image; ?> 1x, <?php echo $med_image; ?> 2x" /> */ ?>
          <img alt="<?php echo $websiteTitle . '-' . $title; ?>" class="fit-image" src="<?php echo $full_image; ?>" data-animation="image-scale" />
        </picture>
        <div class="about__details">
          <h1 class="about__details__title" data-animation="title"><?php echo $title; ?></h1>
          <div class="about__details__text" data-animation="title" data-delay=".15">
            <?php echo get_the_content() ?>
          </div>
          <?php
          if (!empty($extra_content)) {
          ?><div class="about__details__contact" data-animation="title" data-delay=".2">
              <?php echo $extra_content; ?>
            </div><?php
                }
                  ?>
        </div>
      </div>
    </div>
  </main><?php

        /*
wp_reset_postdata();*/

  endwhile;

  get_footer();
  ?></div><?php
