<?php
/**
 * The Header for our theme
 */

global $websiteTitle, $sessionName, $default_lang, $template_url, $schemaTags, $css_version, $socials, $extra_body_class, $is_safari, $is_winIE, $is_gecko, $has_cat;



$lang_ext = ($default_lang==ICL_LANGUAGE_CODE) ? '' : ICL_LANGUAGE_CODE.'/';

$browser = '';

if($is_gecko) {
    $browser = 'mozilla';
}
if($is_winIE) {
    $browser = 'explorer';
}
if($is_safari) {
    $browser = 'safari';
}
$os = '';
$user_agent = getenv("HTTP_USER_AGENT");
if(strpos($user_agent, "Win") !== FALSE)
    $os = "windows";
elseif(strpos($user_agent, "Mac") !== FALSE)
    $os = "mac";

include_once('lib/Mobile_Detect.php');
global $detect;
global $device;
$detect = new Mobile_Detect;


if($detect->isTablet()){
    $device = 'tablet';
}else if ($detect->isMobile()){
    $device = 'mobile';
}else {
    $device = 'desktop';
}

$has_cat = false;

if (isset($_GET['pag']) && !empty($_GET['pag'])&& quote_smart($_GET['pag'])) {
    if($_GET['pag'] == 'cat'){
        $has_cat = true;
    }
}




$is_front_page = is_front_page();

?><!DOCTYPE html>
<html lang="<?php echo $default_lang; ?>" itemscope itemtype="http://schema.org/Corporation">
<head>
    <meta charset="UTF-8">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <meta name="format-detection" content="telephone=no">
    <link rel="dns-prefetch" href="<?php echo WP_HOME; ?>"><link rel="preconnect" href="<?php echo WP_HOME; ?>">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo WP_HOME; ?>/apple-touch-icon.png"><link rel="icon" type="image/png" href="<?php echo WP_HOME; ?>/favicon-32x32.png" sizes="32x32"><link rel="icon" type="image/png" href="<?php echo WP_HOME; ?>/favicon-16x16.png" sizes="16x16"><link rel="manifest" href="<?php echo WP_HOME; ?>/site.webmanifest"><link rel="mask-icon" href="<?php echo WP_HOME; ?>/safari-pinned-tab.svg" color="#5bbad5"><meta name="theme-color" content="#000">


    <?php


      // if(basename(get_page_template()) === 'home.php'){
      //   $css_version = "home";

      // }elseif(  basename(get_page_template()) == 'about.php'){
      //   $css_version = "about";
      // }elseif(  basename(get_page_template()) == 'projects.php'){
      //   $css_version = "projects";
      // }else if(is_single() ){
      //   $css_version = "sproject";
      // }

    ?>

    <link id="<?php echo $css_version; ?>-css" rel="stylesheet" type="text/css" media="all" href="<?php echo $template_url; ?>/public/main.css" />
    <!--[if lt IE 9]>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
    <![endif]-->
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=2, initial-scale=1.0" />
    <!--[if IEMobile]><meta http-equiv="cleartype" content="on" /><![endif]-->
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="theme-color" content="#ffffff"/><?php

    ?>
    <script>
        var websiteURL = '<?php echo WP_HOME; ?>';
        var templateURL = '<?php echo $template_url; ?>/';
        var device = '<?php echo $device; ?>';
    </script>
    <?php wp_head(); ?>
</head>



<body <?php body_class($browser.' '.$device. ' '.$extra_body_class.' '.$os); ?>><?php

/**
 * Preloader
 */

 include(locate_template('elements/preloader.php', false, false));

if (isset($schemaTags)){
    ?><script type="application/ld+json">{ "@context": "https://schema.org", "@type": "Organization", "name": "<?php echo $schemaTags['name']; ?>", "url": "<?php echo $schemaTags['url']; ?>", "legalName": "<?php echo $schemaTags['legalName']; ?>", "logo": "<?php echo $schemaTags['logo']; ?>", "image": "<?php echo $schemaTags['image']; ?>", "description": "<?php echo $schemaTags['description']; ?>", "email": "<?php echo $schemaTags['email']; ?>", "telephone": "<?php echo $schemaTags['telephone']; ?>", "faxNumber": "<?php echo $schemaTags['faxNumber']; ?>", "founder": { "@type": "Person", "name": "<?php echo $schemaTags['founder']; ?>" }, "address": { "@type": "PostalAddress", "addressCountry": "<?php echo $schemaTags['address_array']['country_code']; ?>", "addressRegion": "<?php echo $schemaTags['address_array']['region']; ?>", "addressLocality": "<?php echo $schemaTags['address_array']['locality']; ?>", "postalCode": "<?php echo $schemaTags['address_array']['zipcode']; ?>", "streetAddress": "<?php echo $schemaTags['address_array']['address']; ?>" }, "hasPOS":{ "@type": "Place", "address": { "@type": "PostalAddress", "addressCountry": "<?php echo $schemaTags['address_array']['country_code']; ?>", "addressRegion": "<?php echo $schemaTags['address_array']['region']; ?>", "postalCode": "<?php echo $schemaTags['address_array']['zipcode']; ?>", "streetAddress": "<?php echo $schemaTags['address_array']['address']; ?>" }, "geo":{ "@type":"GeoCoordinates", "address": { "@type": "PostalAddress", "addressCountry": "<?php echo $schemaTags['address_array']['country_code']; ?>", "addressRegion": "<?php echo $schemaTags['address_array']['region']; ?>", "postalCode": "<?php echo $schemaTags['address_array']['zipcode']; ?>", "streetAddress": "<?php echo $schemaTags['address_array']['address']; ?>" }, "latitude": "<?php echo $schemaTags['address_array']['latitude']; ?>", "longitude": "<?php echo $schemaTags['address_array']['longitude']; ?>" } } <?php if (isset($socials)){ ?>,"sameAs":[ <?php $counter = 1; foreach ($socials as $key => &$val){ if ($counter>1){ echo ', '; } ?>"<?php echo $val; ?>"<?php  $counter++; } ?> ]<?php } ?> }
</script><?php

}
?>
<a class="skip-link screen-reader-text" href="#content">Skip to content</a>
<div class="app">
    <header class="header">

          <?php

          if($is_front_page) {

            ?><a class="header__logo" aria-label="<?php echo $websiteTitle; ?>" href="<?php echo WP_HOME. '/' .$lang_ext; ?>"  rel="noopener noreferrer">
              <?php include(locate_template('elements/svg/logo.php', false, false)); ?>
          </a>
          <a id="header__logo-letters" class="header__logo-letters" aria-label="<?php echo $websiteTitle; ?>" href="<?php echo WP_HOME. '/' .$lang_ext; ?>"  rel="noopener noreferrer">
              <?php include(locate_template('elements/svg/logo-letters.php', false, false)); ?>
          </a><?php

          }else {

            ?><a class="header__logo" aria-label="<?php echo $websiteTitle; ?>" href="<?php echo WP_HOME. '/' .$lang_ext; ?>"  rel="noopener noreferrer">
              <?php include(locate_template('elements/svg/logo.php', false, false)); ?>
            </a><?php

          }



          ?>
        <div id="nav-trigger" class="nav cursor">
            <div class="nav__line nav__line__top"></div>
            <div class="nav__line nav__line__med"></div>
            <div class="nav__line nav__line__bottom"></div>
        </div>
    </header>
    <?php

    /**
     * Burger Menu.
     */

    ?><div class="navigation" id="navigation">
        <div class="navigation__overlay"></div>
        <div id="close-nav" class="navigation__close cursor">
          <div class="navigation__close__top"></div>
          <div class="navigation__close__bottom"></div>
        </div>
        <div class="navigation__wrapper">
          <div class="navigation__wrapper__inner">
            <div class="navigation__holder">
              <nav class="navigation__holder__menu">
                <?php

                  wp_nav_menu( array(
                      'theme_location' => 'primary',
                      'menu' => 'MainMenu_'.ICL_LANGUAGE_CODE,
                      'container' => false,
                      'menu_class' => 'menu-ul',
                      'walker' => new MainMenu_myWalker,
                      'menu_to_uppercase'=>true,
                  ));

                ?>
              </nav>
              <div class="languages flex">
                  <div class="languages__text"><?php echo get_field('lang_english','option')?></div>
                  <div class="divider">|</div>
                  <div class="languages__text"><?php echo get_field('lang_greek','option')?></div>
              </div>
            </div>
        </div>
        <div class="contact">
          <?php

              /**
               * Contact.
               */

              $phone = get_field('contact_phone','option');
              $email = get_field('contact_email','option');
              $address = get_field('contact_address','option');
              $address__link = get_field('contact_address_link','option');

              ?><div class="contact__wrapper">
                  <div class="contact__wrapper__info">
                    <?php

                      if(!empty($phone)) {
                        ?><a href="tel:<?php echo $phone; ?>" class="contact__wrapper__phone display-inline"><?php echo $phone; ?></a><?php
                      }
                      if(!empty($email)){
                        ?><a href="mailto:<?php echo $email; ?>" class=" contact__wrapper__email display-inline"><?php echo $email; ?></a><?php
                      }
                      if(!empty($address)){
                        ?><div  class="display-inline contact__wrapper__address"><?php echo $address; ?></div><?php
                      }
                    ?>
                  </div><?php

                  /**
                   * Socials.
                  */

                  $socials = get_field('socials_repeater','option');

                  if(!empty($socials)) {
                    ?><div class="socials"><?php
                        foreach($socials as  &$social) {
                          $title = $social['social_title'];
                          $link = $social['social_link'];

                          if(!empty($link)) {
                            ?><a href="<?php echo $link; ?>" target='_blank' class="social" rel="noopener noreferrer"><?php echo do_uppercase($title); ?></a><?php
                          }
                        }
                    ?></div><?php
                  }
                ?>
              </div>
          </div>
        </div>
    </div>
    <div class="test" id="test"></div>
    <div class="app__wrapper smooth-scroll">


