<?php

/*
 * Custom menu walker
 * */
class MainMenu_myWalker extends Walker_Nav_Menu {
    public function start_lvl( &$output, $depth = 0, $args = array() ) {
        if (!isset($args->only_links) || $args->only_links==false) {
            $output .= "<ul class=\"submenu-list trans-slower\">";
        }
    }
    public function end_lvl( &$output, $depth = 0, $args = array() ) {
        if (!isset($args->only_links) || $args->only_links==false) {
            $output .= "</ul>";
        }
    }

    //public $menu_order = 0;
    public $parent_menu_count = 0;
    public $submenu_count = 0;

    public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
        if (!isset($args->menu_to_uppercase)){
            $args->menu_to_uppercase = true;
            //convert first level to uppercase
        }
        if (!isset($args->submenu_to_uppercase)){
            $args->submenu_to_uppercase = false;
            //convert sub level to uppercase
        }
        //$this->menu_order++;
        if ($item->menu_item_parent==0){
            $this->parent_menu_count++;
            $this->submenu_count = 0;
        }
        else{
            $this->submenu_count++;
        }

        $classes = empty( $item->classes ) ? array() : (array) $item->classes;
        $classes[] = 'menu-item-' . $item->ID;

        $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args, $depth ) );

        if ($item->menu_item_parent==0){
            $class_names .= ' main-item item--'.$item->object_id;
            $class_names .= ' mnu-'.$this->parent_menu_count;
        }
        else{
            $class_names .= ' submenu-item submnu-'.$this->submenu_count;
        }
        $class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';

        $id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args, $depth );
        $id = $id ? ' id="' . esc_attr( $id ) . '"' : '';

        $break_parents_counter = (isset($args->break_parents_counter)) ? $args->break_parents_counter : 4;

        if (isset($args->break_parents) && $args->break_parents && $item->menu_item_parent==0 && $this->parent_menu_count==$break_parents_counter){
            $break_parents_open_divs = (isset($args->break_parents_open_divs)) ? $args->break_parents_open_divs : '';
            $break_parents_close_divs = (isset($args->break_parents_close_divs)) ? $args->break_parents_close_divs : '';

            $output .= '</ul>'.$break_parents_close_divs.$break_parents_open_divs.'<ul class="sitemap-menu">';

            $this->parent_menu_count = 2;
        }


        if (isset($args->break_parents) && in_array('menu-item-has-children', $classes)){
            $output .= '</ul><ul>';
        }


        if (isset($args->break_parents) && in_array('menu-item-has-children', $classes)){
            $output .= '</ul><ul>';
        }

        if (isset($args->only_links) && $args->only_links){
            $output .= '<span' . $id . $class_names .'>';
        }
        else{
            $output .= '<li' . $id . $class_names .'>';
        }

        /*if ($args->menu_class=='mainmenu-ul' && isset($args->logo_link) && $this->parent_menu_count==1){
            //first element: add logo

            $output .= '<a href="'.$args->logo_link.'" class="logo-sm trans-med2"></a>';
        }*/

        $atts = array();
        $atts['title']  = ! empty( $item->attr_title ) ? $item->attr_title : '';
        $atts['target'] = ! empty( $item->target )     ? $item->target     : '';
        $atts['rel']    = ! empty( $item->xfn )        ? $item->xfn        : '';
        $atts['href']   = ! empty( $item->url )        ? $item->url        : '';

        $atts = apply_filters( 'nav_menu_link_attributes', $atts, $item, $args, $depth );

        $menu_link_class = ($item->menu_item_parent==0) ? 'menu-a' : 'submenu-a';
        $menu_link_class .= ' menu-link item--'.$item->object_id;
        $atts['class'] = (isset($atts['class'])) ? $atts['class'].' '.$menu_link_class : $menu_link_class;
        //$atts['class'] = (isset($atts['class'])) ? $atts['class'].' '.$menu_link_class.' menu-a-'.$this->menu_order : $menu_link_class.' menu-a-'.$this->menu_order;

        $attributes = '';
        $attributes_nohref = '';
        foreach ( $atts as $attr => $value ) {
            if ( ! empty( $value ) ) {
                $value = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
                $attributes .= ' ' . $attr . '="' . $value . '"';

                if ($attr!='href'){
                    $attributes_nohref .= ' ' . $attr . '="' . $value . '"';
                }
            }
        }

        $item_output = $args->before;
        if ($atts['href']!='#'){
            $item_output .= '<a'. $attributes .'>';
        }
        else{
            $item_output .= '<span'. $attributes_nohref .'>';
        }

        /*if ($item->menu_item_parent==0){
            $item_output .= sprintf('<img src="%s" alt="'.$item->post_title.'" class="menu-thumb">', get_field('menu_thumb', $item->object_id));
            $item_output .= '<span class="num">'.$this->parent_menu_count.'</span>';
        }*/

        /** This filter is documented in wp-includes/post-template.php */
        /*if (isset($args->theme_location) && $args->theme_location=='secondary'){
            $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
        }*/
        if (($item->menu_item_parent==0 && $args->menu_to_uppercase) || ($item->menu_item_parent>0 && $args->submenu_to_uppercase)){
            $item_output .= $args->link_before . apply_filters( 'the_title', do_uppercase($item->title), $item->ID );
        }
        else{
            $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID );
        }

        if ($args->theme_location == 'primary'){
            /*$category = get_the_category();
            return $category[0]->category_count;*/
            $item_output .= ' <span class="menu-a__mask abs-bl"></span>';
        }

        $item_output .= $args->link_after;
        /*if ($item->menu_item_parent==0){
            $item_output .= '<span class="num trans">'.sprintf("%02d", $this->menu_order).'</span>';
            $this->menu_order += 1;
        }*/

        if ($atts['href']!='#'){
            $item_output .= '</a>';
        }
        else{
            $item_output .= '</span>';
        }
        $item_output .= $args->after;

        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
    }
    public function end_el( &$output, $item, $depth = 0, $args = array() ) {
        if (isset($args->only_links) && $args->only_links){
            $output .= "</span>";
        }
        else{
            $output .= "</li>";
        }
    }
}