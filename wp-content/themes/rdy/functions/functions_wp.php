<?php
// ********** Usefull functions for wordpress ********


/*
 * Get post thumbnail
 * $size: thumbnail, medium, large, full (original)
 * $return_false: if true it will return a default image if no image is found. uses $default_thumb from functions.php
 * $return_array: if true it will return an array containing the image, width and height, for the specified dimensions
 * */
function my_get_post_thumb($post_id, $size='full', $return_default=true, $return_array=false){
    global $default_thumbs_array;

    if ($return_array){
        $reply = array();

        $featured_meta = wp_get_attachment_metadata(get_post_thumbnail_id($post_id));

        if (!empty($featured_meta)){
            $reply = array(
                'width' => $featured_meta['width'],
                'height' => $featured_meta['height'],
                'image' => WP_HOME.'/wp-content/uploads/'.$featured_meta['file'],
            );
            if (!empty($featured_meta['sizes'][$size])){
                $reply = array(
                    'width' => $featured_meta['sizes'][$size]['width'],
                    'height' => $featured_meta['sizes'][$size]['height'],
                    'image' => WP_HOME.'/wp-content/uploads/'.$featured_meta['sizes'][$size]['file'],
                );
            }

        }

        return $reply;
    }

    $img = get_the_post_thumbnail_url($post_id, $size); //get produced thumb
    if (empty($img)){
        //if no thumb produced, get attachment
        $img = wp_get_attachment_url(get_post_thumbnail_id($post_id));
    }
    if (empty($img) && $return_default){
        if ($size=='full'){
            $size = 'large';
        }
        $img = $default_thumbs_array[$size];
    }
    return $img;
}

/*
 * Get ID for page or post, depending on language code
 *
 * eg  $contact_page_id = get_id_for_type_lang(46, 'page', true, ICL_LANGUAGE_CODE);
*/
function get_id_for_type_lang($element_id, $element_type = 'post', $return_original_if_missing = false, $ulanguage_code = null){
    if (function_exists('icl_object_id')) {
        //return icl_object_id($element_id, $element_type, $return_original_if_missing, $ulanguage_code);
        return apply_filters('wpml_object_id', $element_id, $element_type, $return_original_if_missing, $ulanguage_code);
    } else {
        return $element_id;
    }
}


/*
Function to get the primary / first category for post.
$return_all_categories: optionally you may check all categories (for post), to see if specific category-id exists
----
use the return:
$result['primary_category']->name
get_category_link( $result['primary_category']->term_id )

example:
$post_categories = get_post_primary_category($post->ID, 'category', true); //+ if i need to know all category IDs, eg to check if in video category
$primary_category = $post_categories['primary_category'];
<a href="<?php echo get_category_link($primary_category->term_id); ?>" class="post-category display--block"><?php echo do_uppercase($primary_category->name); ?></a>
*/
function get_post_primary_category($post_id, $term='category', $return_all_categories=false){
    $return = array();

    if (class_exists('WPSEO_Primary_Term')){
        // Show Primary category by Yoast if enabled & set
        $wpseo_primary_term = new WPSEO_Primary_Term( $term, $post_id );
        $primary_term = get_term($wpseo_primary_term->get_primary_term());

        if (!is_wp_error($primary_term)){
            $return['primary_category'] = $primary_term;
        }
    }

    if (empty($return['primary_category']) || $return_all_categories){
        $categories_list = get_the_terms($post_id, $term);

        if (empty($return['primary_category']) && !empty($categories_list)){
            $return['primary_category'] = $categories_list[0];
        }
        if ($return_all_categories){
            $return['all_categories'] = array();

            if (!empty($categories_list)){
                foreach($categories_list as &$category){
                    $return['all_categories'][] = $category->term_id;
                }
            }
        }
    }

    return $return;
}

/*
 * Function to get the first term (eg tag or custom taxonomy) for post
 *
 * example:
$primary_term = get_post_primary_term($post->ID, 'taxonomy_name');
<a href="<?php echo get_term_link($primary_term->slug, 'taxonomy_name'); ?>"><?php echo do_uppercase($primary_term->name); ?></a>
 * */
function get_post_primary_term($post_id, $term='taxonomy_name'){
    $terms = get_the_terms($post_id, $term);
    $primary_term = new stdClass();
    if ($terms && !is_wp_error($terms)){
        $primary_term = $terms[0];
    }
    return $primary_term;
}

/*
 * Get all categories IDs for specific post
 * */
function get_post_cat_ids($post_id, $term='category'){
    $categories_list = get_the_terms($post_id, $term);
    $return = array();
    foreach ($categories_list as &$cat){
        $return[] = $cat->term_id;
    }
    return $return;
}


function get_the_page_content($id){
    $content = get_post($id)->post_content;
    $content = apply_filters('the_content', $content);
    $content = str_replace(']]>', ']]>', $content);
    return $content;
}

function my_the_content( $more_link_text = null, $strip_teaser = false) {
    $content = get_the_content( $more_link_text, $strip_teaser );
    $content = apply_filters( 'the_content', $content );
    $content = str_replace( ']]>', ']]&gt;', $content );
    return $content;
}

function my_fix_content($content){
    $content = apply_filters('the_content', $content);
    $content = str_replace(']]>', ']]>', $content);
    return $content;
}
