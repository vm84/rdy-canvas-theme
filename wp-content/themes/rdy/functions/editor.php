<?php
//add custom styles to the WordPress editor (replaces "formats" dropdown)

function my_custom_styles ($init_array) {
    $style_formats = array(
        array(
            'title' => 'Heading 3',
            'block' => 'h3',
            //'classes' => 'small',
            'wrapper' => false,
        ),
        array(
            'title' => 'Heading 4',
            'block' => 'h4',
            //'classes' => 'small',
            'wrapper' => false,
        ),
        array(
            'title' => 'Heading 5',
            'block' => 'h5',
            //'classes' => 'small',
            'wrapper' => false,
        ),
        array(
            'title' => 'Heading 6',
            'block' => 'h6',
            //'classes' => 'small',
            'wrapper' => false,
        ),
        array(
            'title' => 'Small',
            'block' => 'small',
            'classes' => 'small',
            'wrapper' => true,
        ),
        /*array(
            'title' => 'Paragraph',
            'block' => 'p',
            //'classes' => 'small',
            'wrapper' => true,
        ),*/
        array(
            'title' => '"Blockquote"',
            'block' => 'blockquote',
            //'classes' => 'small',
            'wrapper' => true,
        ),
    );
    // Insert the array, JSON ENCODED, into 'style_formats'
    $init_array['style_formats'] = json_encode( $style_formats );

    return $init_array;
}
// Attach callback to 'tiny_mce_before_init'
add_filter( 'tiny_mce_before_init', 'my_custom_styles' );