<?php

function hide_update_notice() {
    //if ( ! current_user_can( 'update_core' ) ) {
        remove_action( 'admin_notices', 'update_nag', 3 );
   // }
}
add_action( 'admin_head', 'hide_update_notice', 1 );

/*
//backend: ACF: show only main projects in post type drop down

/*add_filter('acf/fields/post_object/query/name=project_related', 'my_post_object_query', 10, 3);
function my_post_object_query( $args, $field, $post_id ) {
    $args['post_parent'] = 0;
    $args['post__not_in'] = array($post_id);
    return $args;

}

//ACF: filter related projects to show only the ones that have an inner page (project_has_inner_page): related_projects

add_filter('acf/fields/relationship/query/name=related_projects', 'my_acf_load_active_projects', 10, 3);
function my_acf_load_active_projects( $args, $field, $post_id ) {
    $args['meta_query'] = array(
        array(
            'key'  => 'project_has_inner_page',
            'value'     => 1,
        )
    );
    return $args;
}

*/

/* ================ ACF =============== 

//Add recipe colors in list for ACF: recipe_color

add_filter('acf/load_field/name=recipe_color', 'acf_load_colors_choices');

function acf_load_colors_choices( $field ) {
    global $cms_recipe_colors_list;

    $field['choices'] = array();

    foreach ($cms_recipe_colors_list as $key => &$val){
        $field['choices'][$key] = $val;
    }
    return $field;
}
*/

//add menu_order for posts:

add_action( 'admin_init', 'add_posts_order_custom' );
function add_posts_order_custom(){
    add_post_type_support( 'post', 'page-attributes' );
    //add_post_type_support( 'cpt-name-here', 'page-attributes' );
}

/*

if (is_admin()){
    
    add custom columns in admin taxonomy list  ================
    https://codex.wordpress.org/Plugin_API/Filter_Reference/manage_$taxonomy_id_columns
    */
/*
    //manage_edit-{$taxonomy}_columns
    add_filter("manage_edit-recipe_category_columns", 'recipe_category_columns');

    function recipe_category_columns($columns) {
        unset($columns['wpseo-score-readability']);
        $columns['custom_fields'] = 'Details';

        return $columns;
    }

    //manage_{$taxonomy}_custom_column

    // Add to admin_init function
    add_filter("manage_recipe_category_custom_column", 'recipe_category_column_values', 10, 3);

    function recipe_category_column_values($content, $column_name, $term_id) {
        switch ($column_name) {
            case 'custom_fields':
                $featured_recipe = get_field('featured_recipe', 'recipe_category_'.$term_id);
                if (!empty($featured_recipe)){
                    $content .= '<div><b>Featured recipe: </b>'.$featured_recipe->post_title.'</div>';
                }
                $show_in_filters = get_field('show_in_filters', 'recipe_category_'.$term_id);
                if ($show_in_filters){
                    $content .= '<div><b>Show in filters: </b> yes</div>';
                }
                break;
        }
        return $content;
    }


    //add custom fields in recipes list ===========

    function my_modify_recipe_table( $column ) {
        $column['details'] = 'Details';
        return $column;
    }
    add_filter( 'manage_recipe_posts_columns', 'my_modify_recipe_table' );

    function my_modify_recipe_table_row( $column_name, $post_id ) {
        $custom_fields = get_post_custom( $post_id );

        //recipe_rating, post_views_count, recipe_num_votes

        switch ($column_name) {
            case 'details' :
                $views = (isset($custom_fields['post_views_count'][0])) ? $custom_fields['post_views_count'][0] : '0';
                $rating = (isset($custom_fields['recipe_rating'][0])) ? $custom_fields['recipe_rating'][0] : '0';
                $votes = (isset($custom_fields['recipe_num_votes'][0])) ? $custom_fields['recipe_num_votes'][0] : '0';

                if (!isset($custom_fields['recipe_rating'][0])){
                    add_post_meta($post_id, 'recipe_rating', '0');
                }
                if (!isset($custom_fields['post_views_count'][0])){
                    add_post_meta($post_id, 'post_views_count', '0');
                }
                if (!isset($custom_fields['recipe_num_votes'][0])){
                    add_post_meta($post_id, 'recipe_num_votes', '0');
                }

                echo '<b>Views:</b> '.$views;
                echo '<br/><b>Rating score:</b> '.calc_rating($rating, $votes);
                echo '<br/><b>#Votes:</b> '.$votes;
                break;
            default:
        }
    }
    add_action( 'manage_recipe_posts_custom_column', 'my_modify_recipe_table_row', 10, 2 );

}
*/
//add extra fields in admin user list
/*
if (is_admin()){
    add_action('manage_users_columns','kjl_modify_user_columns');
    function kjl_modify_user_columns($column_headers) {
        unset($column_headers['posts']);
        //unset($column_headers['role']);
        $column_headers['user_order'] = 'Order';
        $column_headers['num_posts'] = 'Posts';
        return $column_headers;
    }

    function kjl_count_user_posts_by_type_and_status( $userid, $post_status, $post_type = "post" ) {
        global $wpdb;
        $where = "WHERE post_author = " . $userid . " AND post_type = '" . $post_type . "' AND (post_status = '" . $post_status . "')";
        $count = $wpdb->get_var( "SELECT COUNT(*) FROM $wpdb->posts $where" );
        return apply_filters( 'get_usernumposts', $count, $userid );
    }

    add_action('manage_users_custom_column', 'kjl_user_posts_count_column_content', 10, 3);
    function kjl_user_posts_count_column_content($value, $column_name, $user_id) {
        $user_data = get_userdata($user_id);
        $user_meta = get_user_meta($user_id);

        $user_roles = $user_data->roles;
        $is_subscriber = (in_array('subscriber', $user_roles)) ? true : false;

        if ( 'num_posts' == $column_name ) {
            if ($is_subscriber){
                $reply = (isset($user_meta['subscribe_referrer'][0])) ? 'Registration Referrer Page: '.urldecode($user_meta['subscribe_referrer'][0]) : '';
                return $reply;
            }
            $post_by_user = kjl_count_user_posts_by_type_and_status( $user_id, 'publish', 'post' );
            $project_by_user = kjl_count_user_posts_by_type_and_status( $user_id, 'publish', 'projects' );
            $all = $post_by_user + $project_by_user + $videos_by_user + $magazines_by_user + $photos_group_by_user + $analysis_by_user;

            return 'All: <a href="'.WP_HOME.'/wp-admin/edit.php?author='.$user_id.'" target="_blank">'.$all.'</a><br/>
            Posts: <a href="'.WP_HOME.'/wp-admin/edit.php?post_type=post&amp;author='.$user_id.'" target="_blank">'.$post_by_user.'</a><br/>
            Projects: <a href="'.WP_HOME.'/wp-admin/edit.php?post_type=projects&amp;author='.$user_id.'" target="_blank">'.$project_by_user.'</a><br/>
            ';
        }
        else if ($column_name=='user_order'){
            if ($is_subscriber){
                $reply = (isset($user_meta['last_login'][0])) ? 'Last login: '.date('d-m-Y H:i', $user_meta['last_login'][0]) : '';
                return $reply;
            }
            else{
                if (get_field('author_visible', 'user_'.$user_id)){
                    return get_field('author_order', 'user_'.$user_id);
                }
                else{
                    return '<i>Not visible</i>';
                }
            }
        }
        return $value;
    }


    //dashboard widgets:

    function dialogos_add_dashboard_widget() {
        wp_add_dashboard_widget(
            'dialogos_dashboard_widget',         // Widget slug.
            'Upcoming Chats',         // Title.
            'dialogos_add_dashboard_widget_function' // Display function.
        );
    }
    add_action( 'wp_dashboard_setup', 'dialogos_add_dashboard_widget' );

    function dialogos_add_dashboard_widget_function() {
        $today = date('Y-m-d');
        $oneWeekLater = date('Y-m-d', strtotime('+1 week'));

        $posts_with_chats = cache_query(array(
            'post_type' => 'post',
            'post_status' => 'publish',
            'orderby' => array('date_clause' => 'ASC', 'menu_order' => 'ASC', 'date' => 'DESC'),
            'update_post_meta_cache' => false,
            'update_post_term_cache' => false,
            'ignore_sticky_posts' => true,
            'posts_per_page' => -1,
            'nopaging' => true,
            'no_found_rows' => true,
            'meta_query' => array(
                'relation' => 'AND',
                array(
                    'key' => 'chat_schedule',
                    'value' => '1',
                    'compare' => '=',
                ),
                'date_clause' => array(
                    'key' => 'chat_date',
                    'value' => $today,
                    'compare' => '>=',
                    'type' => 'DATE'
                ),
                array(
                    'key' => 'chat_date',
                    'value' => $oneWeekLater,
                    'compare' => '<',
                    'type' => 'DATE'
                ),
            )
        ), array('function'=>'get_posts'));

        if (!empty($posts_with_chats)){
            echo '<ul>';
            foreach ($posts_with_chats as &$chat_post){
                ?><li><a href="<?php echo WP_HOME; ?>/wp-admin/post.php?post=<?php echo $chat_post->ID; ?>&action=edit"><?php echo $chat_post->post_title; ?></a></li><?php
            }
            echo '</ul>';
        }
    }
}


if (is_admin()) {
    //add custom fields in posts list ===========

    function my_modify_posts_table( $column ) {
        $column['my_post_views_count'] = 'Views';
        return $column;
    }
    add_filter( 'manage_posts_columns', 'my_modify_posts_table' );

    function my_modify_posts_table_row( $column_name, $post_id ) {
        $custom_fields = get_post_custom( $post_id );

        switch ($column_name) {
            case 'my_post_views_count' :
                echo (isset($custom_fields['my_post_views_count'][0])) ? $custom_fields['my_post_views_count'][0] : 0;
                break;
            default:
        }
    }
    add_action( 'manage_posts_custom_column', 'my_modify_posts_table_row', 10, 2 );
}

if (is_admin()) {
    //add custom fields in packages list ===========

    function my_modify_packages_table( $column ) {
        $column['package_start_date'] = 'Start date';
        $column['package_end_date'] = 'End date';
        return $column;
    }
    add_filter( 'manage_package_posts_columns', 'my_modify_packages_table' );

    function my_modify_packages_table_row( $column_name, $post_id ) {
        $custom_fields = get_post_custom( $post_id );

        switch ($column_name) {
            case 'package_start_date' :
                echo (isset($custom_fields['package_start_date'][0])) ? date("d/m/Y", strtotime($custom_fields['package_start_date'][0])) : ' ';
                break;
            case 'package_end_date' :
                echo (isset($custom_fields['package_end_date'][0])) ? date("d/m/Y", strtotime($custom_fields['package_end_date'][0])) : ' ';
                break;
            default:
        }
    }
    add_action( 'manage_package_posts_custom_column', 'my_modify_packages_table_row', 10, 2 );
}


if (is_admin()){
    //add custom fields in projects list ===========

    function my_modify_project_table( $columns ) {
        $columns = insertArrayAtPosition($columns, array('thumb' => 'Thumb'), 1); // add in second place ( after checkbox)
        $columns = insertArrayAtPosition($columns, array('project_year' => 'Year'), 3);
        return $columns;
    }
    add_filter( 'manage_project_posts_columns', 'my_modify_project_table' );

    function my_modify_project_table_row( $column_name, $post_id ) {
        $custom_fields = get_post_custom( $post_id );

        $img = my_get_post_thumb($post_id, 'project_thumb', false);

        switch ($column_name) {
            case 'thumb' :
                echo (!empty($img)) ? '<img src="'.$img.'" alt="thumb" style="width: 70px">' : '-';
                break;
            case 'project_year' :
                echo (isset($custom_fields['project_year'][0])) ? $custom_fields['project_year'][0] : '';
                break;
            default:
        }
    }
    add_action( 'manage_project_posts_custom_column', 'my_modify_project_table_row', 10, 2 );

}
*/


//fix menu order after new page is created - to be +1 (auto-increase)

function update_page_menu_order_after_save($post_id) {
    if (wp_is_post_revision($post_id)){
        return;
    }

    $post = get_post( $post_id );

    if ($post->post_type != 'page'){
        return;
    }

    $menu_order = $post->menu_order;

    if ($menu_order > 0){
        return;
    }

    if (empty($menu_order) || $menu_order==0){
        //if this page was just saved

        global $wpdb, $table_prefix;

        //ID, post_author, post_date, post_date_gmt, post_content, post_title, post_excerpt, post_status, comment_status, ping_status, post_password, post_name, to_ping, pinged, post_modified, post_modified_gmt, post_content_filtered, post_parent, guid, menu_order, post_type, post_mime_type, comment_count

        $find_max_menu_order = $wpdb->get_results("SELECT max(menu_order) as max from ".$table_prefix."posts where post_type='page' and post_status='publish'");
        //SELECT max(menu_order+0)

        if (isset($find_max_menu_order[0]->max) && $find_max_menu_order[0]->max>0){
            $menu_order = $find_max_menu_order[0]->max + 1;
        }else{
            $menu_order = 1;
        }

        remove_action('save_post', 'update_page_menu_order_after_save');

        // update the post, which calls save_post again
        wp_update_post(  array(
            'ID'           => $post->ID,
            'menu_order'   => $menu_order,
        ));

        // re-hook this function
        add_action('save_post', 'update_page_menu_order_after_save');

    }
}
add_action( 'save_post', 'update_page_menu_order_after_save' );


/* when creating a new recipe or edit a recipe set 2 default meta fields 

function recipe_update_meta_fields_after_save($post_id) {
    if (wp_is_post_revision($post_id)){
        return;
    }

    $post = get_post( $post_id );

    if ($post->post_type != 'recipe'){
        return;
    }

    //custom meta fields that need to be set: recipe_rating, post_views_count, recipe_num_votes

    $postmeta = get_post_meta($post_id);

    if (isset($postmeta['recipe_rating'][0]) && isset($postmeta['post_views_count'][0]) && isset($postmeta['recipe_num_votes'][0])){
        return;
    }

    if (!isset($postmeta['recipe_rating'][0])){
        add_post_meta($post_id, 'recipe_rating', '0');
    }
    if (!isset($postmeta['post_views_count'][0])){
        add_post_meta($post_id, 'post_views_count', '0');
    }
    if (!isset($postmeta['recipe_num_votes'][0])){
        add_post_meta($post_id, 'recipe_num_votes', '0');
    }
}
add_action( 'save_post', 'recipe_update_meta_fields_after_save' );
*/

//prevent sending password reset email:
add_filter( 'send_password_change_email', '__return_false' );

//prevent sending "password changed" email:
add_filter( 'send_email_change_email', '__return_false' );


function taxonomy_checklist_checked_ontop_filter ($args){
    $args['checked_ontop'] = false;
    return $args;
}
add_filter('wp_terms_checklist_args','taxonomy_checklist_checked_ontop_filter');


add_filter('jpeg_quality', function($arg){return 100;});


/*
ad new admin account for owner:
https://codex.wordpress.org/Roles_and_Capabilities
*/

add_role(
    'site_owner',
    'Site Owner',
    array(
        'delete_others_pages'  => true,
        'delete_others_posts'  => true,
        'delete_pages'  => true,
        'delete_posts'  => true,
        'delete_private_pages'  => true,
        'delete_private_posts'  => true,
        'delete_published_pages'  => true,
        'delete_published_posts'  => true,
        'edit_others_pages'  => true,
        'edit_others_posts'  => true,
        'edit_pages'  => true,
        'edit_posts'  => true,
        'edit_private_pages'  => true,
        'edit_private_posts'  => true,
        'edit_published_pages'  => true,
        'edit_published_posts'  => true,
        'manage_categories'  => true,
        'manage_links'  => true,
        'moderate_comments'  => true,
        'publish_pages'  => true,
        'publish_posts'  => true,
        'read'  => true,
        'read_private_pages'  => true,
        'read_private_posts'  => true,
        //'unfiltered_html'  => true,
        'upload_files'  => true,
        'edit_files'  => true,
        'list_users'  => true,
        'edit_users'  => true,
        'create_users'  => true,
        'delete_users'  => true,
        'remove_users'  => true,
        'edit_theme_options'  => true,
        /*'edit_themes'  => true,
        'switch_themes'  => true,*/
    )
);


/* ==== */

if (is_admin()) {

// Customizes 'Editor' role to have the ability to modify menus

    class Custom_Admin{
        public function __construct(){
            // Allow editor to edit theme options (ie Menu)
            add_action('init', array($this, 'init'));
            add_filter('editable_roles', array($this, 'editable_roles'));
        }

        public function init(){
            if ($this->is_editor() || $this->is_site_owner()) {
                add_filter('user_has_cap', array($this, 'user_add_cap'));
            }
        }

        public function wp_die(){
            _default_wp_die_handler(__('You do not have sufficient permissions to access this page.'));
        }

        public function modify_menus(){
            remove_submenu_page('themes.php', 'themes.php'); // hide the theme selection submenu
            remove_submenu_page('themes.php', 'widgets.php'); // hide the widgets submenu
        }

        // Remove 'Administrator' from the list of roles if the current user is not an admin
        public function editable_roles($roles){
            if (isset($roles['administrator']) && !current_user_can('administrator')) {
                unset($roles['administrator']);
            }
            return $roles;
        }

        public function user_add_cap($caps){
            /*$caps['list_users'] = true;
            $caps['create_users'] = true;

            $caps['edit_users'] = true;
            $caps['promote_users'] = true;

            $caps['delete_users'] = true;
            $caps['remove_users'] = true;*/

            $caps['edit_theme_options'] = true;
            return $caps;
        }


        // If current user is called admin or administrative and is an editor
        protected function is_editor(){
            $current_user = wp_get_current_user();
            $is_editor = isset($current_user->caps['editor']) ? $current_user->caps['editor'] : false;
            return ($is_editor);
        }

        protected function is_site_owner(){
            $current_user = wp_get_current_user();
            $is_site_owner = isset($current_user->caps['site_owner']) ? $current_user->caps['site_owner'] : false;
            return ($is_site_owner);
        }
    }

    new Custom_Admin();

}

if (is_admin()){


//add_filter( 'manage_edit-matches_columns', 'my_edit_matches_columns' ) ;

function my_edit_matches_columns( $columns ) {

    $columns = array(
        'title' => __( 'Title' ),
        'organization' => __( 'Organization' ),
        'match_status' => __( 'Status' ),
        'date'        => __( 'Date' ),
    );

    return $columns;
}


//add_action( 'manage_matches_posts_custom_column', 'my_manage_matches_columns', 10, 2 );

function my_manage_matches_columns( $column, $post_id ) {
    global $post;

    switch( $column ) {

        case 'organization' :

            $organ = get_field( 'matche_organization' );
            if ( empty( $organ ) )
                echo __( '-' );
            else
                echo $organ;

            break;

        case 'match_status' :

            $terms = get_the_terms( $post_id, 'matches-status' );
            if ( !empty( $terms ) ) {



                echo  $terms[0]->name ;
            }

            else {
                _e( '-' );
            }

            break;

        default :
            break;
    }
}




function remove_editor() {
    if (isset($_GET['post'])) {
        $id = $_GET['post'];
        $template = get_post_meta($id, '_wp_page_template', true);
        switch (basename($template)) {
            case 'whoweare.php':
                remove_post_type_support('page', 'editor');
            case 'ourvision.php':
                remove_post_type_support('page', 'editor');
            case 'actnow.php':
                remove_post_type_support('page', 'editor');
            default :
                // Don't remove any other template.
                break;
        }
    }
}
add_action('init', 'remove_editor');
}