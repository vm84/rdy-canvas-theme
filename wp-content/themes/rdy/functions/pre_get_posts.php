<?php

function change_posts_query( $query ){
    if (!is_admin() && $query->is_main_query()) {
        if (isset($query->query['category_name']) || isset($query->query['tag'])){
            //if news category or tag selected
            $query->set('orderby', array('menu_order' => 'ASC', 'date' => 'DESC'));
        }
        if(isset($query->query['post_type'])){
            if($query->query['post_type'] == 'cpt_projects' && $query->is_archive){
                $query->set('orderby', array('menu_order' => 'ASC', 'date' => 'DESC'));
            }
        }


    }
}
add_action( 'pre_get_posts', 'change_posts_query' );


/*function my_sanitize_array($array){
    //clean up list
    if (empty($array)){
        return $array;
    }

    foreach ($array as $key => &$vat_val) {
        if (!is_numeric($vat_val)) {
            unset($array[$key]);
        }
        else{
            $array[$key] = intval($vat_val);
        }
    }

    return $array;
}


add_action( 'pre_get_posts', 'change_main_query' );

function change_main_query($query){
    if (!is_admin() && $query->is_main_query()) {
        if (isset($query->query['recipe_category']) || isset($query->query['recipe_tag'])){
            $query->set('posts_per_page', 15);
        }

        if (is_search()){
            $query->set('posts_per_page', 20);

            global $search_type;

            $search_type = (isset($_GET['type']) && !empty($_GET['type'])) ? quote_smart($_GET['type'], true) : '';

            if (!empty($search_type)){
                $query->set('post_type', $search_type);
            }
        }

        /* =========== gastropedia filters ========== *

        if (isset($query->query['gastropedia_page']) || isset($query->query['gastropedia_category'])){

            $query->set('orderby', array('title' => 'ASC', 'menu_order' => 'ASC'));

            if (isset($_GET['filter_cat']) && !empty($_GET['filter_cat']) && is_array($_GET['filter_cat'])) {
                //clean up list
                $filter_categories = my_sanitize_array($_GET['filter_cat']);

                $query->tax_query->queries[] = array(
                    array(
                        'taxonomy' => 'gastropedia_category',
                        'field' => 'term_id',
                        'terms' => $filter_categories,
                        'operator' => 'IN'
                    )
                );
                $query->query_vars['tax_query'] = $query->tax_query->queries;
            }

            if (isset($_GET['gp']) && is_numeric($_GET['gp']) && $_GET['gp']>0){
                //if from mega menu
                global $selected_gastropedia_page;

                $selected_gastropedia_page = quote_smart($_GET['gp'], true);

                if ($selected_gastropedia_page>0) {
                    $query->tax_query->queries[] = array(
                        array(
                            'taxonomy' => 'gastropedia_page',
                            'field' => 'term_id',
                            'terms' => $selected_gastropedia_page,
                        )
                    );
                    $query->query_vars['tax_query'] = $query->tax_query->queries;
                }
            }

        }
    }
}


/*
add custom field in wp_query to filter posts starting from a given letter (gastropedia).

example call:

$res = new WP_Query(array(
    'extend_where' => "post_title like '".$str."'%"
));
*/
/*
add_filter( 'posts_where', 'extend_wp_query_where', 10, 2 );
function extend_wp_query_where( $where, $wp_query ) {
    if ( $extend_where = $wp_query->get( 'extend_where' ) ) {
        //$where .= " AND " . $wpdb->posts . ".post_title " . esc_sql($wpdb->esc_like($post_title_like));
        $where .= " AND " . $extend_where;
    }
    return $where;
}*/

/*
function change_main_query($query){
    if (!is_admin() && $query->is_main_query()) {
        if (isset($query->query['category_name']) && $query->query['category_name']=='news-feed') {
            //news-feed
            $query->set('posts_per_page', 1);

            //creates custom query in template!

        } else if (is_tag()) {
            $query->set('posts_per_page', 16);
            $query->set('post_type', array('post', 'projects', 'analysis'));
            $query->set('post_parent', 0);
        }
    }
    if ($query->is_search){
        $query->set('posts_per_page', 20);
        $query->set('post_parent', 0);

        //filters for search page =====

        //http://localhost/think_news/?s=%CE%B3%CE%B9%CE%B1&start-date=01-05-2017&end-date=22-06-2017&cat%5B%5D=3&cat%5B%5D=4&cat%5B%5D=2&tp%5B%5D=page&tp%5B%5D=post&tp%5B%5D=projects&tp%5B%5D=videos&tp%5B%5D=magazines&tp%5B%5D=photos_group&tp%5B%5D=analysis&chat=y

        global $filter_categories, $available_types, $filter_types, $filter_chat, $start_date, $end_date, $is_filtered_search;  //set to global to be used inside the search page as well

        $is_filtered_search = false;

        $filter_categories = (isset($_GET['cat']) && !empty($_GET['cat']) && is_array($_GET['cat'])) ? $_GET['cat'] : array();
        foreach ($filter_categories as $key => &$vat_val) {
            if (!is_numeric($vat_val)) {
                unset($filter_categories[$key]);
            }
            else{
                $filter_categories[$key] = intval($vat_val);
            }
        }
        if (!empty($filter_categories)){
            $query->tax_query->queries[] = array(
                array(
                    'taxonomy' => 'category',
                    'field' => 'term_id',
                    'terms' => $filter_categories,
                    'operator'=> 'IN'
                )
            );
            $query->query_vars['tax_query'] = $query->tax_query->queries;

            $is_filtered_search = true;
        }

        $filter_types = (isset($_GET['tp']) && !empty($_GET['tp']) && is_array($_GET['tp'])) ? $_GET['tp'] : array();
        foreach ($filter_types as $key => &$vat_val) {
            if (!array_key_exists($vat_val, $available_types)) {
                unset($filter_types[$key]);
            }
            else{
                $filter_types[$key] = quote_smart($vat_val, true);
            }
        }
        if (!empty($filter_types)){
            $query->set('post_type', $filter_types);

            $is_filtered_search = true;
        }


        $filter_chat = (isset($_GET['chat']) && $_GET['chat']=='y') ? true : false;
        if ($filter_chat){
            $query->meta_query->queries[] = array(
                'relation' => 'AND',
                array(
                    'key' => 'chat_schedule',
                    'value' => '1',
                    'compare' => '=',
                ),
                'date_clause' => array(
                    'key' => 'chat_date',
                    'value' => date('Y-m-d'),
                    'compare' => '>=',
                    'type' => 'DATE'
                ),
            );
            $query->query_vars['meta_query'] = $query->meta_query->queries;

            $is_filtered_search = true;
        }

        $start_date = (isset($_GET['start-date']) && !empty($_GET['start-date'])) ? $_GET['start-date'] : '';
        if (!empty($start_date)){
            $query->date_query->queries[] = array(
                'after'     => date('Y-m-d', strtotime($start_date)).' 00:00:00',
            );
            $query->query_vars['date_query'] = $query->date_query->queries;

            $is_filtered_search = true;
        }

        $end_date = (isset($_GET['end-date']) && !empty($_GET['end-date'])) ? $_GET['end-date'] : '';
        if (!empty($end_date)){
            $query->date_query->queries[] = array(
                'before'     => date('Y-m-d', strtotime($end_date)).' 23:59:59',
            );
            $query->query_vars['date_query'] = $query->date_query->queries;

            $is_filtered_search = true;
        }
    }
}
*/