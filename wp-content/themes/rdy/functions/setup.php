<?php
// ********** SETUP Backend ********

//add capabilities for editor: same as admin for editing
$role = get_role('editor');
$role->add_cap('edit_theme_options');

$role = get_role('author');
$role->add_cap('moderate_comments');

//Allow Contributors to Add Media
if ( current_user_can('contributor') && !current_user_can('upload_files') ){
    add_action('admin_init', 'allow_contributor_uploads');
}
function allow_contributor_uploads() {
    $contributor = get_role('contributor');
    $contributor->add_cap('upload_files');
}

// remove appearance submenu: Customize, editor
function as_remove_menus(){
    global $submenu;
    // Appearance Menu
    unset($submenu['themes.php'][6]); //
    unset($submenu['themes.php'][15]);
    unset($submenu['themes.php'][20]);
    //remove_menu_page( 'edit-comments.php' );
}
add_action('admin_menu', 'as_remove_menus');

//advanced custom fields options pages
if( function_exists('acf_add_options_page') ) {
    acf_add_options_page(array(
        'page_title' 	=> 'Site Elements',
        'menu_title'	=> 'Site Elements',
        'menu_slug' 	=> 'theme-general-settings',
        'capability'	=> 'publish_pages',
        'redirect'		=> false
    ));

}

//advanced custom fields options pages
if( function_exists('acf_add_options_page') ) {
    acf_add_options_page(array(
        'page_title' 	=> 'Translations',
        'menu_title'	=> 'Translations',
        'menu_slug' 	=> 'theme-translation-settings',
        'capability'	=> 'publish_pages',
        'redirect'		=> false
    ));

}

/* ==== */

function my_login_stylesheet() {
    wp_enqueue_style( 'custom-admin-css', get_stylesheet_directory_uri() . '/css/admin.css' );
}
add_action( 'login_enqueue_scripts', 'my_login_stylesheet' );
add_action( 'admin_enqueue_scripts', 'my_login_stylesheet' );


add_action( 'init', 'my_deregister_heartbeat', 1 );
function my_deregister_heartbeat() {
    global $pagenow;

    if ( 'post.php' != $pagenow && 'post-new.php' != $pagenow )
        wp_deregister_script('heartbeat');
}


if ( ! function_exists( 'twentyfifteen_setup' ) ) :
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     *
     * @since Twenty Fifteen 1.0
     */
    function twentyfifteen_setup() {

        /*
         * Make theme available for translation.
         * Translations can be filed in the /languages/ directory.
         * If you're building a theme based on twentyfifteen, use a find and replace
         * to change 'twentyfifteen' to the name of your theme in all the template files
         */
        load_theme_textdomain( 'twentyfifteen', get_template_directory() . '/languages' );

        // Add default posts and comments RSS feed links to head.
        add_theme_support( 'automatic-feed-links' );

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support( 'title-tag' );

        // This theme uses wp_nav_menu() in two locations.
        register_nav_menus( array(
            'primary' => 'Burger Menu',
            //'topmenu'  => 'Top Menu',
            /*'categories'  => 'Categories Menu',*/
            'footer'  => 'Footer Menu',
            //'copyrights'  => 'Copyrights Menu',
        ) );

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support( 'html5', array(
            'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
        ) );

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * See: https://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
         */
        add_theme_support( 'post-thumbnails' );
        //set_post_thumbnail_size( 825, 510, true );

        /*add_image_size('home_slider', '1225', '760', true); //hard crop*/
        /*add_image_size('thumb_big', '490', '275');*/
        add_image_size('thumb_big', '1025', '513');
        add_image_size('proj_med', '780', '512');
        add_image_size('proj_mob', '355', '233');


    }
endif; // twentyfifteen_setup
add_action( 'after_setup_theme', 'twentyfifteen_setup' );



/**
 * Register widget area.
 *
 * @since Twenty Fifteen 1.0
 *
 * @link https://codex.wordpress.org/Function_Reference/register_sidebar
 */


if ( ! function_exists( 'twentyfifteen_fonts_url' ) ) :
    /**
     * Register Google fonts for Twenty Fifteen.
     *
     * @since Twenty Fifteen 1.0
     *
     * @return string Google fonts URL for the theme.
     */
    function twentyfifteen_fonts_url() {
        $fonts_url = '';
        $fonts     = array();
        $subsets   = 'latin,latin-ext';

        /*
         * Translators: If there are characters in your language that are not supported
         * by Noto Sans, translate this to 'off'. Do not translate into your own language.
         */
        if ( 'off' !== _x( 'on', 'Noto Sans font: on or off', 'twentyfifteen' ) ) {
            $fonts[] = 'Noto Sans:400italic,700italic,400,700';
        }

        /*
         * Translators: If there are characters in your language that are not supported
         * by Noto Serif, translate this to 'off'. Do not translate into your own language.
         */
        if ( 'off' !== _x( 'on', 'Noto Serif font: on or off', 'twentyfifteen' ) ) {
            $fonts[] = 'Noto Serif:400italic,700italic,400,700';
        }

        /*
         * Translators: If there are characters in your language that are not supported
         * by Inconsolata, translate this to 'off'. Do not translate into your own language.
         */
        if ( 'off' !== _x( 'on', 'Inconsolata font: on or off', 'twentyfifteen' ) ) {
            $fonts[] = 'Inconsolata:400,700';
        }

        /*
         * Translators: To add an additional character subset specific to your language,
         * translate this to 'greek', 'cyrillic', 'devanagari' or 'vietnamese'. Do not translate into your own language.
         */
        $subset = _x( 'no-subset', 'Add new subset (greek, cyrillic, devanagari, vietnamese)', 'twentyfifteen' );

        if ( 'cyrillic' == $subset ) {
            $subsets .= ',cyrillic,cyrillic-ext';
        } elseif ( 'greek' == $subset ) {
            $subsets .= ',greek,greek-ext';
        } elseif ( 'devanagari' == $subset ) {
            $subsets .= ',devanagari';
        } elseif ( 'vietnamese' == $subset ) {
            $subsets .= ',vietnamese';
        }

        if ( $fonts ) {
            $fonts_url = add_query_arg( array(
                'family' => urlencode( implode( '|', $fonts ) ),
                'subset' => urlencode( $subsets ),
            ), '//fonts.googleapis.com/css' );
        }

        return $fonts_url;
    }
endif;

/**
 * JavaScript Detection.
 *
 * Adds a `js` class to the root `<html>` element when JavaScript is detected.
 *
 * @since Twenty Fifteen 1.1
 */
function twentyfifteen_javascript_detection() {
    echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}
add_action( 'wp_head', 'twentyfifteen_javascript_detection', 0 );


function remove_dashboard_meta() {
    remove_meta_box( 'dashboard_primary', 'dashboard', 'side' );
    remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
}

add_action( 'admin_init', 'remove_dashboard_meta' );

remove_filter( 'the_content', 'wpautop' );
remove_filter( 'the_excerpt', 'wpautop' );


function my_set_post_meta($postID, $field='post_views_count') {
    $count = get_post_meta($postID, $field, true);
    if ($count==''){
        delete_post_meta($postID, $field);
        add_post_meta($postID, $field, '0');
    }
    else{
        $count++;
        update_post_meta($postID, $field, $count);
    }
}

if ( is_admin() ){
    add_action( 'admin_bar_menu', 'remove_wp_logo', 999 );
    function remove_wp_logo( $wp_admin_bar ) {
        $wp_admin_bar->remove_node( 'wp-logo' );
    }







	function notes_add_dashboard_widget() {
        wp_add_dashboard_widget(
            'notes_dashboard_widget',         // Widget slug.
            'Notes',         // Title.
            'notes_add_dashboard_widget_function' // Display function.
        );
    }
    add_action( 'wp_dashboard_setup', 'notes_add_dashboard_widget' );

    function notes_add_dashboard_widget_function() {
        global $template_url;

        echo '<p><b>All images that are uploaded on the CMS need to follow the steps below:</b></p>
        <ul>
            <li>Crop / Resize image in specified dimensions (eg in Photoshop).</li>
            <li>In Photoshop, use the "Progressive" Setting and Quality of max 80%.</li>
            <li>Save for Web & Devices.</li>
            <li>Give a proper name to the image. Avoid using 1.jpg. Instead try something like: home-slider-1.jpg</li>
            <li>DO NOT use spaces, Greek or special characters in the image of the name. Instead use letters from the English alhabet, numbers and dash "-".</li>
            <li>Optimize the image. You may use ImageOptim or tinyPNG, both are free.</li>
        </ul>';
    }
}

// REMOVE WP EMOJI
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );

remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'feed_links', 2);
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'start_post_rel_link', 10);
remove_action('wp_head', 'parent_post_rel_link', 10);
remove_action('wp_head', 'adjacent_posts_rel_link', 10);

function _remove_script_version( $src ){
    $parts = explode( '?ver', $src );
    return $parts[0];
}
add_filter( 'script_loader_src', '_remove_script_version', 15, 1 );
add_filter( 'style_loader_src', '_remove_script_version', 15, 1 );



