<?php

function remove_quotes($string) {
    $text = str_replace('"', "", $string);

    return $text;
}


function get_post_date($date){
    global $months;
    $time = strtotime($date);
    return(do_uppercase($months[date('n', $time)]).' '.date('d', $time).', '.date('Y', $time));
}

function get_post_date_el($date){
    global $months;
    $time = strtotime($date);
    return(date('d '.do_uppercase($months[date('n', $time)]).' Y', $time));

}

function match_date($date){
    $time = strtotime($date);
    return(date('d.m', $time));
}


function match_day($date){
    global $days;
    $time = strtotime($date);
    return(date(do_uppercase($days[date('N', $time)]), $time));
}




function get_month_year($date){
    global $months_full;
    $time = strtotime($date);

    $reply = $months_full[date('n', $time)].' '.date(' Y', $time);

    return($reply);
}

function get_simple_date($date, $show_dif = false, $show_time = false, $lang=ICL_LANGUAGE_CODE){
    $time = strtotime($date);

    if ($lang == 'el'){
        global $months_greek;

        $reply = $months_greek[date('n', $time)].' '.date('d, Y', $time);
    }
    else{
        $reply = date('M d, Y', $time);
    }

    if ($show_time){
        $reply .= ' '.date('H:i', $time);
    }

    /*if ($show_dif){
        $now = time();
        $diff = (int) abs($now - $time );
        if ($diff <= WEEK_IN_SECONDS){
            $reply = human_time_diff($time, $now);
            $reply_explode = explode(' ', $reply);
            if (!empty($reply_explode[1]) && array_key_exists($reply_explode[1], $static_translations)){
                $reply_explode[1] = $static_translations[trim($reply_explode[1])];
                $reply = implode(' ', $reply_explode);
            }
            $reply .= ' πριν';
        }
    }*/

    return($reply);

    //return(date('M. d,Y', $time));
}
function make_date_slash($date){
    $time = strtotime($date);
    return(date('d.m.Y', $time));
}

/*function make_date($date, $sep='.', $wrap_month=false){
    $time = strtotime( $date );
    if ($wrap_month){
        return('<span>'.date('M', $time).'</span>'.$sep.date('Y', $time));
    }
    return(date('M', $time).$sep.date('Y', $time));
}
function make_date_year($date){
    $time = strtotime( $date );
    return(date('Y', $time));
}*/


function remove_spec_chars($string) {
    $chars = array('!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '+', '=', '~', '`', '"', "'", '|', '\\', ';', ':', ',', '/', '?', '<', '>');
    $str  = str_replace($chars, "", $string);
    return $str;
}

function remove_tonos($string){
    $grammatametonous   = array('ά','έ', 'ή', 'ί', 'ό', 'ύ', 'ώ', 'Ά', 'Έ', 'Ή', 'Ί', 'Ό', 'Ύ', 'Ώ', 'ς', ' & ');
    $xwristonous = array('α', 'ε', 'η', 'ι', 'ο', 'υ', 'ω', 'Α', 'Ε', 'Η', 'Ι', 'Ο', 'Υ', 'Ω', 'Σ', ' &amp; ');

    $str  = str_replace($grammatametonous, $xwristonous, $string);
    return $str;
}

function remove_tonos_international($string) {
    $grammatametonous   = array('À','Â','Á','Ã','Ä', 'Ç', 'È','É','Ê','Ë', 'Î', 'Ò','Ó','Ô','Õ','Ö', 'Ù','Ú','Û', 'à','à','á','â','ã','ä', 'ç', 'è','é','ê','ë', 'ì','í','î','ï', 'ò','ó','ô','õ', 'ù','ú','û','ü', 'ά','έ', 'ή', 'ί', 'ό', 'ύ', 'ώ', 'Ά', 'Έ', 'Ή', 'Ί', 'Ό', 'Ύ', 'Ώ', 'ς', ' & ');

    $xwristonous = array('A','A','A','A','A', 'C', 'E','E','E','E', 'I', 'O','O','O','O','O', 'U','U','U', 'a','a','a','a','a','a', 'c', 'e','e','e','e', 'i','i','i','i', 'o','o','o','o', 'u','u','u','u','α', 'ε', 'η', 'ι', 'ο', 'υ', 'ω', 'Α', 'Ε', 'Η', 'Ι', 'Ο', 'Υ', 'Ω', 'Σ', ' &amp; ' );

    $str  = str_replace($grammatametonous, $xwristonous, $string);
    return $str;
}

function do_uppercase($string){
    $string  = str_replace('&nbsp;', ' ', $string);
    $string  = remove_tonos($string);
    $string = strtoupper($string);

    $peza_gr = array("α","β","γ","δ","ε","ζ","η","θ","ι","κ","λ","μ","ν","ξ","ο","π","ρ","σ","τ","υ","φ","χ","ψ","ω");
    $caps_gr = array("Α","Β","Γ","Δ","Ε","Ζ","Η","Θ","Ι","Κ","Λ","Μ","Ν","Ξ","Ο","Π","Ρ","Σ","Τ","Υ","Φ","Χ","Ψ","Ω");
    //$string  = str_replace($peza_gr, $caps_gr, $string);

    $str = str_replace("ς", "Σ", $string);
    $str = mb_convert_case($str, MB_CASE_UPPER, "UTF-8"); /*MB_CASE_UPPER, MB_CASE_LOWER, or MB_CASE_TITLE. */
    $str = str_replace("&AMP;","&amp;" , $str);
    $str = str_replace("&RSQUO;","&rsquo;" , $str);
    /*$str = str_replace("<B>","<b>" , $str);
    $str = str_replace("</B>","</b>" , $str);*/
    return $str;
}

function do_case_title($string){
    return mb_convert_case($string, MB_CASE_TITLE, "UTF-8"); /*MB_CASE_UPPER, MB_CASE_LOWER, or MB_CASE_TITLE. */
}
function do_lowercase($string){
    return mb_convert_case($string, MB_CASE_LOWER, "UTF-8"); /*MB_CASE_UPPER, MB_CASE_LOWER, or MB_CASE_TITLE. */
}

function get_copyright($date = '2021'){
    if (date("Y")==$date)
        return ''.$date;
    else
        return ''. $date.'-'. date("Y");
}

function stringify($str){
    $str = do_lowercase($str);
    $str = str_replace(array(':'), '', $str);
    return (str_replace(' ', '-', $str));
}


function pr(){
    $style = ' white-space: pre-wrap; white-space: -moz-pre-wrap; white-space: -pre-wrap; white-space: -o-pre-wrap; word-wrap: break-word; display: block; background:#F7AFDE; border:2px dotted #070; padding: 7px; color:#000; font-family: Arial, Verdana; font-size:11px; text-align:left;';
    echo '<pre style="'.$style.'">';
    foreach (func_get_args() as $arg){
        print_r($arg);
        echo '<br/>';
    }
    echo '</pre>';
}

function prd($data){
    foreach (func_get_args() as $arg){
        pr($arg);
    }
    die();
}

function my_is_ajax(){
    return (empty($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') ? false : true;
}

function make_excerpt($text, $limit, $fixtext=false, $trailing_dots=true){
    if ($fixtext){
        if (strlen($text) <= $limit-2){
            return fix_text($text);
        }

        $text = utf8_substr(strip_tags($text), array('start'=>0, 'end'=>$limit));
        $text = $text['first_text'];
        if ($trailing_dots){
            $text .= "...";
        }
        return fix_text($text);
    }
    else{
        if (strlen($text) <= $limit-2){
            return $text;
        }

        $text = utf8_substr(strip_tags($text), array('start'=>0, 'end'=>$limit));
        $text = $text['first_text'];
        if ($trailing_dots){
            $text .= "...";
        }
        return $text;
    }
}

function utf8_substr($str, $options = array()){
    $options['word'] = (!isset($options['word'])) ? false : true;
    $options['start'] = isset($options['start']) ? $options['start'] : 0;
    $options['end'] = isset($options['end']) ? $options['end'] : strlen($str);
    $options['after'] = (isset($options['after']) && strlen($str) > $options['end']) ? $options['after'] : NULL;

    if (strlen($str) <= $options['end']) {
        $returnValue['first_text'] = str_replace(array('<p>', '</p>'), '', $str);
        return $returnValue;
    }

    preg_match_all("/./u", $str, $ar); // /u = utf8
    $string = $ar[0];

    if ($options['word'] === true) {
        if (!empty($string[$options['end']])){
            for($i = $options['end']; $i<=count($string); $i++){
                if (isset($string[$i]) && $string[$i] == ' '){
                    $finalPos = $i;
                    break;
                }
            }
        }
    }
    $options['end'] = isset($finalPos) ? $finalPos : $options['end'];

    $first_text = join("", array_slice($string, $options['start'], $options['end'])) . $options['after'];
    $rest_text = join("", array_slice($string, $options['end'], strlen($str)));

    $returnValue = array(
        'first_text' => $first_text,
        'rest_text' => $rest_text,
    );

    return $returnValue;
}

function uppercase_tag($str, $tag){
    $str = preg_replace_callback('/<'.$tag.'\b[^>]*>(.*?)<\/'.$tag.'>/is', 'upppercase_matches', $str);
    return $str;
}
function remove_paragraph($str){
    $str = str_replace('</p><p>', "<br>", $str);
    $str = str_replace(array('<p>','</p>'), '', $str);
    return $str;
}

function fix_text($str, $args = array()){
    $default_options = array(
        'contain_in_paragraph' => false,
        'uppercase_headings' => true,
        'uppercase_captions' => true,
        'uppercase_blockquote' => false,
        //'headers_class' => ''
    );
    $options = array_merge($default_options, $args);

    $str = str_replace(array("<br>", "<br/><br/>"), "<br/>", $str);
    $str = str_replace(array("<p>&nbsp;</p>", "<p></p>", '<div><br/></div>', '<div></div>'), "", $str);
    $str = str_replace(array("<p><p>", "<p> ", "<p>&nbsp;", '<p><br/>'), "<p>", $str);
    $str = str_replace(array("</p></p>", "<br/></p>"), "</p>", $str);
    $str = str_replace("<p><ul>", "<ul>", $str);
    $str = str_replace("</ul></p>", "</ul>", $str);
    $str = str_replace('http://mailto:', 'mailto:', $str);

    if ($options['contain_in_paragraph']){
        if (!strstr($str, '<p>') && !strstr($str, '</p>')){
            $str = '<p>'.$str.'</p>';
        }
    }

    if ($options['uppercase_headings']){
        # do_uppercase in h1, h2, ... tags
        $str = preg_replace_callback('/<h([1-6])\b[^>]*>(.*?)<\/h\1>/is', 'upppercase_matches', $str);
    }
    if ($options['uppercase_captions']){
        $str = preg_replace_callback('/<figcaption\b[^>]*>(.*?)<\/figcaption>/is', 'upppercase_matches', $str);
    }
    if ($options['uppercase_blockquote']){
        $str = preg_replace_callback('/<blockquote\b[^>]*>(.*?)<\/blockquote>/is', 'upppercase_matches', $str);
    }

    $str = str_replace(array("<blockquote><p>", "<blockquote>\r\n<p>"), '<blockquote class="blockquote"><div class="blockquote-text over"><p>', $str);
    $str = str_replace(array('</p></blockquote>', "</p>\r\n</blockquote>"), '</p></div><div class="clear"></div></blockquote>', $str);

    //$str = add_iframe_wrapper($str);

    return($str);
}

function uppercase_headers($str){
    # do_uppercase in h1, h2, ... tags
    return preg_replace_callback('~<h([1-6])\b[^>]*>.+?</h\1>~is', 'header_upppercase', $str);
}

function upppercase_matches($matches) {
    //pr($matches);
    return str_replace(end($matches), do_uppercase(end($matches)), $matches[0]);
}

function last_word($str){
    $words = preg_split('/\s+/', $str[0]);       //break into words
    $words[count($words)-1] = '<span class="last-word">'.$words[count($words)-1].'</span>';
    return implode(" ", $words);
}

function wrap_words($string){
    return preg_replace('/(<[\s\S]*?>)|([^\s-\<]+)/',"$1<span>$2</span>", $string);
}

function wrap_chars($str){
    return preg_replace("/([^\\s>])(?!(?:[^<>]*)?>)/u", "<span>$1</span>", $str);
}

function wrap_specific_word($str, $word){
    return preg_replace("/b(".$word.")b/i", '<span class="highlight">1</span>', $str);
}

/*function break_to_words($str){
    $words = preg_split('/\s+/', $str);       //break into words
    $words[count($words)-1] = '<span class="last-word">'.$words[count($words)-1].'</span>';
    $str = implode(" ", $words);
    return $str;
}*/

function quote_smart($value, $remove_quotes=false) {
    $value = strip_tags($value);

    if ($value===NULL || $value==='null') {
        $value = "null";
    } else {
        // Stripslashes
            $value = htmlspecialchars($value);
            $value = stripslashes($value);
        // Quote if not a number or a numeric string
        if (!is_numeric($value)) {
            if ($remove_quotes) {
                return $value;
            }
            $value = "'" . $value . "'";
        }
    }
    return $value;
}

/*
 * fix iframe for videos:
 * */
function filter_embed_oembed_html( $html, $url, $attr, $post_id ) {
    if (strpos($html, 'youtube') !== false || strpos($html, 'vimeo') !== false) {
        $html = fix_iframe($html, array('add_wrapper'=>false, 'autoplay'=>1, 'single_iframe'=>true));
        return '<div class="video-holder">' . $html . '</div>';
    }
    return $html;
};
add_filter( 'embed_oembed_html', 'filter_embed_oembed_html', 10, 4 );



/*
 * Used to fix a single iframe
 * */
function fix_iframe($str, $params=array()){
    $default_params = array(
        'class' => 'fill-dimensions',
        'single_iframe' => 'false',
        'add_wrapper' => true,
        'autoplay' => 1
    );
    $options = array_merge($default_params, $params);

    if ($options['single_iframe']){
        $dom = new DomDocument();
        $dom->loadHtml(strip_tags($str, '<iframe>'));
        $nodes = $dom->getElementsByTagName('iframe');
        foreach($nodes as $ifr){
            $initial_src = $ifr->getAttribute('src');

            $src = rtrim($initial_src, '&');
            if (!stripos($src, '?')){
                $src .= '?';
            }
            $src .= '&wmode=opaque&rel=0&amp;showinfo=0&color=ffffff&title=0&byline=0&portrait=0&autoplay='.$options['autoplay'];
            $ifr->setAttribute('src', $src);
        }
        $str = $dom->saveHtml($nodes->item(0));

        if ($options['add_wrapper']){
            $str = str_replace('<iframe ', '<div class="video-holder"><iframe class="'.$options['class'].'" ', $str);
            $str = str_replace('</iframe>', '</iframe></div>', $str);
        }
        else{
            $str = str_replace('<iframe ', '<iframe class="'.$options['class'].'" ', $str);
        }
    }

    /*$str = preg_replace(
        '@^<iframe\s*title="(.*?)"\s*width="(.*?)"\s*height="(.*?)"\s*src="(.*?)"\s*(.*?)</iframe>$@s',
        '<iframe class="'.$options['class'].'" title="\1" width="' . $width . '" height="' . $height . '" src="\4&wmode=opaque&rel=0&amp;controls=0&amp;showinfo=0" \5</iframe>',
        $str
    );
    */

    return $str;
}


function get_iframe_src($str, $add_params = true){
    preg_match('/src="([^"]+)"/', $str, $match);
    $src = $match[1];
    if ($add_params){
        $src .= '?wmode=opaque&rel=0&amp;showinfo=0&color=ffffff&title=0&byline=0&portrait=0';
    }
    return $src;
}

function add_iframe_wrapper($str, $str2, $autoplay=false){
    //https://www.youtube.com/watch?v=FLd00Bx4tOk  --> https://www.youtube.com/embed/FLd00Bx4tOk?feature=oembed"
    //https://vimeo.com/114570360  --> https://player.vimeo.com/video/114570360?app_id=122963"

    $str = str_replace('watch?v=', 'embed/', $str);
    $str = str_replace('https://vimeo.com/', 'https://player.vimeo.com/video/', $str);

    $str .= '?feature=oembed&wmode=opaque&rel=0&amp;showinfo=0&color=f2cd01&title=0&byline=0&portrait=0&autoplay='.$autoplay.'?muted=1';
    return '<div class="video-container abs-center"><div class="video-holder js-video-fade opc-0"><iframe width="640" height="360" class="video-iframe" allow=autoplay frameborder="0" src="'.$str.'"></iframe></div><div class="video-text opc-0 js-video-fade">'.do_uppercase($str2).'</div></div>';
}


function add_iframe_wrapper2($str, $autoplay=false){
//https://www.youtube.com/watch?v=FLd00Bx4tOk --> https://www.youtube.com/embed/FLd00Bx4tOk?feature=oembed"
//https://vimeo.com/114570360 --> https://player.vimeo.com/video/114570360?app_id=122963"

    $str = str_replace('watch?v=', 'embed/', $str);
    $str = str_replace('https://vimeo.com/', 'https://player.vimeo.com/video/', $str);

    $str .= '?feature=oembed&wmode=opaque&rel=0&amp;showinfo=0&color=f2cd01&title=0&byline=0&portrait=0&autoplay='.$autoplay.'?muted=1';
    return '<div class="emb-container"><iframe width="640" height="360" class="video-iframe" allow=autoplay frameborder="0" src="'.$str.'"></iframe></div>';
}



function fetchData($url){
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 50);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    $result = curl_exec($ch);
    curl_close($ch);
    return $result;
}

/*
 * Get Yahoo weather
 *
 * to find the woeid: http://woeid.rosselliot.co.nz/  (56558361 for santorini)
 *
 * OR find it throught the yahoo API if based on user location:
 *
 * select * from geo.places where text="Santorini, Greece" AND placeTypeName.code = 7  (town)
	https://developer.yahoo.com/yql/console/#h=select+*+from+geo.places+where+text%3D%22Santorini%2C+Greece%22
*
*  Weather codes: codes: https://developer.yahoo.com/weather/documentation.html#codes
 * */

function get_weather($WOEID=''){
    global $sessionName, $useCache, $templatedir;

    include_once($templatedir.'/lib/MyCache.php');

    if (isset($_COOKIE[$sessionName.'_woeid']) && !empty($_COOKIE[$sessionName.'_woeid'])){
        $WOEID = $_COOKIE[$sessionName.'_woeid'];
    }
    $user_area = '';
    if (isset($_COOKIE[$sessionName.'_area']) && !empty($_COOKIE[$sessionName.'_area'])){
        $user_area = $_COOKIE[$sessionName.'_area'];
    }

    if ($WOEID==''){
        if (empty($user_area)){
            $ip = ($_SERVER['HTTP_HOST']!='localhost') ? $_SERVER['REMOTE_ADDR'] : '5.55.120.200';
            $user_data = json_decode(fetchData('http://freegeoip.net/json/'.$ip));
            if (!isset($user_data->city) || empty($user_data->city)){
                $user_data = new stdClass();
                $user_data->city = 'Athens';
                $user_data->country_name = 'Greece';
            }
            $user_area = $user_data->city.', '.$user_data->country_name;
        }

        //find $WOEID according to users location
        $yql_query = 'select * from geo.places where text="'.$user_area.'" AND placeTypeName.code = 7';
        $woeid_data = json_decode(fetchData('http://query.yahooapis.com/v1/public/yql?q=' . urlencode($yql_query) . '&format=json'));
        $WOEID = '946738'; //default athens
        if (isset($woeid_data->query->results->place->woeid)){
            $WOEID = $woeid_data->query->results->place->woeid;
        }
        else if (isset($woeid_data->query->results->place) && is_array($woeid_data->query->results->place) && !empty($woeid_data->query->results->place)){
            $WOEID = $woeid_data->query->results->place[0]->woeid;
        }

        setcookie($sessionName.'_woeid', $WOEID, time()+(3600*24*60), '/');    //60 days
        $_COOKIE[$sessionName.'_woeid'] = $WOEID;

        setcookie($sessionName.'_area', $user_area, time()+(3600*24*60), '/');    //60 days
        $_COOKIE[$sessionName.'_area'] = $user_area;
    }

    $weather = array();
    $cache_file = 'weather_'.$WOEID;

    if ($useCache){
        $this_call_cache = new Cache();

        if ($cachedData = $this_call_cache->read($cache_file, '3hours')) {
            if (!empty($cachedData) ) {
                $weather = $cachedData;
            }
        }
    }
    if (empty($weather)){
        $yql_query = 'select item.condition from weather.forecast where woeid ='.$WOEID.' and u="c"';
        $yql_query_url = "http://query.yahooapis.com/v1/public/yql?q=" . urlencode($yql_query) . "&format=json";
        $weather = fetchData($yql_query_url);
        $reply_weather = json_decode($weather);

        if (isset($reply_weather->query->results->channel->item->condition) && $useCache){
            $this_call_cache->write($cache_file, $weather, '3hours', false);
        }
    }
    else{
        $reply_weather = json_decode($weather);
    }

    /*
    stdClass Object
    (
        [query] => stdClass Object
            (
                [count] => 1
                [created] => 2016-06-22T07:06:45Z
                [lang] => en-US
                [results] => stdClass Object
                    (
                        [channel] => stdClass Object
                            (
                                [item] => stdClass Object
                                    (
                                        [condition] => stdClass Object
                                            (
                                                [code] => 23
                                                [date] => Wed, 22 Jun 2016 09:00 AM EEST
                                                [temp] => 27
                                                [text] => Breezy
                                            )
    */

    $reply = array();

    if (isset($reply_weather->query->results->channel->item->condition)){
        $conditions = $reply_weather->query->results->channel->item->condition;

        $reply['text'] = $conditions->text;
        $reply['code'] = $conditions->code;
        $reply['temp'] = $conditions->temp;
        $city = explode(',', $user_area);
        $reply['city'] = $city[0];
    }

    return $reply;
}

function filesize_formatted($path, $show_file_type=true){
    $system_file_path = realpath(getcwd().'/wp-content/uploads/'.basename($path));

    $return = '';

    if (file_exists($system_file_path)){
        $size = filesize($system_file_path);
        $units = array( 'B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
        $power = $size > 0 ? floor(log($size, 1024)) : 0;
        $return = number_format($size / pow(1024, $power), 2, '.', ',') . ' ' . $units[$power];

        if ($show_file_type){
            $return = pathinfo($path, PATHINFO_EXTENSION).', '.$return;
        }
    }

    return $return;
}

function format_url($url, $http_action='include'){
    if (empty($url)){
        return '#';
    }
    if ($http_action == 'include'){
        /*if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
            $url = "http://" . $url;
        }*/
        /*parse_url($url, PHP_URL_SCHEME) === null ? 'http://'.$url : $url;*/
        if (strpos($url, '://')===false) {
            $url = 'http://' . $url;
        }
    }
    else{
        $url = str_replace(array('http://', 'https://'), '', $url);
    }
    if (substr($url, -1) == '/'){
        $url = substr($url, 0, strlen($url)-1);
    }
    return $url;
}

function mailchimpStore($data = array()){
    //$data['lang'] = (isset($data['lang'])) ? $data['lang'] : 'en';
    $user_email = $data['email'];
    $merge_vars = array(
        'INSDATE' => date('d/m/Y H:i:s'),
        'REF' => $data['ref'],
        //'LANG' => $data['lang']
    );

    if (isset($data['your-name'])){
        $merge_vars['FNAME'] = quote_smart($data['your-name'], true);
    }
    if (isset($data['lastname'])){
        $merge_vars['LNAME'] = quote_smart($data['lastname'], true);
    }
    if (isset($data['company'])){
        $merge_vars['COMPANY'] = quote_smart($data['company'], true);
    }
    if (isset($data['title'])){
        $merge_vars['TITLE'] = quote_smart($data['title'], true);
    }
    if (isset($data['telephone'])){
        $merge_vars['TEL'] = quote_smart($data['telephone'], true);
    }

    $reply = my_Mailchimp_subscribe($user_email, $merge_vars);

    if ($reply != 4 && $reply != 5){
        add_gdpr_log($user_email, 'NS');
    }

    return $reply;
}

function my_Mailchimp_subscribe($user_email, $merge_vars){
    global $mailchimp_keys, $templatedir;

    require_once($templatedir.'/lib/MCAPI.class.php');
    $api = new MCAPI($mailchimp_keys['api_key']); //api key - thinknews

    $api->listSubscribe($mailchimp_keys['List_ID'], $user_email, $merge_vars);

    $reply_flag = 1;
    if ($api->errorCode) {
        if ($api->errorCode == 214) {//already subscribed
            $reply_flag = 5;
        } else { //$api->errorMessage;
            $reply_flag = 4;
        }
    }
	
	if ($reply_flag != 4 && $reply_flag != 5){
        add_gdpr_log($user_email, 'NS');
    }

    return $reply_flag;
}

function my_Mailchimp_unsubscribe($email_address){
    global $mailchimp_keys, $templatedir;

    require_once($templatedir.'/lib/MCAPI.class.php');
    $api = new MCAPI($mailchimp_keys['api_key']); //api key - thinknews

    $api->listUnsubscribe($mailchimp_keys['List_ID'], $email_address, true, false, true);
    if ($api->errorCode) {
        return false;
    }
    return true;
}

function encodeURI($file) {
    if (is_array($file)){
        $url = $file[0];
    }
    else{
        $url = $file;
    }
    $replace_pairs = array(
        '%2D'=>'-','%5F'=>'_','%2E'=>'.','%21'=>'!', '%7E'=>'~',
        '%2A'=>'*',
        //'%28'=>'(', '%29'=>')', '%27'=>"'",
        '%3B'=>';','%2C'=>',','%2F'=>'/','%3F'=>'?','%3A'=>':',
        '%40'=>'@','%26'=>'&','%3D'=>'=','%2B'=>'+','%24'=>'$',
        '%23'=>'#'
    );
    //$url = rawurlencode($url);
    $url = strtr(rawurlencode($url), $replace_pairs);
    //$url = str_replace()
    $url = str_replace("%252", "%2", $url);

    if (is_array($file)){
        $file[0] = $url;
    }
    else{
        $file = $url;
    }
    return $file;
}

function clearCache(){
    global $cacheDir;

    $files = glob($cacheDir.'*'); // get all file names
    foreach($files as &$file){ // iterate files
        if(is_file($file)){
            @unlink($file); // delete file
        }
        else{
            rmdir ($file);
        }
    }
}

function get_email_template($subject, $return_element='header'){
    global $websiteTitle, $template_url;

    $email_template_settings = array(
        'link_color' => '#a9a59e',
        'bg_color' => '#fff',
        'letters_color' => '#333',
        'logo' => $template_url.'/images/logo-black.png',
    );

    $HTML_head = '<html>
			<head>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
				<title>'.$subject.'</title>
				<style type="text/css" media="all">
					a{
						color:'.$email_template_settings['link_color'].';
						text-decoration:underline;
					}
					h4{
						color:'.$email_template_settings['link_color'].';
						margin:10px 0 20px 0;
						padding:0;
						font-size:14px;
					}
					ul{
						margin:2px 0 13px 0;
						padding:0 0 0 0;
						list-style:disc;
					}
					ul li{
						margin:0 0 3px 20px;
						padding:0;
						list-style:disc;
					}
				</style>
			</head>
			<body style="background:'.$email_template_settings['bg_color'].'; color:'.$email_template_settings['letters_color'].'; font-size:16px; font-family:Calibri, Arial, Helvetica, sans-serif; margin:0; padding:0;">
				<center>
					<table cellpadding="0" cellspacing="0" style="width:500px; margin:0 auto; border:none;">
					    <tr>
					        <td style="padding: 10px; text-align:left;"><a href="'.WP_HOME.'"><img src="'.$email_template_settings['logo'].'" alt="'.$websiteTitle.'" border="0" /></a></td>
					    </tr>
					    <tr>
					        <td style="line-height:1.1em; padding: 10px;">
					            <h1 style="margin-bottom:20px; font-size:20px; color:'.$email_template_settings['link_color'].';">'.$subject.'</h1>';

    $HTML_footer = '        </td>
                    </tr>
					    </table>
				</center>
			</body>
		</html>';

    if ($return_element == "header")
        return $HTML_head;
    else return $HTML_footer;

}

/* ==== */

function html_skrollr_points_InView($default_classes, $animation = 'anim--offview anim--up-sm', $delay = '', $trans='trans-slowest trans-cubic', $data_start='bottom-top', $data_end='85p-top'){
    return 'data-'.$data_start.'="@class: '.$default_classes.' '.$animation.' '.$trans.' '.$delay.'" data-'.$data_end.'="@class: '.$default_classes.' '.$trans.' '.$delay.'"';
}

function html_skrollr_points($params=array()){
    $default_classes = (isset($params['default_classes'])) ? $params['default_classes'] : '';   //the default classes - otherwise they will be overriden
    $animation = (isset($params['animation'])) ? $params['animation'] : 'anim-offview anim--fadeup';   //css animation
    $delay = (isset($params['delay'])) ? $params['delay'] : '';
    $trans = (isset($params['trans'])) ? $params['trans'] : 'trans-slowest';
    $data_start = (isset($params['data_start'])) ? $params['data_start'] : 'bottom-top';    //position of no animate - as long as it is out of the screen no animation has started
    $data_end = (isset($params['data_end'])) ? $params['data_end'] : '85p-top'; //position to start the animations. eg when on 85% from the top

    return 'data-'.$data_start.'="@class: '.$default_classes.' '.$animation.' '.$trans.' '.$delay.'" data-'.$data_end.'="@class: '.$default_classes.' '.$trans.' '.$delay.'"';
}

/*
* Advanced settings for skrollr
 * eg
echo skrollr_InView(array('default_classes'=>'home-section', 'animation'=>'anim-offview', 'data_medium_state'=>'85p-top', 'med_state_class'=>'anim--med-state', 'data_end'=>'50p-top', 'data_out' =>'center-bottom'));
OR
echo skrollr_InView(array('default_classes'=>'gutter wide post-item', 'animation'=>'anim-offview'));
*/
function skrollr_InView($params=array()){
    $default_classes = (isset($params['default_classes'])) ? $params['default_classes'] : '';   //the default classes - otherwise they will be overriden
    $animation = (isset($params['animation'])) ? $params['animation'] : 'anim-offview anim--fadeup';   //css animation
    $delay = (isset($params['delay'])) ? $params['delay'] : '';
    $trans = (isset($params['trans'])) ? $params['trans'] : 'trans-slowest';
    $data_start = (isset($params['data_start'])) ? $params['data_start'] : 'bottom-top';    //position of no animate - as long as it is out of the screen no animation has started
    $data_end = (isset($params['data_end'])) ? $params['data_end'] : '85p-top'; //position to start the animations. eg when on 85% from the top

    $data_out = (isset($params['data_out'])) ? $params['data_out'] : '';    //can use it for triggering a leave event, eg center-bottom

    $data_medium_state =  (isset($params['data_medium_state'])) ? $params['data_medium_state'] : '';    //intermedium - 1st set of anmations, eg 85p-top
    $med_state_class =  (isset($params['med_state_class'])) ? $params['med_state_class'] : '';  //intermedium - class to add, eg anim--med-state

    $reply = 'data-'.$data_start.'="@class: '.$default_classes.' '.$animation.' '.$trans.' '.$delay.'" data-'.$data_end.'="@class: '.$default_classes.' '.$trans.' '.$delay.'"';
    if (!empty($data_out)){
        //leave event, eg hide block when it is leaving the viewport
        $data_out_class = (isset($params['data_out_class'])) ? $params['data_out_class'] : '';
        $reply .= ' data-'.$data_out.'="@class: '.$default_classes.' '.$data_out_class.' '.$trans.' '.$delay.'"';
    }
    if (!empty($data_medium_state)){
        $reply .= ' data-'.$data_medium_state.'="@class: '.$default_classes.' '.$med_state_class.' '.$animation.' '.$trans.' '.$delay.'"';
    }
    return $reply;
}

function css_transform($what){
    return '-moz-transform:'.$what.'; -webkit-transform:'.$what.'; transform:'.$what.';';
}

/*function detect_mobile(){
    $useragent=$_SERVER['HTTP_USER_AGENT'];
    if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))){
        return true;
    }
    return false;
}
*/
function get_username_from_email($email){
    if (empty($email)){
        return '';
    }
    $result = explode('@', $email);
    return $result[0];
}

function date_compare($date1, $date2){
    return strtotime($date1) - strtotime($date2);
}

function insertArrayAtPosition( $array, $insert, $position ) {
    /*
    $array : The initial array i want to modify
    $insert : the new array i want to add, eg array('key' => 'value') or array('value')
    $position : the position where the new array will be inserted into. Please mind that arrays start at 0
    */
    return array_slice($array, 0, $position, TRUE) + $insert + array_slice($array, $position, NULL, TRUE);
}

function do_highlight($str, $key, $length=0){
    if (empty($str)){
        return '';
    }
    $str = strip_tags($str);


    if ($length > 0){
        //do substring:

        $pos = stripos($str, $key);

        if (strlen($key) < $pos) {
            $pos -= strlen($key);
        }
        else {
            $pos = 0;
        }

        if ($pos === false){
            $pos = 0;
        }

        $str = utf8_substr_v1($str, $pos, $length);
    }

    $res = str_ireplace($key, '<span class="highlight">'.$key.'</span>', $str);
    return $res;
}