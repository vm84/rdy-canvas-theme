<?php
function get_next_page($menu_order=1, $post_type='page', $action='next'){
    global $wpdb, $table_prefix;

    //ID, post_author, post_date, post_date_gmt, post_content, post_title, post_excerpt, post_status, comment_status, ping_status, post_password, post_name, to_ping, pinged, post_modified, post_modified_gmt, post_content_filtered, post_parent, guid, menu_order, post_type, post_mime_type, comment_count

    if (function_exists('icl_object_id')) {
        //wpml enabled

        if ($action == 'next'){
            $reply = $wpdb->get_results("SELECT ".$table_prefix."posts.ID, post_title FROM ".$table_prefix."posts
                JOIN ".$table_prefix."icl_translations t on t.element_id=".$table_prefix."posts.ID and language_code='".ICL_LANGUAGE_CODE."'
                where post_type='".$post_type."' and post_status='publish' and menu_order>".$menu_order." order by menu_order ASC, post_date DESC limit 1");
        }
        else{
            $reply = $wpdb->get_results("SELECT ".$table_prefix."posts.ID, post_title FROM ".$table_prefix."posts
                JOIN ".$table_prefix."icl_translations t on t.element_id=".$table_prefix."posts.ID and language_code='".ICL_LANGUAGE_CODE."'
                where post_type='".$post_type."' and post_status='publish' and menu_order<".$menu_order." order by menu_order DESC, post_date ASC limit 1");
        }

    }
    else{
        if ($action == 'next'){
            $reply = $wpdb->get_results("SELECT ID, post_title FROM ".$table_prefix."posts where post_type='".$post_type."' and post_status='publish' and menu_order>".$menu_order." order by menu_order ASC, post_date DESC limit 1");
        }
        else {
            $reply = $wpdb->get_results("SELECT ID, post_title FROM " . $table_prefix . "posts where post_type='" . $post_type . "' and post_status='publish' and menu_order<" . $menu_order . " order by menu_order DESC, post_date ASC limit 1");
        }
    }

    return $reply[0];
}


function custom_post_nav($args=array()) {
    /*
    eg
    custom_post_nav(array(
            'prev_text' => '<span class="prev-link trans font-bold font-small"><span class="icon icon--arrow-left"></span> '.get_trans('trans_prev_post', true).'</span>',
            'next_text' => '<span class="next-link trans font-bold font-small">'.get_trans('trans_next_post', true).' <span class="icon icon--arrow-right"></span></span>',
            'menu_order' => $post->menu_order,
            'post_type' => 'post'
        ));
    */

    global $wpdb, $table_prefix;

    //$under_same_category = (isset($args['under_same_category'])) ? $args['under_same_category'] : false;

    //ID, post_author, post_date, post_date_gmt, post_content, post_title, post_excerpt, post_status, comment_status, ping_status, post_password, post_name, to_ping, pinged, post_modified, post_modified_gmt, post_content_filtered, post_parent, guid, menu_order, post_type, post_mime_type, comment_count

    if (function_exists('icl_object_id')) {
        //wpml enabled

        $next = $wpdb->get_results("SELECT ".$table_prefix."posts.ID, post_title FROM ".$table_prefix."posts
        JOIN ".$table_prefix."icl_translations t on t.element_id=".$table_prefix."posts.ID and language_code='".ICL_LANGUAGE_CODE."'
        where post_type='".$args['post_type']."' and post_status='publish' and menu_order>".$args['menu_order']." order by menu_order ASC, post_date DESC limit 1");

        $prev = $wpdb->get_results("SELECT ".$table_prefix."posts.ID, post_title FROM ".$table_prefix."posts
        JOIN ".$table_prefix."icl_translations t on t.element_id=".$table_prefix."posts.ID and language_code='".ICL_LANGUAGE_CODE."'
        where post_type='".$args['post_type']."' and post_status='publish' and menu_order<".$args['menu_order']." order by menu_order DESC, post_date ASC limit 1");
    }
    else{
        $next = $wpdb->get_results("SELECT ID, post_title FROM ".$table_prefix."posts where post_type='".$args['post_type']."' and post_status='publish' and menu_order>".$args['menu_order']." order by menu_order ASC, post_date DESC limit 1");
        $prev = $wpdb->get_results("SELECT ID, post_title FROM ".$table_prefix."posts where post_type='".$args['post_type']."' and post_status='publish' and menu_order<".$args['menu_order']." order by menu_order DESC, post_date ASC limit 1");
    }

    /*if ($under_same_category && !empty($args['taxonomy'])){
        $args['tax_query'] = array(
            array(
                'taxonomy'  => $args['taxonomy']['taxonomy'],
                'field'     => 'cat_id',
                'terms'     => $args['taxonomy']['term_id']
            )
        );
    }*/

    if (isset($prev[0]->ID)){
        ?><a href="<?php echo get_permalink($prev[0]->ID); ?>" class="page-numbers prev" aria-label="prev link"><?php echo $args['prev_text']; ?></a><?php
    }
    else{
        echo '<span class="prev page-numbers disabled" aria-label="prev link">'.$args['prev_text'].'</span>';
    }

    if (isset($next[0]->ID)){
        ?><a href="<?php echo get_permalink($next[0]->ID); ?>" class="page-numbers next" aria-label="next link"><?php echo $args['next_text']; ?></a><?php
    }
    else{
        echo '<span class="next page-numbers disabled" aria-label="next link">'.$args['next_text'].'</span>';
    }
}

function theme_post_nav($args=array()) {
    /*$args = array(
        'prev_text' => '<span class="icomoon">&#xe604;</span><span class="letters trans">Prev.</span>',
        'next_text' => '<span class="icomoon">&#xe605;</span><span class="letters trans">Next</span>',
    );*/
    $under_same_category = (isset($args['under_same_category'])) ? $args['under_same_category'] : false;

    $prev = get_next_post( $under_same_category, '', 'category' );
    if (!empty($prev)){
        ?><a href="<?php echo get_permalink( $prev->ID ); ?>" class="page-numbers prev" aria-label="next prev"><?php echo $args['prev_text']; ?></a><?php
    }
    else{
        echo '<span class="prev page-numbers disabled" aria-label="prev link">'.$args['prev_text'].'</span>';
    }

    $next = get_previous_post( $under_same_category, '', 'category' );
    if (!empty($next)){
        ?><a href="<?php echo get_permalink( $next->ID ); ?>" class="page-numbers next" aria-label="next link"><?php echo $args['next_text']; ?></a><?php
    }
    else{
        echo '<span class="next page-numbers disabled" aria-label="next link">'.$args['next_text'].'</span>';
    }
}

function theme_paging_nav($args = array()) {
    global $wp_query, $wp_rewrite;

    // Don't print empty markup if there's only one page.
    if ( $wp_query->max_num_pages < 2 ) {
        return;
    }

    $paged        = get_query_var( 'paged' ) ? intval( get_query_var( 'paged' ) ) : 1;
    $pagenum_link = html_entity_decode( get_pagenum_link() );
    $query_args   = array();
    $url_parts    = explode( '?', $pagenum_link );

    if ( isset( $url_parts[1] ) ) {
        wp_parse_str( $url_parts[1], $query_args );
    }

    $pagenum_link = remove_query_arg( array_keys( $query_args ), $pagenum_link );
    $pagenum_link = trailingslashit( $pagenum_link ) . '%_%';

    $format  = $wp_rewrite->using_index_permalinks() && ! strpos( $pagenum_link, 'index.php' ) ? 'index.php/' : '';
    $format .= $wp_rewrite->using_permalinks() ? user_trailingslashit( $wp_rewrite->pagination_base . '/%#%', 'paged' ) : '?paged=%#%';

    //pr($query_args);

    // Set up paginated links.
    $links = paginate_links( array(
        'base'     => $pagenum_link,
        'format'   => $format,
        'total'    => $wp_query->max_num_pages,
        'current'  => $paged,
        'mid_size' => 1,
        //'add_args' => array_map( 'urlencode', $query_args ),
        'add_args' => $query_args,
        'next_text' => $args['next_text'],
        'prev_text' => $args['prev_text'],
        'type' => 'array'
    ) );

    if ( $links ) :
        ?>
        <nav class="navigation pagination">
            <?php
            if ( $paged == 1) echo '<span class="prev page-numbers disabled" aria-label="prev link">'.$args['prev_text'].'</span>';

            echo join('', $links);

            if ( $paged == $wp_query->max_num_pages ) echo '<span class="next page-numbers disabled" aria-label="next link">'.$args['next_text'].'</span>';
            ?>
            <div class="clear"></div>
        </nav>
    <?php
    endif;
}

if ( ! function_exists( 'twentyfifteen_comment_nav' ) ) :
    /**
     * Display navigation to next/previous comments when applicable.
     *
     * @since Twenty Fifteen 1.0
     */
    function twentyfifteen_comment_nav() {
        // Are there comments to navigate through?
        if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) :
            ?>
            <nav class="navigation comment-navigation" role="navigation">
                <h2 class="screen-reader-text"><?php _e( 'Comment navigation', 'twentyfifteen' ); ?></h2>
                <div class="nav-links">
                    <?php
                    if ( $prev_link = get_previous_comments_link( __( 'Older Comments', 'twentyfifteen' ) ) ) :
                        printf( '<div class="nav-previous">%s</div>', $prev_link );
                    endif;

                    if ( $next_link = get_next_comments_link( __( 'Newer Comments', 'twentyfifteen' ) ) ) :
                        printf( '<div class="nav-next">%s</div>', $next_link );
                    endif;
                    ?>
                </div><!-- .nav-links -->
            </nav><!-- .comment-navigation -->
        <?php
        endif;
    }
endif;
?>