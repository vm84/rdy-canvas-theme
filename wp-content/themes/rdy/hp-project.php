<?php
global $websiteTitle, $schemaTags;

/**
 * ACF Fields.
 */
$image = $project['hp_project_image'];
$title = $project['hp_project_title'];
$text = $project['hp_project_text'];

/**
 * Image.
 */
$img_url = $image['url'];
$image_h = $image['height'];
$image_w = $image['width'];
$ratio = ($image_h * 100) / $image_w;


/**
 * Link.
 */

$hp_project_link = $project['hp_project_link'];


?><div class="hp-project" data-animation="image" itemscope itemtype="http://schema.org/CreativeWork">
  <?php


      ?><a itemprop="url" href="<?php echo $hp_project_link; ?>" class="hp-project__wrapper hp-project__link relative cursor js-canvas-link" data-index="<?php echo $counter; ?>">
          <img itemprop="image" data-src="<?php echo $img_url; ?>" class="lazy fit-image" width="<?php echo $image_w; ?>" height="<?php echo $image_h; ?>" alt="<?php echo $websiteTitle.'-'.$title; ?>">
          <?php
            if(!empty($text) || !empty($title)) {
              ?><span class="hp-project__info">
                  <span class="hp-project__info__overlay display-block"></span>
                  <span class="hp-project__info__inner display-block">
                  <?php

                    if(!empty($title)) {
                      ?><h3 itemprop="name" class="hp-project__info__title"><?php echo $title; ?></h3><?php
                    }
                     if(!empty($text)) {
                      ?><span itemprop="description" class="hp-project__info__text display-block"><?php echo $text; ?></span><?php
                    }

                  ?>
                  </span>

              </span><?php
            }
          ?>
    </a><?php



    //=========== SCHEMA ==========

    ?>
    <span itemprop="dateModified" content="<?php echo $post->post_modified; ?>"></span>
    <span itemprop="author publisher" itemscope itemtype="http://schema.org/Corporation">
      <span itemprop="name" content="<?php echo $websiteTitle; ?>"></span>
       <span class="hidden" itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
            <img itemprop="url" src="<?php echo $schemaTags['image']; ?>" alt="logo">
            <meta itemprop="width" content="200">
            <meta itemprop="height" content="200">
        </span>
    </span>
</div>
