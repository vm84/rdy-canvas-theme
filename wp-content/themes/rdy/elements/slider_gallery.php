<div id="js-slider" class="slider" >
    <div class="slider__bg"></div>
    <div id="gallery-timeline" class="gallery-timeline"></div>
    <div class="slider__close" id="js-slider-close">
        <svg x="0px" y="0px"
             viewBox="0 0 512.001 512.001" style="enable-background:new 0 0 512.001 512.001;" xml:space="preserve">
        <path d="M284.286,256.002L506.143,34.144c7.811-7.811,7.811-20.475,0-28.285c-7.811-7.81-20.475-7.811-28.285,0L256,227.717
			L34.143,5.859c-7.811-7.811-20.475-7.811-28.285,0c-7.81,7.811-7.811,20.475,0,28.285l221.857,221.857L5.858,477.859
			c-7.811,7.811-7.811,20.475,0,28.285c3.905,3.905,9.024,5.857,14.143,5.857c5.119,0,10.237-1.952,14.143-5.857L256,284.287
			l221.857,221.857c3.905,3.905,9.024,5.857,14.143,5.857s10.237-1.952,14.143-5.857c7.811-7.811,7.811-20.475,0-28.285
			L284.286,256.002z"/>
        </svg>
    </div>
    <div class="slider__holder">
        <div class="swiper">
            <div class="swiper-wrapper">
                <?php foreach ($main_gallery as &$gallery_image){
                    ?>
                    <div class="swiper-slide">
                        <div class="swiper-slider__inner">
                            <img class="swiper-lazy" data-src="<?php echo $gallery_image['url']; ?>"/>
                        </div>
                    </div>
                    <?php
                } ?>
            </div>
            <div class="swiper-button-prev">
                <svg class="slider__arrow slider__arrow--prev" x="0px" y="0px" viewBox="0 0 477.175 477.175" style="" xml:space="preserve"> <path d="M360.731,229.075l-225.1-225.1c-5.3-5.3-13.8-5.3-19.1,0s-5.3,13.8,0,19.1l215.5,215.5l-215.5,215.5c-5.3,5.3-5.3,13.8,0,19.1c2.6,2.6,6.1,4,9.5,4c3.4,0,6.9-1.3,9.5-4l225.1-225.1C365.931,242.875,365.931,234.275,360.731,229.075z"></path></svg>
            </div>
            <div class="swiper-button-next">
                <svg class="slider__arrow slider__arrow--next" x="0px" y="0px" viewBox="0 0 477.175 477.175" style="" xml:space="preserve"> <path d="M360.731,229.075l-225.1-225.1c-5.3-5.3-13.8-5.3-19.1,0s-5.3,13.8,0,19.1l215.5,215.5l-215.5,215.5c-5.3,5.3-5.3,13.8,0,19.1c2.6,2.6,6.1,4,9.5,4c3.4,0,6.9-1.3,9.5-4l225.1-225.1C365.931,242.875,365.931,234.275,360.731,229.075z"></path></svg>
            </div>
        </div>
    </div>

</div>
