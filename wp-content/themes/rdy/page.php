<?php
/**
* The template for displaying all single posts
*
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
* @subpackage Twenty_Nineteen
* @since 1.0.0
*/

global $websiteTitle, $template_url;


get_header();

while (have_posts()) : the_post();
    $thispage = $post;
    $thispage_id = $post->ID;


    //$bg_image = my_get_post_thumb($thispage_id);
    //$top_title = get_field('page_top_title', $thispage_id);

?><main id="content" class="main-content" data-template="page">

</main><?php

/*
wp_reset_postdata();*/

endwhile;

get_footer();
