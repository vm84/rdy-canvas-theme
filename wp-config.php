<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'rdy-canvas-theme' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'vmitsis' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'dlD[o2Bo(C9p[sL0Yd`~/6W=;5P0tj>p`} nNQWHX3cx&^hz_cC;!gE?:V]u&H/A' );
define( 'SECURE_AUTH_KEY',  '}XE8pbR}rPxYK[@?3fU|LiQDIeIn:ALT(Nq9~H6RJJCgaD_+&NO~-k%[4&w#H(hh' );
define( 'LOGGED_IN_KEY',    'UP;P9xQ1G%pi|iBy8#H/lq8U.*Q3O5(DB5qA^Lp;2`=:1*M9hO_e#U*$4AZ0Ndaj' );
define( 'NONCE_KEY',        'ch9E@|S2@O#[RT!P:jl%pB.]&{hIqDbaaWTcnMko*$1kL63bwWX`I49{78vtE{4m' );
define( 'AUTH_SALT',        ',Z^Gi@=b6Jki~rIyW*10#NgvW0J-e: qJxQ!wHUF=D77#uX0e*Mio2`t#F4nG9n)' );
define( 'SECURE_AUTH_SALT', '[y%&tg[iVVSFA}_&S)^Q]9i`N]QR}[Gjns:cj1cQ|e_s9oKcn%(+aM1&ie`9`Jmo' );
define( 'LOGGED_IN_SALT',   '@|!`8J-+fA>IUaY1u1)VgBg03#amb7`pya|i/oB>e])C92uCOex}J{uJarEg}ORf' );
define( 'NONCE_SALT',       '3U,BA,NQ{d@:_K{CD^fDQTGAiFKiE v+0n4gE/NSno42LhbGBEDQLPf46UHR/Q?#' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'kga_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

define('WP_HOME', 'http://localhost/rdy-canvas-theme');
define('WP_SITEURL', 'http://localhost/rdy-canvas-theme');

//contact form 7: remove uneeded css & js
define('WPCF7_AUTOP', false);
define('WPCF7_LOAD_CSS', false);    //remove it completely
define('WPCF7_LOAD_JS', false);  //load it only when needed (through functons.php file)

//contact form 7: allow only admin access
define( 'WPCF7_ADMIN_READ_CAPABILITY', 'manage_options' );
define( 'WPCF7_ADMIN_READ_WRITE_CAPABILITY', 'manage_options' );

define( 'DISALLOW_FILE_EDIT', true ); // Disable File Editor

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
    define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

